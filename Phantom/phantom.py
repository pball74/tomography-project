# The challenge of this project is to develop a program that is able to analyse tomography images in an autonomus way.
# In this program we want to segmente the different materials displayed on an image.
# For this reason we have developped a class using several functions to execute the procedure.

import os
import gc
import sys
import vtkmodules
import vtkmodules.all
import vtk
import cv2
import numba
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from scipy.signal import argrelextrema
from tqdm import tqdm
from scipy import interpolate, signal, ndimage
from numba import jit
from skimage import measure
from vtk.util import numpy_support
from texttable import Texttable
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import pandas as pd
from source_ui_mod.phantom_ui_mod import Ui_MainWindow



class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.init_Parameters()
        self.IO = InputOutput(parent=self)
        self.Picture = Picture(parent=self)
        self.Parameters = Parameters(parent=self)
        self.CheckableComboBox = CheckableComboBox()
        self.UpdateCheckableComboBox = UpdateCheckableComboBox(parent=self)
        self.SaveFrames = SaveFrames(parent=self)
        self.StlSettings = StlSettings(parent=self)
        self.Display = Display(parent=self)
        self.Manual_parameters = Manual_parameters(parent=self)
        self.ObjectList = ObjectList(parent=self)
        self.Check = Check(parent=self)
        self.ProgressBar = ProgressBar(parent=self)
        self.Run = Run(parent=self)
        self.Tomography = Tomography(parent=self)

        self.ui.timer = QTimer(self)
        self.ui.timer.setInterval(50)
        self.ui.timer.timeout.connect(self.refresh_Ui_Display)
        self.ui.timer.start()



    def init_Parameters(self):
        self.ui.label_time = QtWidgets.QLabel(self.ui.centralwidget)
        self.ui.label_time.setStyleSheet('border: 0; color:  black;')
        self.ui.label_time.setGeometry(QtCore.QRect(675, 15, 351, 20))
        self.ui.label_time.setText(
            QtCore.QDateTime.currentDateTime().toString())

        self.ui.Exit_Button.clicked.connect(sys.exit)
        self.ui.Object_List_State_Value = 1
        self.ui.Voxel_State_Value = 1
        self.ui.Input_Output_State_Value = 1
        self.ui.Sphere_Size_State_Value = 1
        self.ui.Reduction_Coefficient_State_Value = 1
        self.ui.Opening_State_Value = 1

        self.ui.Parameters_tabWidget.setCurrentIndex(0)
        self.ui.Object_tabWidget.setCurrentIndex(0)


    def refresh_Ui_Display(self):
        self.ui.label_time.setText(
            QtCore.QDateTime.currentDateTime().toString())



class InputOutput(QtCore.QObject):
    def __init__(self, parent):
        super(InputOutput, self).__init__(parent)
        self.init_Parameters()
        self.connections()

    def init_Parameters(self):
        self.parent().ui.Text_Input_Output_State = " /!\ Select correct path /!\ "
        self.format_img = ".tif"

    def Check_Input_Output_State (self) :
        if self.parent().ui.Input_Folder_Directory_Text.text() !="" and self.parent().ui.Output_Folder_Directory_Text.text() !="":
            self.parent().ui.Text_Input_Output_State = "All good"
            self.parent().ui.Input_Output_State_Value = 0
        else :
            self.parent().ui.Text_Input_Output_State = " /!\ Select correct path /!\ "
            self.parent().ui.Input_Output_State_Value = 1

        if self.parent().ui.Input_Folder_Directory_Text.text() !="":
            self.impathes = sorted([f for f in os.listdir(self.parent().ui.Input_Folder_Directory_Text.text()) if f.endswith(self.format_img)])
            if len(self.impathes) == 0 :
                self.parent().ui.Text_Input_Output_State = " /!\ Wrong input path /!\ "
                self.parent().ui.Input_Output_State_Value = 1


    def GetfileInput(self):
        folder = str(QFileDialog.getExistingDirectory(None, "Select Directory"))
        self.parent().ui.Input_Folder_Directory_Text.setText(folder)
        if self.parent().ui.Input_Folder_Directory_Text.text() == "" :
            self.parent().ui.Input_Folder_Directory_Text.setStyleSheet("border : 0.5px solid red;")
        else :
            self.parent().ui.Input_Folder_Directory_Text.setStyleSheet("border : 0.5px solid green;")

    def GetfileOutput(self):
        folder = str(QFileDialog.getExistingDirectory(None, "Select Directory"))
        self.parent().ui.Output_Folder_Directory_Text.setText(folder)
        if self.parent().ui.Output_Folder_Directory_Text.text() == "" :
            self.parent().ui.Output_Folder_Directory_Text.setStyleSheet("border : 0.5px solid red;")
        else :
            self.parent().ui.Output_Folder_Directory_Text.setStyleSheet("border : 0.5px solid green;")

    def connections(self):
        self.parent().ui.Input_Picture_Folder.clicked.connect(self.GetfileInput)
        self.parent().ui.Output_Picture_Folder.clicked.connect(self.GetfileOutput)
        self.parent().ui.Input_Folder_Directory_Text.textChanged.connect(self.Check_Input_Output_State)
        self.parent().ui.Output_Folder_Directory_Text.textChanged.connect(self.Check_Input_Output_State)
        self.parent().ui.Input_Picture_Folder.clicked.connect(self.Check_Input_Output_State)
        self.parent().ui.Output_Picture_Folder.clicked.connect(self.Check_Input_Output_State)

class Picture(QtCore.QObject):
    def __init__(self, parent):
        super(Picture, self).__init__(parent)
        self.init_Parameters()
        self.connections()

    def init_Parameters(self):
        self.parent().ui.Picture_number_LineEdit.setText("0")
        self.format_img = ".tif"
        self.parent().ui.Picture_Display.setText('<font color="orange">Select an input path</font>')
        self.parent().ui.Picture_Display.setFont(QFont('Arial', 28))
        self.pict_number = 0
        self.impathes = []


    @staticmethod
    @jit(nopython=True)
    def ChangePixelValue(source, coordonates, value):
        image = source.copy()
        for i in range(len(coordonates)):
            image[coordonates[i][0],coordonates[i][1]] = value
        return image
    @staticmethod
    @jit(nopython=True)
    def GetPixelValueBetween(source, infvalue, suppvalue):
        position = np.where(np.logical_and(infvalue<=source, source<=suppvalue))
        zipped = np.column_stack((position))
        return zipped

    @staticmethod
    @jit(nopython=True)
    def GetPixelValueOutside(source, infvalue, suppvalue):
        position = np.where(np.logical_or(infvalue>source, source>suppvalue))
        zipped = np.column_stack((position))
        return zipped

    def Input_dir(self):
        if self.parent().ui.Input_Folder_Directory_Text.text() == '':
            self.parent().ui.Picture_Display.setText('<font color="orange">Select an input path</font>')
            self.parent().ui.Picture_Display.setFont(QFont('Arial', 28))
            self.parent().ui.Activate_thresholding_checkBox.setEnabled(False)
            self.parent().ui.Save_active_frame_pushButton.setEnabled(False)
        if self.parent().ui.Input_Folder_Directory_Text.text() != '':
            self.Input_dir = self.parent().ui.Input_Folder_Directory_Text.text()
            self.impathes = sorted([f for f in os.listdir(self.Input_dir) if f.endswith(self.format_img)])
            self.parent().ui.Picture_number_horizontalScrollBar.setMaximum(len(self.impathes)-1)
            if len(self.impathes) == 0:
                self.parent().ui.Picture_Display.setText('<font color="red">/!\ Wrong input path /!\</font>')
                self.parent().ui.Picture_Display.setFont(QFont('Arial', 28))
                self.parent().ui.Activate_thresholding_checkBox.setEnabled(False)
                self.parent().ui.Save_active_frame_pushButton.setEnabled(False)
                self.parent().ui.frame_number_label.setEnabled(False)
                self.parent().ui.Picture_number_LineEdit.setEnabled(False)
                self.parent().ui.Picture_number_horizontalScrollBar.setEnabled(False)
                self.parent().ui.Activate_thresholding_checkBox.setChecked(False)
                self.parent().ui.min_threshold_lineEdit.setText("0")
                self.parent().ui.max_threshold_lineEdit.setText("65535")

            else :
                self.parent().ui.Activate_thresholding_checkBox.setEnabled(True)
                self.parent().ui.Save_active_frame_pushButton.setEnabled(True)
                self.parent().ui.frame_number_label.setEnabled(True)
                self.parent().ui.Picture_number_LineEdit.setEnabled(True)
                self.parent().ui.Picture_number_horizontalScrollBar.setEnabled(True)
                self.show_picture()

    def show_picture(self):
        if self.parent().ui.Input_Folder_Directory_Text.text() != '' :
            if len(self.impathes) == 0:
                return
            if self.pict_number == 0 :
                self.frame = np.asarray(Image.open(self.parent().ui.Input_Folder_Directory_Text.text()+'/slice00000.tif'))
                if self.parent().ui.Activate_thresholding_checkBox.isChecked() is True:
                    coordonates_in = self.GetPixelValueBetween(self.frame, int(self.parent().ui.min_threshold_lineEdit.text()), int(self.parent().ui.max_threshold_lineEdit.text()))
                    coordonates_out = self.GetPixelValueOutside(self.frame, int(self.parent().ui.min_threshold_lineEdit.text()), int(self.parent().ui.max_threshold_lineEdit.text()))
                    self.frame1 = self.ChangePixelValue(self.frame, coordonates_in, 2**16-1)
                    self.frame2 = self.ChangePixelValue(self.frame1, coordonates_out, 0)
            else:
                self.frame = np.asarray(Image.open(self.parent().ui.Input_Folder_Directory_Text.text()+'/slice'+str(self.pict_number)+'.tif'))
                if self.parent().ui.Activate_thresholding_checkBox.isChecked() is True:
                    coordonates_in = self.GetPixelValueBetween(self.frame, int(self.parent().ui.min_threshold_lineEdit.text()), int(self.parent().ui.max_threshold_lineEdit.text()))
                    coordonates_out = self.GetPixelValueOutside(self.frame, int(self.parent().ui.min_threshold_lineEdit.text()), int(self.parent().ui.max_threshold_lineEdit.text()))
                    self.frame1 = self.ChangePixelValue(self.frame, coordonates_in, 2**16-1)
                    self.frame2 = self.ChangePixelValue(self.frame1, coordonates_out, 0)
            h,w = self.frame.shape
            if self.parent().ui.Activate_thresholding_checkBox.isChecked() is True:
                im = QImage(self.frame2.data,w,h,QImage.Format_Grayscale16)

            if self.parent().ui.Activate_thresholding_checkBox.isChecked() is False:
                im = QImage(self.frame.data,w,h,QImage.Format_Grayscale16)

            self.pixmap = QPixmap(im)
            self.parent().ui.Picture_Display.setPixmap(self.pixmap)

    def picture_number(self):
        a = self.parent().ui.Picture_number_horizontalScrollBar.value()

        if len(self.impathes) == 0:
            return
        if a < 10 :
            self.pict_number = "0000"+str(a)
        elif a < 100 :
            self.pict_number = "000"+str(a)
        elif a < 1000 :
            self.pict_number = "00"+str(a)
        elif a < 10000 :
            self.pict_number = "0"+str(a)
        elif a < 100000 :
            self.pict_number = str(a)

        self.show_picture()

    def line_edit(self):
        self.parent().ui.Picture_number_LineEdit.setText(str(self.parent().ui.Picture_number_horizontalScrollBar.value()))
        self.parent().ui.min_threshold_lineEdit.setText(str(self.parent().ui.min_threshold_horizontalScrollBar.value()))
        self.parent().ui.max_threshold_lineEdit.setText(str(self.parent().ui.max_threshold_horizontalScrollBar.value()))


    def horizontal_slide_bar(self):
        if self.parent().ui.Picture_number_LineEdit.text() == "":
            self.parent().ui.Picture_number_LineEdit.setText("0")
        if self.parent().ui.Picture_number_LineEdit.text() == "-":
            self.parent().ui.Picture_number_LineEdit.setText("0")
        if self.parent().ui.Picture_number_LineEdit.text() == "+":
            self.parent().ui.Picture_number_LineEdit.setText("0")
        if int(self.parent().ui.Picture_number_LineEdit.text()) < 0 :
            self.parent().ui.Picture_number_LineEdit.setText("0")
        if self.parent().ui.Input_Folder_Directory_Text.text() == '':
            if int(self.parent().ui.Picture_number_LineEdit.text()) > 99 :
                self.parent().ui.Picture_number_LineEdit.setText("99")
        else :
            if int(self.parent().ui.Picture_number_LineEdit.text()) > len(self.impathes) :
                self.parent().ui.Picture_number_LineEdit.setText(str(len(self.impathes)))
        self.parent().ui.Picture_number_horizontalScrollBar.setValue(int(self.parent().ui.Picture_number_LineEdit.text()))

        if self.parent().ui.min_threshold_lineEdit.text() == "":
            self.parent().ui.min_threshold_lineEdit.setText("0")
        if self.parent().ui.min_threshold_lineEdit.text() == "-":
            self.parent().ui.min_threshold_lineEdit.setText("0")
        if self.parent().ui.min_threshold_lineEdit.text() == "+":
            self.parent().ui.min_threshold_lineEdit.setText("0")
        if int(self.parent().ui.min_threshold_lineEdit.text()) < 0 :
            self.parent().ui.min_threshold_lineEdit.setText("0")
        if int(self.parent().ui.min_threshold_lineEdit.text()) > 65535 :
            self.parent().ui.min_threshold_lineEdit.setText("65535")
        self.parent().ui.min_threshold_horizontalScrollBar.setValue(int(self.parent().ui.min_threshold_lineEdit.text()))

        if self.parent().ui.max_threshold_lineEdit.text() == "":
            self.parent().ui.max_threshold_lineEdit.setText("0")
        if self.parent().ui.max_threshold_lineEdit.text() == "-":
            self.parent().ui.max_threshold_lineEdit.setText("0")
        if self.parent().ui.max_threshold_lineEdit.text() == "+":
            self.parent().ui.max_threshold_lineEdit.setText("0")
        if int(self.parent().ui.max_threshold_lineEdit.text()) < 0 :
            self.parent().ui.max_threshold_lineEdit.setText("0")
        if int(self.parent().ui.max_threshold_lineEdit.text()) > 65535 :
            self.parent().ui.max_threshold_lineEdit.setText("65535")
        self.parent().ui.max_threshold_horizontalScrollBar.setValue(int(self.parent().ui.max_threshold_lineEdit.text()))


    def min_horizontal_slide_bar(self):
        if int(self.parent().ui.min_threshold_lineEdit.text()) > int(self.parent().ui.max_threshold_lineEdit.text()) :
            self.parent().ui.max_threshold_lineEdit.setText(self.parent().ui.min_threshold_lineEdit.text())

    def max_horizontal_slide_bar(self):
        if int(self.parent().ui.max_threshold_lineEdit.text()) < int(self.parent().ui.min_threshold_lineEdit.text()) :
            self.parent().ui.min_threshold_lineEdit.setText(self.parent().ui.max_threshold_lineEdit.text())

    def Activate_thresholding_State(self):
        if self.parent().ui.Activate_thresholding_checkBox.isChecked() is True :
            self.parent().ui.min_threshold_horizontalScrollBar.setEnabled(True)
            self.parent().ui.max_threshold_horizontalScrollBar.setEnabled(True)
            self.parent().ui.min_threshold_lineEdit.setEnabled(True)
            self.parent().ui.max_threshold_lineEdit.setEnabled(True)
            self.parent().ui.min_threshold_Label.setEnabled(True)
            self.parent().ui.max_threshold_Label.setEnabled(True)
        else:
            self.parent().ui.min_threshold_horizontalScrollBar.setEnabled(False)
            self.parent().ui.max_threshold_horizontalScrollBar.setEnabled(False)
            self.parent().ui.min_threshold_lineEdit.setEnabled(False)
            self.parent().ui.max_threshold_lineEdit.setEnabled(False)
            self.parent().ui.min_threshold_Label.setEnabled(False)
            self.parent().ui.max_threshold_Label.setEnabled(False)

    def Save_picture(self):
        saved_frame = str(QFileDialog.getSaveFileName(None, "Select Saving Directory"))
        if str(saved_frame) == str(('', '')):
            return
        if self.parent().ui.Activate_thresholding_checkBox.isChecked() is True:
            ims = Image.fromarray(self.frame2)
        if self.parent().ui.Activate_thresholding_checkBox.isChecked() is False:
            ims = Image.fromarray(self.frame)
        ims.save(saved_frame[2:-19]+".tif", compression = None)

    def connections(self):

        self.parent().ui.Picture_number_LineEdit.textChanged.connect(self.horizontal_slide_bar)
        self.parent().ui.Input_Folder_Directory_Text.textChanged.connect(self.Input_dir)
        self.parent().ui.Picture_number_horizontalScrollBar.valueChanged.connect(self.line_edit)
        self.parent().ui.Picture_number_LineEdit.textChanged.connect(self.picture_number)
        self.parent().ui.min_threshold_horizontalScrollBar.valueChanged.connect(self.line_edit)
        self.parent().ui.max_threshold_horizontalScrollBar.valueChanged.connect(self.line_edit)
        self.parent().ui.min_threshold_horizontalScrollBar.valueChanged.connect(self.min_horizontal_slide_bar)
        self.parent().ui.max_threshold_horizontalScrollBar.valueChanged.connect(self.max_horizontal_slide_bar)
        self.parent().ui.max_threshold_lineEdit.textChanged.connect(self.horizontal_slide_bar)
        self.parent().ui.min_threshold_lineEdit.textChanged.connect(self.horizontal_slide_bar)
        self.parent().ui.Activate_thresholding_checkBox.stateChanged.connect(self.Activate_thresholding_State)
        self.parent().ui.Activate_thresholding_checkBox.stateChanged.connect(self.show_picture)
        self.parent().ui.max_threshold_lineEdit.textChanged.connect(self.show_picture)
        self.parent().ui.min_threshold_lineEdit.textChanged.connect(self.show_picture)
        self.parent().ui.Save_active_frame_pushButton.clicked.connect(self.Save_picture)


class Parameters(QtCore.QObject):
    def __init__(self, parent):
        super(Parameters, self).__init__(parent)
        self.init_Parameters()
        self.connections()

    def init_Parameters(self):
        self.onlyInt = QIntValidator()
        self.parent().ui.X_Size_Text_Box.setValidator(self.onlyInt)
        self.parent().ui.X_Size_Text_Box.setInputMask("999.99999999")
        self.parent().ui.X_Size_Text_Box.setAlignment(Qt.AlignLeft)
        self.parent().ui.Y_Size_Text_Box.setValidator(self.onlyInt)
        self.parent().ui.Y_Size_Text_Box.setInputMask("999.99999999")
        self.parent().ui.Y_Size_Text_Box.setAlignment(Qt.AlignLeft)
        self.parent().ui.Z_Size_Text_Box.setValidator(self.onlyInt)
        self.parent().ui.Z_Size_Text_Box.setInputMask("999.99999999")
        self.parent().ui.Z_Size_Text_Box.setAlignment(Qt.AlignLeft)
        self.parent().ui.Opening_Size_X_Text_Box.setValidator(self.onlyInt)
        self.parent().ui.Opening_Size_Y_Text_Box.setValidator(self.onlyInt)
        self.parent().ui.Text_Voxel_State = " /!\ Fill voxel value /!\ "
        self.parent().ui.Text_Opening_State = " /!\  Fill with value  /!\ "
        self.parent().ui.Picture_number_LineEdit.setValidator(self.onlyInt)
        self.parent().ui.min_threshold_lineEdit.setValidator(self.onlyInt)
        self.parent().ui.max_threshold_lineEdit.setValidator(self.onlyInt)
        self.parent().ui.Gradient_value_lineEdit.setValidator(self.onlyInt)
        self.parent().ui.Gradient_value_lineEdit.setInputMask("9.999")
        self.parent().ui.Gradient_value_lineEdit.setText("0.0")

    def activation(self):
        if self.parent().ui.Opening_Check_Box.isChecked() == True:
            self.parent().ui.Opening_Methode_Label.setEnabled(True)
            self.parent().ui.Opening_Methode_Combo_Box.setEnabled(True)
            self.parent().ui.Opening_Size_Label.setEnabled(True)
            self.parent().ui.Opening_Size_X_Text_Box.setEnabled(True)
            self.parent().ui.Opening_Size_Y_Text_Box.setEnabled(True)
            self.parent().ui.X_opening_label.setEnabled(True)
            self.parent().ui.Y_opening_label.setEnabled(True)
        else:
            self.parent().ui.Opening_Methode_Label.setEnabled(False)
            self.parent().ui.Opening_Methode_Combo_Box.setEnabled(False)
            self.parent().ui.Opening_Size_Label.setEnabled(False)
            self.parent().ui.Opening_Size_X_Text_Box.setEnabled(False)
            self.parent().ui.Opening_Size_Y_Text_Box.setEnabled(False)
            self.parent().ui.Opening_Size_X_Text_Box.setText("1")
            self.parent().ui.Opening_Size_Y_Text_Box.setText("1")
            self.parent().ui.X_opening_label.setEnabled(False)
            self.parent().ui.Y_opening_label.setEnabled(False)

        if self.parent().ui.Borders_Off_Check_Box.isChecked() == True:
            self.parent().ui.Gradient_value_label.setEnabled(True)
            self.parent().ui.Gradient_value_lineEdit.setEnabled(True)

        else:
            self.parent().ui.Gradient_value_label.setEnabled(False)
            self.parent().ui.Gradient_value_lineEdit.setEnabled(False)
            self.parent().ui.Visualize_border_off_pushButton.setEnabled(False)

    def Check_Combo_Box_State (self, Check_Box) :
        if Check_Box.currentText() == "Without overlap":
            self.parent().ui.Connectivity_Check_Box.setEnabled(True)
            self.parent().ui.Save_STL_Check_Box.setEnabled(True)
        else :
            self.parent().ui.Connectivity_Check_Box.setEnabled(False)
            self.parent().ui.Connectivity_Check_Box.setChecked(False)
            self.parent().ui.Save_STL_Check_Box.setEnabled(False)
            self.parent().ui.Save_STL_Check_Box.setChecked(False)

    def Auto_Voxel_Filling (self):
        Voxel_text = self.parent().ui.X_Size_Text_Box.text()
        self.parent().ui.Y_Size_Text_Box.setText(Voxel_text)
        self.parent().ui.Z_Size_Text_Box.setText(Voxel_text)


    def Voxel_State (self):
        if (any([self.parent().ui.X_Size_Text_Box.text() == ".",
                 self.parent().ui.Y_Size_Text_Box.text() == ".",
                 self.parent().ui.Z_Size_Text_Box.text() == "."])):
            self.parent().ui.Text_Voxel_State = " /!\ Fill voxel value /!\ "
            self.parent().ui.Voxel_State_Value = 1
        elif (any([float(self.parent().ui.X_Size_Text_Box.text()) == 0.0,
                   float(self.parent().ui.Y_Size_Text_Box.text()) == 0.0,
                   float(self.parent().ui.Z_Size_Text_Box.text()) == 0.0])):
            self.parent().ui.Text_Voxel_State = " /!\ Value has to be =/= 0.0 /!\ "
            self.parent().ui.Voxel_State_Value = 1
        elif (any([float(self.parent().ui.X_Size_Text_Box.text()) < 0.0,
                   float(self.parent().ui.Y_Size_Text_Box.text()) < 0.0,
                   float(self.parent().ui.Z_Size_Text_Box.text()) < 0.0])):
            self.parent().ui.Text_Voxel_State = " /!\ Value has to be greater than 0 /!\ "
            self.parent().ui.Voxel_State_Value = 1
        else :
            self.parent().ui.Text_Voxel_State = " All good "
            self.parent().ui.Voxel_State_Value = 0

    def Opening_State (self):
        if (any([self.parent().ui.Opening_Size_X_Text_Box.text() == "",
                 self.parent().ui.Opening_Size_Y_Text_Box.text() == ""])):
            self.parent().ui.Text_Opening_State = " /!\  Fill with value  /!\ "
            self.parent().ui.Opening_State_Value = 1
        elif (any([int(self.parent().ui.Opening_Size_X_Text_Box.text()) == 0,
                 int(self.parent().ui.Opening_Size_Y_Text_Box.text()) == 0])):
            self.parent().ui.Text_Opening_State = " /!\ Not 0 /!\ "
            self.parent().ui.Opening_State_Value = 1
        else :
            self.parent().ui.Text_Opening_State = " All good "
            self.parent().ui.Opening_State_Value = 0

    def Borders_Off_value (self):
        if self.parent().ui.Gradient_value_lineEdit.text() =='.':
            self.parent().ui.Gradient_value_lineEdit.setText("0.0")

        if float(self.parent().ui.Gradient_value_lineEdit.text()) != 0.0 and len(self.parent().Picture.impathes) !=0 and self.parent().ui.Borders_Off_Check_Box.isChecked() == True :
            self.parent().ui.Visualize_border_off_pushButton.setEnabled(True)
        else :
            self.parent().ui.Visualize_border_off_pushButton.setEnabled(False)


    def connections(self):
        self.parent().ui.Opening_Check_Box.stateChanged.connect(self.activation)
        self.parent().ui.Borders_Off_Check_Box.stateChanged.connect(self.activation)
        self.parent().ui.Segmentation_Methode_1_ComboBox.currentTextChanged.connect(lambda : self.Check_Combo_Box_State(self.parent().ui.Segmentation_Methode_1_ComboBox))
        self.parent().ui.Opening_Check_Box.stateChanged.connect(self.Opening_State)
        self.parent().ui.Opening_Size_X_Text_Box.textChanged.connect(self.Opening_State)
        self.parent().ui.Opening_Size_Y_Text_Box.textChanged.connect(self.Opening_State)
        self.parent().ui.X_Size_Text_Box.textChanged.connect(self.Auto_Voxel_Filling)
        self.parent().ui.X_Size_Text_Box.textChanged.connect(self.Voxel_State)
        self.parent().ui.Y_Size_Text_Box.textChanged.connect(self.Voxel_State)
        self.parent().ui.Z_Size_Text_Box.textChanged.connect(self.Voxel_State)
        self.parent().ui.Gradient_value_lineEdit.textChanged.connect(self.Borders_Off_value)
        self.parent().ui.Input_Folder_Directory_Text.textChanged.connect(self.Borders_Off_value)
        self.parent().ui.Borders_Off_Check_Box.stateChanged.connect(self.Borders_Off_value)

class CheckableComboBox(QComboBox):
    def __init__(self):
        super(CheckableComboBox, self).__init__()
        self.view().pressed.connect(self.handle_item_pressed)
        self.setModel(QStandardItemModel(self))

    def handle_item_pressed(self, index):
        item = self.model().itemFromIndex(index)
        if item.checkState() == Qt.Checked:
            item.setCheckState(Qt.Unchecked)
        else:
            item.setCheckState(Qt.Checked)
        self.check_items()

    def item_checked(self, index):
        item = self.model().item(index, 0)
        return item.checkState() == Qt.Checked

    def check_items(self):
        checkedItems = []
        for i in range(self.count()):
            if self.item_checked(i):
                checkedItems.append(i)
    sys.stdout.flush()

class UpdateCheckableComboBox(QtCore.QObject):
    def __init__(self, parent):
        super(UpdateCheckableComboBox, self).__init__(parent)

    def Saved_Objects (self) :
        self.parent().ui.STL_Display_Combo_Box.clear()
        self.parent().ui.Save_STL_Combo_Box.clear()
        self.parent().ui.Save_Frames_Combo_Box.clear()
        Item_Combo_Box_List = [None]*self.parent().ObjectList.Object_List_Create_Counter_i
        if self.parent().ObjectList.Object_List_Create_Counter_i == 1 :
            if self.parent().ui.Object_List_SpinBox_0.value() == 1 :
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 2 :
            if self.parent().ui.Object_List_SpinBox_0.value() == 2 :
                self.parent().ui.Object_List_SpinBox_0.setValue(1)
            if self.parent().ui.Object_List_SpinBox_1.value() == 2 :
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 3 :
            if self.parent().ui.Object_List_SpinBox_0.value() == 3 :
                self.parent().ui.Object_List_SpinBox_0.setValue(2)
            if self.parent().ui.Object_List_SpinBox_1.value() == 3 :
                self.parent().ui.Object_List_SpinBox_1.setValue(2)
            if self.parent().ui.Object_List_SpinBox_2.value() == 3 :
                self.parent().ui.Object_List_SpinBox_2.setValue(2)
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 4 :
            if self.parent().ui.Object_List_SpinBox_0.value() == 4 :
                self.parent().ui.Object_List_SpinBox_0.setValue(3)
            if self.parent().ui.Object_List_SpinBox_1.value() == 4 :
                self.parent().ui.Object_List_SpinBox_1.setValue(3)
            if self.parent().ui.Object_List_SpinBox_2.value() == 4 :
                self.parent().ui.Object_List_SpinBox_2.setValue(3)
            if self.parent().ui.Object_List_SpinBox_3.value() == 4 :
                self.parent().ui.Object_List_SpinBox_3.setValue(3)
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 5 :
            if self.parent().ui.Object_List_SpinBox_0.value() == 5 :
                self.parent().ui.Object_List_SpinBox_0.setValue(4)
            if self.parent().ui.Object_List_SpinBox_1.value() == 5 :
                self.parent().ui.Object_List_SpinBox_1.setValue(4)
            if self.parent().ui.Object_List_SpinBox_2.value() == 5 :
                self.parent().ui.Object_List_SpinBox_2.setValue(4)
            if self.parent().ui.Object_List_SpinBox_3.value() == 5 :
                self.parent().ui.Object_List_SpinBox_3.setValue(4)
            if self.parent().ui.Object_List_SpinBox_4.value() == 5 :
                self.parent().ui.Object_List_SpinBox_4.setValue(4)
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 6 :
            if self.parent().ui.Object_List_SpinBox_0.value() == 6 :
                self.parent().ui.Object_List_SpinBox_0.setValue(5)
            if self.parent().ui.Object_List_SpinBox_1.value() == 6 :
                self.parent().ui.Object_List_SpinBox_1.setValue(5)
            if self.parent().ui.Object_List_SpinBox_2.value() == 6 :
                self.parent().ui.Object_List_SpinBox_2.setValue(5)
            if self.parent().ui.Object_List_SpinBox_3.value() == 6 :
                self.parent().ui.Object_List_SpinBox_3.setValue(5)
            if self.parent().ui.Object_List_SpinBox_4.value() == 6 :
                self.parent().ui.Object_List_SpinBox_4.setValue(5)
            if self.parent().ui.Object_List_SpinBox_5.value() == 6 :
                self.parent().ui.Object_List_SpinBox_5.setValue(5)
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 7 :
            if self.parent().ui.Object_List_SpinBox_0.value() == 7 :
                self.parent().ui.Object_List_SpinBox_0.setValue(6)
            if self.parent().ui.Object_List_SpinBox_1.value() == 7 :
                self.parent().ui.Object_List_SpinBox_1.setValue(6)
            if self.parent().ui.Object_List_SpinBox_2.value() == 7 :
                self.parent().ui.Object_List_SpinBox_2.setValue(6)
            if self.parent().ui.Object_List_SpinBox_3.value() == 7 :
                self.parent().ui.Object_List_SpinBox_3.setValue(6)
            if self.parent().ui.Object_List_SpinBox_4.value() == 7 :
                self.parent().ui.Object_List_SpinBox_4.setValue(6)
            if self.parent().ui.Object_List_SpinBox_5.value() == 7 :
                self.parent().ui.Object_List_SpinBox_5.setValue(6)
            if self.parent().ui.Object_List_SpinBox_6.value() == 7 :
                self.parent().ui.Object_List_SpinBox_6.setValue(6)
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_6.value()] = self.parent().ui.Object_List_LineEdited_6.text()
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 8 :
            if self.parent().ui.Object_List_SpinBox_0.value() == 8 :
                self.parent().ui.Object_List_SpinBox_0.setValue(7)
            if self.parent().ui.Object_List_SpinBox_1.value() == 8 :
                self.parent().ui.Object_List_SpinBox_1.setValue(7)
            if self.parent().ui.Object_List_SpinBox_2.value() == 8 :
                self.parent().ui.Object_List_SpinBox_2.setValue(7)
            if self.parent().ui.Object_List_SpinBox_3.value() == 8 :
                self.parent().ui.Object_List_SpinBox_3.setValue(7)
            if self.parent().ui.Object_List_SpinBox_4.value() == 8 :
                self.parent().ui.Object_List_SpinBox_4.setValue(7)
            if self.parent().ui.Object_List_SpinBox_5.value() == 8 :
                self.parent().ui.Object_List_SpinBox_5.setValue(7)
            if self.parent().ui.Object_List_SpinBox_6.value() == 8 :
                self.parent().ui.Object_List_SpinBox_6.setValue(7)
            if self.parent().ui.Object_List_SpinBox_7.value() == 8 :
                self.parent().ui.Object_List_SpinBox_7.setValue(7)
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_6.value()] = self.parent().ui.Object_List_LineEdited_6.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_7.value()] = self.parent().ui.Object_List_LineEdited_7.text()
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 9 :
            if self.parent().ui.Object_List_SpinBox_0.value() == 9 :
                self.parent().ui.Object_List_SpinBox_0.setValue(8)
            if self.parent().ui.Object_List_SpinBox_1.value() == 9 :
                self.parent().ui.Object_List_SpinBox_1.setValue(8)
            if self.parent().ui.Object_List_SpinBox_2.value() == 9 :
                self.parent().ui.Object_List_SpinBox_2.setValue(8)
            if self.parent().ui.Object_List_SpinBox_3.value() == 9 :
                self.parent().ui.Object_List_SpinBox_3.setValue(8)
            if self.parent().ui.Object_List_SpinBox_4.value() == 9 :
                self.parent().ui.Object_List_SpinBox_4.setValue(8)
            if self.parent().ui.Object_List_SpinBox_5.value() == 9 :
                self.parent().ui.Object_List_SpinBox_5.setValue(8)
            if self.parent().ui.Object_List_SpinBox_6.value() == 9 :
                self.parent().ui.Object_List_SpinBox_6.setValue(8)
            if self.parent().ui.Object_List_SpinBox_7.value() == 9 :
                self.parent().ui.Object_List_SpinBox_7.setValue(8)
            if self.parent().ui.Object_List_SpinBox_8.value() == 9 :
                self.parent().ui.Object_List_SpinBox_8.setValue(8)
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_6.value()] = self.parent().ui.Object_List_LineEdited_6.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_7.value()] = self.parent().ui.Object_List_LineEdited_7.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_8.value()] = self.parent().ui.Object_List_LineEdited_8.text()
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 10 :
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_6.value()] = self.parent().ui.Object_List_LineEdited_6.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_7.value()] = self.parent().ui.Object_List_LineEdited_7.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_8.value()] = self.parent().ui.Object_List_LineEdited_8.text()
            Item_Combo_Box_List[self.parent().ui.Object_List_SpinBox_9.value()] = self.parent().ui.Object_List_LineEdited_9.text()

        for i in range(len(Item_Combo_Box_List)):
            self.parent().ui.STL_Display_Combo_Box.addItem(Item_Combo_Box_List[i])
            item = self.parent().ui.STL_Display_Combo_Box.model().item(i, 0)
            item.setCheckState(Qt.Checked)
        self.parent().ui.STL_Display_horizontal_Combo_Box_Layout.addWidget(self.parent().ui.STL_Display_Combo_Box)
        for i in range(len(Item_Combo_Box_List)):
            self.parent().ui.Save_STL_Combo_Box.addItem(Item_Combo_Box_List[i])
            item = self.parent().ui.Save_STL_Combo_Box.model().item(i, 0)
            item.setCheckState(Qt.Checked)
        self.parent().ui.Save_STL_horizontal_Combo_Box_Layout.addWidget(self.parent().ui.Save_STL_Combo_Box)
        for i in range(len(Item_Combo_Box_List)):
            self.parent().ui.Save_Frames_Combo_Box.addItem(Item_Combo_Box_List[i])
            item = self.parent().ui.Save_Frames_Combo_Box.model().item(i, 0)
            item.setCheckState(Qt.Checked)
        self.parent().ui.Save_Frames_horizontal_Combo_Box_Layout.addWidget(self.parent().ui.Save_Frames_Combo_Box)

    def connections(self):
        self.parent().ui.Object_List_Add_Button.clicked.connect(self.Saved_Objects)


class SaveFrames(QtCore.QObject):
    def __init__(self, parent):
        super(SaveFrames, self).__init__(parent)
        self.init_Parameters()
        self.connections()

    def init_Parameters(self):
        self.parent().ui.Save_Frames_Combo_Box_Widget_Label = QtWidgets.QLabel(self.parent().ui.Save_Frame_GroupBox)
        self.parent().ui.Save_Frames_Combo_Box_Widget_Label.setObjectName("Save_Frames_Combo_Box_Widget_Label")
        self.parent().ui.Save_Frames_Combo_Box_Widget_Label.setDisabled(False)
        self.parent().ui.Save_Frames_Combo_Box_Widget_Label.setGeometry(QtCore.QRect(10, 50, 170, 16))
        self.parent().ui.Save_Frames_Combo_Box_Widget = QtWidgets.QWidget(self.parent().ui.Save_Frame_GroupBox)
        self.parent().ui.Save_Frames_Combo_Box_Widget.setGeometry(QtCore.QRect(10, 70, 100, 30))
        self.parent().ui.Save_Frames_Combo_Box_Widget.setObjectName("Save_Frames_Combo_Box_Widget")
        self.parent().ui.Save_Frames_horizontal_Combo_Box_Layout = QtWidgets.QHBoxLayout(self.parent().ui.Save_Frames_Combo_Box_Widget)
        self.parent().ui.Save_Frames_horizontal_Combo_Box_Layout.setContentsMargins(0, 0, 0, 0)
        self.parent().ui.Save_Frames_horizontal_Combo_Box_Layout.setObjectName("Save_Frames_horizontal_Combo_Box_Layout")
        self.parent().ui.Save_Frames_Combo_Box = CheckableComboBox()
        self.parent().ui.Save_Frames_Combo_Box.setDisabled(False)
        self.parent().ui.Save_Frames_horizontal_Combo_Box_Layout.addWidget(self.parent().ui.Save_Frames_Combo_Box)

    def activation(self):
        if self.parent().ui.Save_Frames_Check_Box.isChecked() == True:
            self.parent().ui.Save_Frame_Methode_Combo_Box.setEnabled(True)
            self.parent().ui.Save_Frame_Methode_Label.setEnabled(True)
            self.parent().ui.Save_Frames_Combo_Box_Widget_Label.setEnabled(True)
            self.parent().ui.Save_Frames_Combo_Box.setEnabled(True)

        else:
            self.parent().ui.Save_Frame_Methode_Combo_Box.setEnabled(False)
            self.parent().ui.Save_Frame_Methode_Label.setEnabled(False)
            self.parent().ui.Save_Frames_Combo_Box_Widget_Label.setEnabled(False)
            self.parent().ui.Save_Frames_Combo_Box.setEnabled(False)

    def connections(self):
        self.parent().ui.Save_Frames_Check_Box.stateChanged.connect(self.activation)

class StlSettings(QtCore.QObject):
    def __init__(self, parent):
        super(StlSettings, self).__init__(parent)
        self.init_Parameters()
        self.connections()

    def init_Parameters(self):
        self.parent().ui.Reduction_Coefficient_Text_Box.setValidator(self.parent().Parameters.onlyInt)
        self.parent().ui.Reduction_Coefficient_Text_Box.setInputMask("9.999")
        self.parent().ui.Save_STL_Combo_Box_Widget = QtWidgets.QWidget(self.parent().ui.STL_File_GroupBox)
        self.parent().ui.Save_STL_Combo_Box_Widget.setGeometry(QtCore.QRect(20, 60, 100, 30))
        self.parent().ui.Save_STL_Combo_Box_Widget.setObjectName("Save_STL_Combo_Box_Widget")
        self.parent().ui.Save_STL_horizontal_Combo_Box_Layout = QtWidgets.QHBoxLayout(self.parent().ui.Save_STL_Combo_Box_Widget)
        self.parent().ui.Save_STL_horizontal_Combo_Box_Layout.setContentsMargins(0, 0, 0, 0)
        self.parent().ui.Save_STL_horizontal_Combo_Box_Layout.setObjectName("Save_STL_horizontal_Combo_Box_Layout")
        self.parent().ui.Save_STL_Combo_Box = CheckableComboBox()
        self.parent().ui.Save_STL_Combo_Box.setDisabled(True)
        self.parent().ui.Save_STL_horizontal_Combo_Box_Layout.addWidget(self.parent().ui.Save_STL_Combo_Box)
        self.parent().ui.Combo_Box_Widget = QtWidgets.QWidget(self.parent().ui.STL_File_GroupBox)
        self.parent().ui.Combo_Box_Widget.setGeometry(QtCore.QRect(20, 180, 120, 30))
        self.parent().ui.Combo_Box_Widget.setObjectName("Combo_Box_Widget")
        self.parent().ui.horizontal_Combo_Box_Layout = QtWidgets.QHBoxLayout(self.parent().ui.Combo_Box_Widget)
        self.parent().ui.horizontal_Combo_Box_Layout.setContentsMargins(0, 0, 0, 0)
        self.parent().ui.horizontal_Combo_Box_Layout.setObjectName("horizontal_Combo_Box_Layout")
        self.parent().ui.STL_Save_Combo_Box = CheckableComboBox()
        self.parent().ui.STL_Save_Combo_Box.setDisabled(True)
        self.parent().ui.STL_Save_Combo_Box_List = ["Decimation", "Filling", "VTK Connectivity"]
        for i in range(len(self.parent().ui.STL_Save_Combo_Box_List)):
            self.parent().ui.STL_Save_Combo_Box.addItem(self.parent().ui.STL_Save_Combo_Box_List[i])
            item = self.parent().ui.STL_Save_Combo_Box.model().item(i, 0)
            if i == 0 :
                item.setCheckState(Qt.Checked)
            else :
                item.setCheckState(Qt.Unchecked)
        self.parent().ui.horizontal_Combo_Box_Layout.addWidget(self.parent().ui.STL_Save_Combo_Box)


    def activation(self, Check_Box):
        if Check_Box.text() == "Save .stl":
            if Check_Box.isChecked() == True:
                self.parent().ui.Display_STL_Check_Box.setEnabled(True)
                self.parent().ui.Decimation_Process_Check_Box.setEnabled(True)
                self.parent().ui.Mass_Center_Check_Box.setEnabled(True)
                self.parent().ui.Save_STL_Combo_Box_Widget_Label.setEnabled(True)
                self.parent().ui.Save_STL_Combo_Box.setEnabled(True)
            else:
                self.parent().ui.Display_STL_Check_Box.setEnabled(False)
                self.parent().ui.Show_Mass_Center_Check_Box.setEnabled(False)
                self.parent().ui.Decimation_Process_Check_Box.setEnabled(False)
                self.parent().ui.Mass_Center_Check_Box.setEnabled(False)
                self.parent().ui.Display_STL_Check_Box.setChecked(False)
                self.parent().ui.Decimation_Process_Check_Box.setChecked(False)
                self.parent().ui.Mass_Center_Check_Box.setChecked(False)
                self.parent().ui.Show_Mass_Center_Check_Box.setEnabled(False)
                self.parent().ui.Show_Mass_Center_Check_Box.setChecked(False)
                self.parent().ui.Save_STL_Combo_Box_Widget_Label.setEnabled(False)
                self.parent().ui.Save_STL_Combo_Box.setEnabled(False)

        if Check_Box.text() == "Decimation process":
            if Check_Box.isChecked() == True :
                self.parent().ui.Reduction_Coefficient_Label.setEnabled(True)
                self.parent().ui.Preserve_Topology_Check_Box.setEnabled(True)
                self.parent().ui.Combo_Box_Widget_Label.setEnabled(True)
                self.parent().ui.STL_Save_Combo_Box.setEnabled(True)
                self.parent().ui.Reduction_Coefficient_Text_Box.setEnabled(True)
            else:
                self.parent().ui.Reduction_Coefficient_Label.setEnabled(False)
                self.parent().ui.Preserve_Topology_Check_Box.setEnabled(False)
                self.parent().ui.Combo_Box_Widget_Label.setEnabled(False)
                self.parent().ui.STL_Save_Combo_Box.setEnabled(False)
                self.parent().ui.Reduction_Coefficient_Text_Box.setText("0.0")
                self.parent().ui.Reduction_Coefficient_Text_Box.setEnabled(False)
                self.parent().ui.Preserve_Topology_Check_Box.setChecked(False)

    def Reduction_State (self):

        if self.parent().ui.Reduction_Coefficient_Text_Box.text() =="." :
            self.parent().ui.Text_Reduction_Coefficient_State = " /!\  Value has to be a number  /!\ "
            self.parent().ui.Reduction_Coefficient_State_Value = 1
        elif float(self.parent().ui.Reduction_Coefficient_Text_Box.text()) == 0.0 :
            self.parent().ui.Text_Reduction_Coefficient_State = " /!\  Value has to be =/= 0.0  /!\ "
            self.parent().ui.Reduction_Coefficient_State_Value = 1
        elif float(self.parent().ui.Reduction_Coefficient_Text_Box.text()) >= 1.0 :
            self.parent().ui.Text_Reduction_Coefficient_State = " /!\  Value has to be < 1.0  /!\ "
            self.parent().ui.Reduction_Coefficient_State_Value = 1
        else :
            self.parent().ui.Text_Reduction_Coefficient_State = " All good "
            self.parent().ui.Reduction_Coefficient_State_Value = 0

    def connections(self):
        self.parent().ui.Save_STL_Check_Box.stateChanged.connect(lambda : self.activation(self.parent().ui.Save_STL_Check_Box))
        self.parent().ui.Decimation_Process_Check_Box.stateChanged.connect(lambda : self.activation(self.parent().ui.Decimation_Process_Check_Box))
        self.parent().ui.Reduction_Coefficient_Text_Box.textChanged.connect(self.Reduction_State)
        self.parent().ui.Decimation_Process_Check_Box.stateChanged.connect(self.Reduction_State)

class Display(QtCore.QObject) :
    def __init__(self, parent):
        super(Display, self).__init__(parent)
        self.init_Parameters()
        self.connections()

    def init_Parameters(self):
        self.parent().ui.Sphere_Size_Text_Box.setValidator(self.parent().Parameters.onlyInt)
        self.parent().ui.Sphere_Size_Text_Box.setInputMask("9.9")
        self.parent().ui.Sphere_Size_Text_Box.setText("4.0")
        self.parent().ui.STL_Display_Combo_Box_Widget = QtWidgets.QWidget(self.parent().ui.Display_GroupBox)
        self.parent().ui.STL_Display_Combo_Box_Widget.setGeometry(QtCore.QRect(200, 40, 100, 30))
        self.parent().ui.STL_Display_Combo_Box_Widget.setObjectName("STL_Display_Combo_Box_Widget")
        self.parent().ui.STL_Display_horizontal_Combo_Box_Layout = QtWidgets.QHBoxLayout(self.parent().ui.STL_Display_Combo_Box_Widget)
        self.parent().ui.STL_Display_horizontal_Combo_Box_Layout.setContentsMargins(0, 0, 0, 0)
        self.parent().ui.STL_Display_horizontal_Combo_Box_Layout.setObjectName("STL_Display_horizontal_Combo_Box_Layout")
        self.parent().ui.STL_Display_Combo_Box = CheckableComboBox()
        self.parent().ui.STL_Display_Combo_Box.setDisabled(True)
        self.parent().ui.STL_Display_horizontal_Combo_Box_Layout.addWidget(self.parent().ui.STL_Display_Combo_Box)

    def Sphere_Size_State (self):

        if self.parent().ui.Sphere_Size_Text_Box.text() == "." :
            self.parent().ui.Text_Sphere_Size_State = " /!\ Fill sphere value /!\ "
            self.parent().ui.Sphere_Size_State_Value = 1

        elif float(self.parent().ui.Sphere_Size_Text_Box.text()) == 0.0 :
            self.parent().ui.Text_Sphere_Size_State = " /!\ Value has to be =/= 0.0 /!\ "
            self.parent().ui.Sphere_Size_State_Value = 1

        elif float(self.parent().ui.Sphere_Size_Text_Box.text()) < 0.0 :
            self.parent().ui.Text_Sphere_Size_State = " /!\ Value has to be greater than 0 /!\ "
            self.parent().ui.Sphere_Size_State_Value = 1

        else :
            self.parent().ui.Text_Sphere_Size_State = " All good "
            self.parent().ui.Sphere_Size_State_Value = 0

    def activation(self, Check_Box):
        if Check_Box.text() == "Display stl":
            if Check_Box.isChecked() == True and self.parent().ui.Mass_Center_Check_Box.isChecked() == True :
                self.parent().ui.Show_Mass_Center_Check_Box.setEnabled(True)
            else :
                self.parent().ui.Show_Mass_Center_Check_Box.setEnabled(False)
                self.parent().ui.Show_Mass_Center_Check_Box.setChecked(False)
            if Check_Box.isChecked() == True :
                self.parent().ui.STL_Display_Combo_Box_Widget_Label.setEnabled(True)
                self.parent().ui.STL_Display_Combo_Box.setEnabled(True)
            else :
                self.parent().ui.STL_Display_Combo_Box_Widget_Label.setEnabled(False)
                self.parent().ui.STL_Display_Combo_Box.setEnabled(False)

        if Check_Box.text() == "Calculate stl's mass center":
            if Check_Box.isChecked() == True and self.parent().ui.Display_STL_Check_Box.isChecked() == True :
                self.parent().ui.Show_Mass_Center_Check_Box.setEnabled(True)
            else:
                self.parent().ui.Show_Mass_Center_Check_Box.setEnabled(False)
                self.parent().ui.Show_Mass_Center_Check_Box.setChecked(False)

        if Check_Box.text() == "Show mass center":
            if Check_Box.isChecked() == True :
                self.parent().ui.Sphere_Size_Text_Box.setEnabled(True)
            else:
                self.parent().ui.Sphere_Size_Text_Box.setEnabled(False)
                self.parent().ui.Sphere_Size_Text_Box.setText("2.0")


    def connections(self):
        self.parent().ui.Display_STL_Check_Box.stateChanged.connect(lambda : self.activation(self.parent().ui.Display_STL_Check_Box))
        self.parent().ui.Mass_Center_Check_Box.stateChanged.connect(lambda : self.activation(self.parent().ui.Mass_Center_Check_Box))
        self.parent().ui.Show_Mass_Center_Check_Box.stateChanged.connect(lambda : self.activation(self.parent().ui.Show_Mass_Center_Check_Box))
        self.parent().ui.Show_Mass_Center_Check_Box.stateChanged.connect(self.Sphere_Size_State)
        self.parent().ui.Sphere_Size_Text_Box.textChanged.connect(self.Sphere_Size_State)

class Manual_parameters(QtCore.QObject):
    def __init__(self, parent):
        super(Manual_parameters, self).__init__(parent)
        self.init_parameters()
        self.connections()

    def init_parameters(self):
        self.parent().ui.Manual_obj_label.setEnabled(False)
        self.parent().ui.Manual_max_label.setEnabled(False)
        self.parent().ui.Manual_min_label.setEnabled(False)

    def connections(self):
        self.parent().ui.Manual_checkBox.stateChanged.connect(self.enabling)

    def enabling(self):
        if self.parent().ui.Manual_checkBox.isChecked() is True :
            self.parent().ui.Manual_obj_label.setEnabled(True)
            self.parent().ui.Manual_max_label.setEnabled(True)
            self.parent().ui.Manual_min_label.setEnabled(True)
            self.parent().ui.Segmentation_Methode_1_Label.setEnabled(False)
            self.parent().ui.Segmentation_Methode_2_Label.setEnabled(False)
            self.parent().ui.Segmentation_Methode_1_ComboBox.setEnabled(False)
            self.parent().ui.Segmentation_Methode_2_ComboBox.setEnabled(False)
            if self.parent().ObjectList.Object_List_Create_Counter_i == 1 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)

            elif self.parent().ObjectList.Object_List_Create_Counter_i == 2 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_label1.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 3 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_2.setValue(2)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_label1.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_label2.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 4 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_2.setValue(2)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_3.setValue(3)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_label1.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_label2.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_label3.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 5 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_2.setValue(2)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_3.setValue(3)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_4.setValue(4)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_label1.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_label2.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_label3.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_label4.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 6 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_2.setValue(2)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_3.setValue(3)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_4.setValue(4)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_5.setValue(5)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_label1.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_label2.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_label3.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_label4.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_label5.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 7 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_2.setValue(2)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_3.setValue(3)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_4.setValue(4)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_5.setValue(5)
                self.parent().ui.Object_List_SpinBox_6.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_6.setValue(6)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_label1.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_label2.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_label3.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_label4.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_label5.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
                self.parent().ui.Manual_obj_label6.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(True)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 8 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_2.setValue(2)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_3.setValue(3)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_4.setValue(4)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_5.setValue(5)
                self.parent().ui.Object_List_SpinBox_6.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_6.setValue(6)
                self.parent().ui.Object_List_SpinBox_7.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_7.setValue(7)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_label1.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_label2.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_label3.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_label4.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_label5.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
                self.parent().ui.Manual_obj_label6.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(True)
                self.parent().ui.Manual_obj_label7.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(True)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 9 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_2.setValue(2)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_3.setValue(3)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_4.setValue(4)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_5.setValue(5)
                self.parent().ui.Object_List_SpinBox_6.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_6.setValue(6)
                self.parent().ui.Object_List_SpinBox_7.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_7.setValue(7)
                self.parent().ui.Object_List_SpinBox_8.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_8.setValue(8)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_label1.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_label2.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_label3.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_label4.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_label5.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
                self.parent().ui.Manual_obj_label6.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(True)
                self.parent().ui.Manual_obj_label7.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(True)
                self.parent().ui.Manual_obj_label8.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_8.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_8.setEnabled(True)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 10 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_0.setValue(0)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_1.setValue(1)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_2.setValue(2)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_3.setValue(3)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_4.setValue(4)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_5.setValue(5)
                self.parent().ui.Object_List_SpinBox_6.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_6.setValue(6)
                self.parent().ui.Object_List_SpinBox_7.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_7.setValue(7)
                self.parent().ui.Object_List_SpinBox_8.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_8.setValue(8)
                self.parent().ui.Object_List_SpinBox_9.setEnabled(False)
                self.parent().ui.Object_List_SpinBox_9.setValue(9)
                self.parent().ui.Manual_obj_label0.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
                self.parent().ui.Manual_obj_label1.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
                self.parent().ui.Manual_obj_label2.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
                self.parent().ui.Manual_obj_label3.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
                self.parent().ui.Manual_obj_label4.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
                self.parent().ui.Manual_obj_label5.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
                self.parent().ui.Manual_obj_label6.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(True)
                self.parent().ui.Manual_obj_label7.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(True)
                self.parent().ui.Manual_obj_label8.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_8.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_8.setEnabled(True)
                self.parent().ui.Manual_obj_label9.setEnabled(True)
                self.parent().ui.Manual_obj_min_LineEdited_9.setEnabled(True)
                self.parent().ui.Manual_obj_max_LineEdited_9.setEnabled(True)
        else :
            self.parent().ui.Manual_obj_label.setEnabled(False)
            self.parent().ui.Manual_max_label.setEnabled(False)
            self.parent().ui.Manual_min_label.setEnabled(False)
            self.parent().ui.Segmentation_Methode_1_Label.setEnabled(True)
            self.parent().ui.Segmentation_Methode_2_Label.setEnabled(True)
            self.parent().ui.Segmentation_Methode_1_ComboBox.setEnabled(True)
            self.parent().ui.Segmentation_Methode_2_ComboBox.setEnabled(True)
            if self.parent().ObjectList.Object_List_Create_Counter_i == 1 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 2 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 3 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_label2.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 4 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_label2.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_label3.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 5 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_label2.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_label3.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_label4.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 6 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_label2.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_label3.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_label4.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_label5.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 7 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_6.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_label2.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_label3.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_label4.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_label5.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_label6.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(False)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 8 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_6.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_7.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_label2.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_label3.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_label4.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_label5.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_label6.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(False)
                self.parent().ui.Manual_obj_label7.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(False)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 9 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_6.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_7.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_8.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_label2.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_label3.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_label4.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_label5.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_label6.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(False)
                self.parent().ui.Manual_obj_label7.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(False)
                self.parent().ui.Manual_obj_label8.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_8.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_8.setEnabled(False)
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 10 :
                self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_6.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_7.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_8.setEnabled(True)
                self.parent().ui.Object_List_SpinBox_9.setEnabled(True)
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_label2.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_label3.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_label4.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_label5.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_label6.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(False)
                self.parent().ui.Manual_obj_label7.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(False)
                self.parent().ui.Manual_obj_label8.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_8.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_8.setEnabled(False)
                self.parent().ui.Manual_obj_label9.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_9.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_9.setEnabled(False)

    def object_name(self):
        if self.parent().ObjectList.Object_List_Create_Counter_i == 1 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 2 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
            Input_text1 = self.parent().ui.Object_List_LineEdited_1.text()
            self.parent().ui.Manual_obj_label1.setText(Input_text1)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 3 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
            Input_text1 = self.parent().ui.Object_List_LineEdited_1.text()
            self.parent().ui.Manual_obj_label1.setText(Input_text1)
            Input_text2 = self.parent().ui.Object_List_LineEdited_2.text()
            self.parent().ui.Manual_obj_label2.setText(Input_text2)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 4 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
            Input_text1 = self.parent().ui.Object_List_LineEdited_1.text()
            self.parent().ui.Manual_obj_label1.setText(Input_text1)
            Input_text2 = self.parent().ui.Object_List_LineEdited_2.text()
            self.parent().ui.Manual_obj_label2.setText(Input_text2)
            Input_text3 = self.parent().ui.Object_List_LineEdited_3.text()
            self.parent().ui.Manual_obj_label3.setText(Input_text3)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 5 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
            Input_text1 = self.parent().ui.Object_List_LineEdited_1.text()
            self.parent().ui.Manual_obj_label1.setText(Input_text1)
            Input_text2 = self.parent().ui.Object_List_LineEdited_2.text()
            self.parent().ui.Manual_obj_label2.setText(Input_text2)
            Input_text3 = self.parent().ui.Object_List_LineEdited_3.text()
            self.parent().ui.Manual_obj_label3.setText(Input_text3)
            Input_text4 = self.parent().ui.Object_List_LineEdited_4.text()
            self.parent().ui.Manual_obj_label4.setText(Input_text4)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 6 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
            Input_text1 = self.parent().ui.Object_List_LineEdited_1.text()
            self.parent().ui.Manual_obj_label1.setText(Input_text1)
            Input_text2 = self.parent().ui.Object_List_LineEdited_2.text()
            self.parent().ui.Manual_obj_label2.setText(Input_text2)
            Input_text3 = self.parent().ui.Object_List_LineEdited_3.text()
            self.parent().ui.Manual_obj_label3.setText(Input_text3)
            Input_text4 = self.parent().ui.Object_List_LineEdited_4.text()
            self.parent().ui.Manual_obj_label4.setText(Input_text4)
            Input_text5 = self.parent().ui.Object_List_LineEdited_5.text()
            self.parent().ui.Manual_obj_label5.setText(Input_text5)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 7 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
            Input_text1 = self.parent().ui.Object_List_LineEdited_1.text()
            self.parent().ui.Manual_obj_label1.setText(Input_text1)
            Input_text2 = self.parent().ui.Object_List_LineEdited_2.text()
            self.parent().ui.Manual_obj_label2.setText(Input_text2)
            Input_text3 = self.parent().ui.Object_List_LineEdited_3.text()
            self.parent().ui.Manual_obj_label3.setText(Input_text3)
            Input_text4 = self.parent().ui.Object_List_LineEdited_4.text()
            self.parent().ui.Manual_obj_label4.setText(Input_text4)
            Input_text5 = self.parent().ui.Object_List_LineEdited_5.text()
            self.parent().ui.Manual_obj_label5.setText(Input_text5)
            Input_text6 = self.parent().ui.Object_List_LineEdited_6.text()
            self.parent().ui.Manual_obj_label6.setText(Input_text6)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 8 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
            Input_text1 = self.parent().ui.Object_List_LineEdited_1.text()
            self.parent().ui.Manual_obj_label1.setText(Input_text1)
            Input_text2 = self.parent().ui.Object_List_LineEdited_2.text()
            self.parent().ui.Manual_obj_label2.setText(Input_text2)
            Input_text3 = self.parent().ui.Object_List_LineEdited_3.text()
            self.parent().ui.Manual_obj_label3.setText(Input_text3)
            Input_text4 = self.parent().ui.Object_List_LineEdited_4.text()
            self.parent().ui.Manual_obj_label4.setText(Input_text4)
            Input_text5 = self.parent().ui.Object_List_LineEdited_5.text()
            self.parent().ui.Manual_obj_label5.setText(Input_text5)
            Input_text6 = self.parent().ui.Object_List_LineEdited_6.text()
            self.parent().ui.Manual_obj_label6.setText(Input_text6)
            Input_text7 = self.parent().ui.Object_List_LineEdited_7.text()
            self.parent().ui.Manual_obj_label7.setText(Input_text7)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 9 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
            Input_text1 = self.parent().ui.Object_List_LineEdited_1.text()
            self.parent().ui.Manual_obj_label1.setText(Input_text1)
            Input_text2 = self.parent().ui.Object_List_LineEdited_2.text()
            self.parent().ui.Manual_obj_label2.setText(Input_text2)
            Input_text3 = self.parent().ui.Object_List_LineEdited_3.text()
            self.parent().ui.Manual_obj_label3.setText(Input_text3)
            Input_text4 = self.parent().ui.Object_List_LineEdited_4.text()
            self.parent().ui.Manual_obj_label4.setText(Input_text4)
            Input_text5 = self.parent().ui.Object_List_LineEdited_5.text()
            self.parent().ui.Manual_obj_label5.setText(Input_text5)
            Input_text6 = self.parent().ui.Object_List_LineEdited_6.text()
            self.parent().ui.Manual_obj_label6.setText(Input_text6)
            Input_text7 = self.parent().ui.Object_List_LineEdited_7.text()
            self.parent().ui.Manual_obj_label7.setText(Input_text7)
            Input_text8 = self.parent().ui.Object_List_LineEdited_8.text()
            self.parent().ui.Manual_obj_label8.setText(Input_text8)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 10 :
            Input_text0 = self.parent().ui.Object_List_LineEdited_0.text()
            self.parent().ui.Manual_obj_label0.setText(Input_text0)
            Input_text1 = self.parent().ui.Object_List_LineEdited_1.text()
            self.parent().ui.Manual_obj_label1.setText(Input_text1)
            Input_text2 = self.parent().ui.Object_List_LineEdited_2.text()
            self.parent().ui.Manual_obj_label2.setText(Input_text2)
            Input_text3 = self.parent().ui.Object_List_LineEdited_3.text()
            self.parent().ui.Manual_obj_label3.setText(Input_text3)
            Input_text4 = self.parent().ui.Object_List_LineEdited_4.text()
            self.parent().ui.Manual_obj_label4.setText(Input_text4)
            Input_text5 = self.parent().ui.Object_List_LineEdited_5.text()
            self.parent().ui.Manual_obj_label5.setText(Input_text5)
            Input_text6 = self.parent().ui.Object_List_LineEdited_6.text()
            self.parent().ui.Manual_obj_label6.setText(Input_text6)
            Input_text7 = self.parent().ui.Object_List_LineEdited_7.text()
            self.parent().ui.Manual_obj_label7.setText(Input_text7)
            Input_text8 = self.parent().ui.Object_List_LineEdited_8.text()
            self.parent().ui.Manual_obj_label8.setText(Input_text8)
            Input_text9 = self.parent().ui.Object_List_LineEdited_9.text()
            self.parent().ui.Manual_obj_label9.setText(Input_text9)

    def Mask_value(self):
        if self.parent().ObjectList.Object_List_Create_Counter_i == 1 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
        if self.parent().ObjectList.Object_List_Create_Counter_i == 2 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
            if self.parent().ui.Manual_obj_min_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText(self.parent().ui.Manual_obj_max_LineEdited_1.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText(self.parent().ui.Manual_obj_min_LineEdited_1.text())
        if self.parent().ObjectList.Object_List_Create_Counter_i == 3 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
            if self.parent().ui.Manual_obj_min_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText(self.parent().ui.Manual_obj_max_LineEdited_1.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText(self.parent().ui.Manual_obj_min_LineEdited_1.text())
            if self.parent().ui.Manual_obj_min_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText(self.parent().ui.Manual_obj_max_LineEdited_2.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText(self.parent().ui.Manual_obj_min_LineEdited_2.text())
        if self.parent().ObjectList.Object_List_Create_Counter_i == 4 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
            if self.parent().ui.Manual_obj_min_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText(self.parent().ui.Manual_obj_max_LineEdited_1.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText(self.parent().ui.Manual_obj_min_LineEdited_1.text())
            if self.parent().ui.Manual_obj_min_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText(self.parent().ui.Manual_obj_max_LineEdited_2.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText(self.parent().ui.Manual_obj_min_LineEdited_2.text())
            if self.parent().ui.Manual_obj_min_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText(self.parent().ui.Manual_obj_max_LineEdited_3.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText(self.parent().ui.Manual_obj_min_LineEdited_3.text())
        if self.parent().ObjectList.Object_List_Create_Counter_i == 5 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
            if self.parent().ui.Manual_obj_min_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText(self.parent().ui.Manual_obj_max_LineEdited_1.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText(self.parent().ui.Manual_obj_min_LineEdited_1.text())
            if self.parent().ui.Manual_obj_min_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText(self.parent().ui.Manual_obj_max_LineEdited_2.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText(self.parent().ui.Manual_obj_min_LineEdited_2.text())
            if self.parent().ui.Manual_obj_min_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText(self.parent().ui.Manual_obj_max_LineEdited_3.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText(self.parent().ui.Manual_obj_min_LineEdited_3.text())
            if self.parent().ui.Manual_obj_min_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText(self.parent().ui.Manual_obj_max_LineEdited_4.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText(self.parent().ui.Manual_obj_min_LineEdited_4.text())
        if self.parent().ObjectList.Object_List_Create_Counter_i == 6 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
            if self.parent().ui.Manual_obj_min_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText(self.parent().ui.Manual_obj_max_LineEdited_1.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText(self.parent().ui.Manual_obj_min_LineEdited_1.text())
            if self.parent().ui.Manual_obj_min_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText(self.parent().ui.Manual_obj_max_LineEdited_2.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText(self.parent().ui.Manual_obj_min_LineEdited_2.text())
            if self.parent().ui.Manual_obj_min_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText(self.parent().ui.Manual_obj_max_LineEdited_3.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText(self.parent().ui.Manual_obj_min_LineEdited_3.text())
            if self.parent().ui.Manual_obj_min_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText(self.parent().ui.Manual_obj_max_LineEdited_4.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText(self.parent().ui.Manual_obj_min_LineEdited_4.text())
            if self.parent().ui.Manual_obj_min_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText(self.parent().ui.Manual_obj_max_LineEdited_5.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText(self.parent().ui.Manual_obj_min_LineEdited_5.text())
        if self.parent().ObjectList.Object_List_Create_Counter_i == 7 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
            if self.parent().ui.Manual_obj_min_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText(self.parent().ui.Manual_obj_max_LineEdited_1.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText(self.parent().ui.Manual_obj_min_LineEdited_1.text())
            if self.parent().ui.Manual_obj_min_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText(self.parent().ui.Manual_obj_max_LineEdited_2.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText(self.parent().ui.Manual_obj_min_LineEdited_2.text())
            if self.parent().ui.Manual_obj_min_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText(self.parent().ui.Manual_obj_max_LineEdited_3.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText(self.parent().ui.Manual_obj_min_LineEdited_3.text())
            if self.parent().ui.Manual_obj_min_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText(self.parent().ui.Manual_obj_max_LineEdited_4.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText(self.parent().ui.Manual_obj_min_LineEdited_4.text())
            if self.parent().ui.Manual_obj_min_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText(self.parent().ui.Manual_obj_max_LineEdited_5.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText(self.parent().ui.Manual_obj_min_LineEdited_5.text())
            if self.parent().ui.Manual_obj_min_LineEdited_6.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_6.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText(self.parent().ui.Manual_obj_max_LineEdited_6.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText(self.parent().ui.Manual_obj_min_LineEdited_6.text())
        if self.parent().ObjectList.Object_List_Create_Counter_i == 8 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
            if self.parent().ui.Manual_obj_min_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText(self.parent().ui.Manual_obj_max_LineEdited_1.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText(self.parent().ui.Manual_obj_min_LineEdited_1.text())
            if self.parent().ui.Manual_obj_min_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText(self.parent().ui.Manual_obj_max_LineEdited_2.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText(self.parent().ui.Manual_obj_min_LineEdited_2.text())
            if self.parent().ui.Manual_obj_min_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText(self.parent().ui.Manual_obj_max_LineEdited_3.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText(self.parent().ui.Manual_obj_min_LineEdited_3.text())
            if self.parent().ui.Manual_obj_min_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText(self.parent().ui.Manual_obj_max_LineEdited_4.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText(self.parent().ui.Manual_obj_min_LineEdited_4.text())
            if self.parent().ui.Manual_obj_min_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText(self.parent().ui.Manual_obj_max_LineEdited_5.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText(self.parent().ui.Manual_obj_min_LineEdited_5.text())
            if self.parent().ui.Manual_obj_min_LineEdited_6.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_6.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText(self.parent().ui.Manual_obj_max_LineEdited_6.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText(self.parent().ui.Manual_obj_min_LineEdited_6.text())
            if self.parent().ui.Manual_obj_min_LineEdited_7.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_7.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_7.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_7.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_7.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_7.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_7.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_7.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_7.setText(self.parent().ui.Manual_obj_max_LineEdited_7.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_7.setText(self.parent().ui.Manual_obj_min_LineEdited_7.text())
        if self.parent().ObjectList.Object_List_Create_Counter_i == 9 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
            if self.parent().ui.Manual_obj_min_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText(self.parent().ui.Manual_obj_max_LineEdited_1.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText(self.parent().ui.Manual_obj_min_LineEdited_1.text())
            if self.parent().ui.Manual_obj_min_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText(self.parent().ui.Manual_obj_max_LineEdited_2.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText(self.parent().ui.Manual_obj_min_LineEdited_2.text())
            if self.parent().ui.Manual_obj_min_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText(self.parent().ui.Manual_obj_max_LineEdited_3.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText(self.parent().ui.Manual_obj_min_LineEdited_3.text())
            if self.parent().ui.Manual_obj_min_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText(self.parent().ui.Manual_obj_max_LineEdited_4.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText(self.parent().ui.Manual_obj_min_LineEdited_4.text())
            if self.parent().ui.Manual_obj_min_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText(self.parent().ui.Manual_obj_max_LineEdited_5.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText(self.parent().ui.Manual_obj_min_LineEdited_5.text())
            if self.parent().ui.Manual_obj_min_LineEdited_6.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_6.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText(self.parent().ui.Manual_obj_max_LineEdited_6.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText(self.parent().ui.Manual_obj_min_LineEdited_6.text())
            if self.parent().ui.Manual_obj_min_LineEdited_7.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_7.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_7.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_7.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_7.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_7.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_7.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_7.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_7.setText(self.parent().ui.Manual_obj_max_LineEdited_7.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_7.setText(self.parent().ui.Manual_obj_min_LineEdited_7.text())
            if self.parent().ui.Manual_obj_min_LineEdited_8.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_8.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_8.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_8.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_8.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_8.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_8.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_8.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_8.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_8.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_8.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_8.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_8.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_8.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_8.setText(self.parent().ui.Manual_obj_max_LineEdited_8.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_8.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_8.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_8.setText(self.parent().ui.Manual_obj_min_LineEdited_8.text())
        if self.parent().ObjectList.Object_List_Create_Counter_i == 10 :
            if self.parent().ui.Manual_obj_min_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_0.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_0.setText(self.parent().ui.Manual_obj_max_LineEdited_0.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_0.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_0.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_0.setText(self.parent().ui.Manual_obj_min_LineEdited_0.text())
            if self.parent().ui.Manual_obj_min_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_1.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_1.setText(self.parent().ui.Manual_obj_max_LineEdited_1.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_1.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_1.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_1.setText(self.parent().ui.Manual_obj_min_LineEdited_1.text())
            if self.parent().ui.Manual_obj_min_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_2.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_2.setText(self.parent().ui.Manual_obj_max_LineEdited_2.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_2.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_2.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_2.setText(self.parent().ui.Manual_obj_min_LineEdited_2.text())
            if self.parent().ui.Manual_obj_min_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_3.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_3.setText(self.parent().ui.Manual_obj_max_LineEdited_3.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_3.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_3.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_3.setText(self.parent().ui.Manual_obj_min_LineEdited_3.text())
            if self.parent().ui.Manual_obj_min_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_4.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_4.setText(self.parent().ui.Manual_obj_max_LineEdited_4.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_4.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_4.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_4.setText(self.parent().ui.Manual_obj_min_LineEdited_4.text())
            if self.parent().ui.Manual_obj_min_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_5.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_5.setText(self.parent().ui.Manual_obj_max_LineEdited_5.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_5.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_5.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_5.setText(self.parent().ui.Manual_obj_min_LineEdited_5.text())
            if self.parent().ui.Manual_obj_min_LineEdited_6.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_6.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_6.setText(self.parent().ui.Manual_obj_max_LineEdited_6.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_6.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_6.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_6.setText(self.parent().ui.Manual_obj_min_LineEdited_6.text())
            if self.parent().ui.Manual_obj_min_LineEdited_7.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_7.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_7.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_7.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_7.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_7.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_7.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_7.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_7.setText(self.parent().ui.Manual_obj_max_LineEdited_7.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_7.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_7.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_7.setText(self.parent().ui.Manual_obj_min_LineEdited_7.text())
            if self.parent().ui.Manual_obj_min_LineEdited_8.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_8.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_8.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_8.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_8.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_8.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_8.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_8.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_8.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_8.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_8.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_8.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_8.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_8.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_8.setText(self.parent().ui.Manual_obj_max_LineEdited_8.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_8.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_8.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_8.setText(self.parent().ui.Manual_obj_min_LineEdited_8.text())
            if self.parent().ui.Manual_obj_min_LineEdited_9.text() == "":
                self.parent().ui.Manual_obj_min_LineEdited_9.setText("0")
            if self.parent().ui.Manual_obj_max_LineEdited_9.text() == "":
                self.parent().ui.Manual_obj_max_LineEdited_9.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_9.text()) < 0 :
                self.parent().ui.Manual_obj_min_LineEdited_9.setText("0")
            if int(self.parent().ui.Manual_obj_min_LineEdited_9.text()) > 65535 :
                self.parent().ui.Manual_obj_min_LineEdited_9.setText("65535")
            if int(self.parent().ui.Manual_obj_max_LineEdited_9.text()) < 0 :
                self.parent().ui.Manual_obj_max_LineEdited_9.setText("0")
            if int(self.parent().ui.Manual_obj_max_LineEdited_9.text()) > 65535 :
                self.parent().ui.Manual_obj_max_LineEdited_9.setText("65535")
            if int(self.parent().ui.Manual_obj_min_LineEdited_9.text()) > int(self.parent().ui.Manual_obj_max_LineEdited_9.text()) :
                self.parent().ui.Manual_obj_min_LineEdited_9.setText(self.parent().ui.Manual_obj_max_LineEdited_9.text())
            if int(self.parent().ui.Manual_obj_max_LineEdited_9.text()) < int(self.parent().ui.Manual_obj_min_LineEdited_9.text()) :
                self.parent().ui.Manual_obj_max_LineEdited_9.setText(self.parent().ui.Manual_obj_min_LineEdited_9.text())

class ObjectList(QtCore.QObject):
    def __init__(self, parent):
        super(ObjectList, self).__init__(parent)
        self.init_Parameters()
        self.connections()

    def init_Parameters(self):
        self.Object_List_Create_Counter_i = 0
        self.parent().ui.widgetObject_List_Add_Button_0 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_0.setGeometry(QtCore.QRect(280, 20, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_0.setObjectName("widgetObject_List_Add_Button_0")
        self.parent().ui.widgetObject_List_Add_Button_1 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_1.setGeometry(QtCore.QRect(280, 50, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_1.setObjectName("widgetObject_List_Add_Button_1")
        self.parent().ui.widgetObject_List_Add_Button_2 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_2.setGeometry(QtCore.QRect(280, 80, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_2.setObjectName("widgetObject_List_Add_Button_2")
        self.parent().ui.widgetObject_List_Add_Button_3 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_3.setGeometry(QtCore.QRect(280, 110, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_3.setObjectName("widgetObject_List_Add_Button_3")
        self.parent().ui.widgetObject_List_Add_Button_4 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_4.setGeometry(QtCore.QRect(280, 140, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_4.setObjectName("widgetObject_List_Add_Button_4")
        self.parent().ui.widgetObject_List_Add_Button_5 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_5.setGeometry(QtCore.QRect(280, 170, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_5.setObjectName("widgetObject_List_Add_Button_5")
        self.parent().ui.widgetObject_List_Add_Button_6 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_6.setGeometry(QtCore.QRect(280, 200, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_6.setObjectName("widgetObject_List_Add_Button_6")
        self.parent().ui.widgetObject_List_Add_Button_7 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_7.setGeometry(QtCore.QRect(280, 230, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_7.setObjectName("widgetObject_List_Add_Button_7")
        self.parent().ui.widgetObject_List_Add_Button_8 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_8.setGeometry(QtCore.QRect(280, 260, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_8.setObjectName("widgetObject_List_Add_Button_8")
        self.parent().ui.widgetObject_List_Add_Button_9 = QtWidgets.QWidget(self.parent().ui.Object_List_GroupBox)
        self.parent().ui.widgetObject_List_Add_Button_9.setGeometry(QtCore.QRect(280, 290, 130, 22))
        self.parent().ui.widgetObject_List_Add_Button_9.setObjectName("widgetObject_List_Add_Button_9")


    def Object_List_Input (self):
        if self.Object_List_Create_Counter_i == 10:
            self.parent().ui.Object_List_Add_Button.setEnabled(False)

        elif self.parent().ui.Object_List_LineEdit.text() != "":
            self.parent().ui.Object_List_Add_Button.setEnabled(True)

        else:
            self.parent().ui.Object_List_Add_Button.setEnabled(False)

    def Object_List_Create (self):
        if self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i == 0 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_0 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_0)
            self.parent().ui.horizontalObject_List_CreateLayout_0.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_0.setObjectName("horizontalObject_List_CreateLayout_0")
            self.parent().ui.Object_List_LineEdited_0 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_0)
            self.parent().ui.Object_List_LineEdited_0.setObjectName("Object_List_LineEdited_0")
            self.parent().ui.horizontalObject_List_CreateLayout_0.addWidget(self.parent().ui.Object_List_LineEdited_0)
            self.parent().ui.Object_List_SpinBox_0 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_0)
            self.parent().ui.Object_List_SpinBox_0.setObjectName("Object_List_SpinBox_0")
            self.parent().ui.Object_List_SpinBox_0.setValue(0)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(0)
            self.parent().ui.horizontalObject_List_CreateLayout_0.addWidget(self.parent().ui.Object_List_SpinBox_0)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_0)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_0.addWidget(self.parent().ui.Delete_Object_List_Button)

            self.parent().ui.Manual_obj_label0 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label0.setObjectName("Manual_obj_label0")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label0, 1, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_0 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_0.setObjectName("Manual_obj_min_LineEdited_0")
            self.parent().ui.Manual_obj_min_LineEdited_0.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_0.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_0, 1, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_0 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_0.setObjectName("Manual_obj_max_LineEdited_0")
            self.parent().ui.Manual_obj_max_LineEdited_0.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_0.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_0, 1, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label0.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_0.setText(Input_text)
            self.parent().ui.Manual_obj_label0.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_0.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_0.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_0.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_0.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_0.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_0.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_0.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_0.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_0.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)


        elif self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i==1 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_0.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_1 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_1)
            self.parent().ui.horizontalObject_List_CreateLayout_1.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_1.setObjectName("horizontalObject_List_CreateLayout_1")
            self.parent().ui.Object_List_LineEdited_1 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_1)
            self.parent().ui.Object_List_LineEdited_1.setObjectName("Object_List_LineEdited_1")
            self.parent().ui.horizontalObject_List_CreateLayout_1.addWidget(self.parent().ui.Object_List_LineEdited_1)
            self.parent().ui.Object_List_SpinBox_1 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_1)
            self.parent().ui.Object_List_SpinBox_1.setObjectName("Object_List_SpinBox_1")
            self.parent().ui.Object_List_SpinBox_1.setValue(1)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(1)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(1)
            self.parent().ui.horizontalObject_List_CreateLayout_1.addWidget(self.parent().ui.Object_List_SpinBox_1)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_1)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_1.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Manual_obj_label1 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label1.setObjectName("Manual_obj_label1")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label1, 2, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_1 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_1.setObjectName("Manual_obj_min_LineEdited_1")
            self.parent().ui.Manual_obj_min_LineEdited_1.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_1.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_1, 2, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_1 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_1.setObjectName("Manual_obj_max_LineEdited_1")
            self.parent().ui.Manual_obj_max_LineEdited_1.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_1.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_1, 2, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label1.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_1.setText(Input_text)
            self.parent().ui.Manual_obj_label1.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_1.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_1.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_1.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_1.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_1.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_1.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_1.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_1.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_1.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)


        elif self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i==2 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_1.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_2 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_2)
            self.parent().ui.horizontalObject_List_CreateLayout_2.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_2.setObjectName("horizontalObject_List_CreateLayout_2")
            self.parent().ui.Object_List_LineEdited_2 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_2)
            self.parent().ui.Object_List_LineEdited_2.setObjectName("Object_List_LineEdited_2")
            self.parent().ui.horizontalObject_List_CreateLayout_2.addWidget(self.parent().ui.Object_List_LineEdited_2)
            self.parent().ui.Object_List_SpinBox_2 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_2)
            self.parent().ui.Object_List_SpinBox_2.setObjectName("Object_List_SpinBox_2")
            self.parent().ui.Object_List_SpinBox_2.setValue(2)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(2)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(2)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(2)
            self.parent().ui.horizontalObject_List_CreateLayout_2.addWidget(self.parent().ui.Object_List_SpinBox_2)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_2)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_2.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Manual_obj_label2 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label2.setObjectName("Manual_obj_label2")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label2, 3, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_2 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_2.setObjectName("Manual_obj_min_LineEdited_2")
            self.parent().ui.Manual_obj_min_LineEdited_2.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_2.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_2, 3, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_2 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_2.setObjectName("Manual_obj_max_LineEdited_2")
            self.parent().ui.Manual_obj_max_LineEdited_2.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_2.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_2, 3, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label2.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_2.setText(Input_text)
            self.parent().ui.Manual_obj_label2.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_2.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_2.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_2.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_2.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_2.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_2.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_2.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_2.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_2.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)


        elif self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i==3 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_2.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_3 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_3)
            self.parent().ui.horizontalObject_List_CreateLayout_3.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_3.setObjectName("horizontalObject_List_CreateLayout_3")
            self.parent().ui.Object_List_LineEdited_3 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_3)
            self.parent().ui.Object_List_LineEdited_3.setObjectName("Object_List_LineEdited_3")
            self.parent().ui.horizontalObject_List_CreateLayout_3.addWidget(self.parent().ui.Object_List_LineEdited_3)
            self.parent().ui.Object_List_SpinBox_3 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_3)
            self.parent().ui.Object_List_SpinBox_3.setObjectName("Object_List_SpinBox_3")
            self.parent().ui.Object_List_SpinBox_3.setValue(3)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(3)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(3)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(3)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(3)
            self.parent().ui.horizontalObject_List_CreateLayout_3.addWidget(self.parent().ui.Object_List_SpinBox_3)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_3)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_3.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Manual_obj_label3 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label3.setObjectName("Manual_obj_label3")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label3, 4, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_3 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_3.setObjectName("Manual_obj_min_LineEdited_3")
            self.parent().ui.Manual_obj_min_LineEdited_3.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_3.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_3, 4, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_3 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_3.setObjectName("Manual_obj_max_LineEdited_3")
            self.parent().ui.Manual_obj_max_LineEdited_3.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_3.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_3, 4, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label3.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_3.setText(Input_text)
            self.parent().ui.Manual_obj_label3.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_3.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_3.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_3.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_3.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_3.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_3.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_3.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_3.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_3.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)


        elif self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i==4 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_3.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_4 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_4)
            self.parent().ui.horizontalObject_List_CreateLayout_4.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_4.setObjectName("horizontalObject_List_CreateLayout_4")
            self.parent().ui.Object_List_LineEdited_4 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_4)
            self.parent().ui.Object_List_LineEdited_4.setObjectName("Object_List_LineEdited_4")
            self.parent().ui.horizontalObject_List_CreateLayout_4.addWidget(self.parent().ui.Object_List_LineEdited_4)
            self.parent().ui.Object_List_SpinBox_4 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_4)
            self.parent().ui.Object_List_SpinBox_4.setObjectName("Object_List_SpinBox_4")
            self.parent().ui.Object_List_SpinBox_4.setValue(4)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(4)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(4)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(4)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(4)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(4)
            self.parent().ui.horizontalObject_List_CreateLayout_4.addWidget(self.parent().ui.Object_List_SpinBox_4)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_4)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_4.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Manual_obj_label4 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label4.setObjectName("Manual_obj_label4")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label4, 5, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_4 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_4.setObjectName("Manual_obj_min_LineEdited_4")
            self.parent().ui.Manual_obj_min_LineEdited_4.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_4.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_4, 5, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_4 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_4.setObjectName("Manual_obj_max_LineEdited_4")
            self.parent().ui.Manual_obj_max_LineEdited_4.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_4.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_4, 5, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label4.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_4.setText(Input_text)
            self.parent().ui.Manual_obj_label4.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_4.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_4.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_4.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_4.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_4.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_4.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_4.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_4.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_4.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)



        elif self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i==5 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_4.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_5 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_5)
            self.parent().ui.horizontalObject_List_CreateLayout_5.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_5.setObjectName("horizontalObject_List_CreateLayout_5")
            self.parent().ui.Object_List_LineEdited_5 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_5)
            self.parent().ui.Object_List_LineEdited_5.setObjectName("Object_List_LineEdited_5")
            self.parent().ui.horizontalObject_List_CreateLayout_5.addWidget(self.parent().ui.Object_List_LineEdited_5)
            self.parent().ui.Object_List_SpinBox_5 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_5)
            self.parent().ui.Object_List_SpinBox_5.setObjectName("Object_List_SpinBox_5")
            self.parent().ui.Object_List_SpinBox_5.setValue(5)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_5.setMaximum(5)
            self.parent().ui.horizontalObject_List_CreateLayout_5.addWidget(self.parent().ui.Object_List_SpinBox_5)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_5)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_5.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Manual_obj_label5 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label5.setObjectName("Manual_obj_label5")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label5, 6, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_5 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_5.setObjectName("Manual_obj_min_LineEdited_5")
            self.parent().ui.Manual_obj_min_LineEdited_5.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_5.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_5, 6, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_5 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_5.setObjectName("Manual_obj_max_LineEdited_5")
            self.parent().ui.Manual_obj_max_LineEdited_5.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_5.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_5, 6, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label5.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_5.setText(Input_text)
            self.parent().ui.Manual_obj_label5.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_5.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_5.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_5.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_5.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_5.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_5.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_5.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_5.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_5.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)


        elif self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i==6 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_5.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_6 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_6)
            self.parent().ui.horizontalObject_List_CreateLayout_6.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_6.setObjectName("horizontalObject_List_CreateLayout_6")
            self.parent().ui.Object_List_LineEdited_6 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_6)
            self.parent().ui.Object_List_LineEdited_6.setObjectName("Object_List_LineEdited_6")
            self.parent().ui.horizontalObject_List_CreateLayout_6.addWidget(self.parent().ui.Object_List_LineEdited_6)
            self.parent().ui.Object_List_SpinBox_6 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_6)
            self.parent().ui.Object_List_SpinBox_6.setObjectName("Object_List_SpinBox_6")
            self.parent().ui.horizontalObject_List_CreateLayout_6.addWidget(self.parent().ui.Object_List_SpinBox_6)
            self.parent().ui.Object_List_SpinBox_6.setValue(6)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_5.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_6.setMaximum(6)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_6)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_6.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Manual_obj_label6 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label6.setObjectName("Manual_obj_label6")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label6, 7, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_6 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_6.setObjectName("Manual_obj_min_LineEdited_6")
            self.parent().ui.Manual_obj_min_LineEdited_6.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_6.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_6, 7, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_6 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_6.setObjectName("Manual_obj_max_LineEdited_6")
            self.parent().ui.Manual_obj_max_LineEdited_6.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_6.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_6, 7, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label6.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_6.setText(Input_text)
            self.parent().ui.Manual_obj_label6.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_6.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_6.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_6.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_6.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_6.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_6.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_6.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_6.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_6.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)

        elif self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i==7 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_6.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_7 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_7)
            self.parent().ui.horizontalObject_List_CreateLayout_7.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_7.setObjectName("horizontalObject_List_CreateLayout_7")
            self.parent().ui.Object_List_LineEdited_7 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_7)
            self.parent().ui.Object_List_LineEdited_7.setObjectName("Object_List_LineEdited_7")
            self.parent().ui.horizontalObject_List_CreateLayout_7.addWidget(self.parent().ui.Object_List_LineEdited_7)
            self.parent().ui.Object_List_SpinBox_7 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_7)
            self.parent().ui.Object_List_SpinBox_7.setObjectName("Object_List_SpinBox_7")
            self.parent().ui.horizontalObject_List_CreateLayout_7.addWidget(self.parent().ui.Object_List_SpinBox_7)
            self.parent().ui.Object_List_SpinBox_7.setValue(7)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_5.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_6.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_7.setMaximum(7)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_7)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_7.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Manual_obj_label7 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label7.setObjectName("Manual_obj_label7")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label7, 8, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_7 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_7.setObjectName("Manual_obj_min_LineEdited_7")
            self.parent().ui.Manual_obj_min_LineEdited_7.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_7.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_7, 8, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_7 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_7.setObjectName("Manual_obj_max_LineEdited_7")
            self.parent().ui.Manual_obj_max_LineEdited_7.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_7.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_7, 8, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label7.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_7.setText(Input_text)
            self.parent().ui.Manual_obj_label7.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_7.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_7.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_7.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_7.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_7.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_7.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_7.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_7.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_7.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)

        elif self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i==8 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_7.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_8 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_8)
            self.parent().ui.horizontalObject_List_CreateLayout_8.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_8.setObjectName("horizontalObject_List_CreateLayout_8")
            self.parent().ui.Object_List_LineEdited_8 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_8)
            self.parent().ui.Object_List_LineEdited_8.setObjectName("Object_List_LineEdited_8")
            self.parent().ui.horizontalObject_List_CreateLayout_8.addWidget(self.parent().ui.Object_List_LineEdited_8)
            self.parent().ui.Object_List_SpinBox_8 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_8)
            self.parent().ui.Object_List_SpinBox_8.setObjectName("Object_List_SpinBox_8")
            self.parent().ui.horizontalObject_List_CreateLayout_8.addWidget(self.parent().ui.Object_List_SpinBox_8)
            self.parent().ui.Object_List_SpinBox_8.setValue(8)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_5.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_6.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_7.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_8.setMaximum(8)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_8)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_8.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Manual_obj_label8 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label8.setObjectName("Manual_obj_label8")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label8, 9, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_8 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_8.setObjectName("Manual_obj_min_LineEdited_8")
            self.parent().ui.Manual_obj_min_LineEdited_8.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_8.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_8, 9, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_8 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_8.setObjectName("Manual_obj_max_LineEdited_8")
            self.parent().ui.Manual_obj_max_LineEdited_8.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_8.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_8, 9, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label8.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_8.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_8.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_8.setText(Input_text)
            self.parent().ui.Manual_obj_label8.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_8.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_8.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_8.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_8.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_8.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_8.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_8.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_8.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_8.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)

        elif self.parent().ui.Object_List_Add_Button.clicked and self.Object_List_Create_Counter_i==9 :
            self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i+1
            self.parent().ui.horizontalObject_List_CreateLayout_8.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_9 = QtWidgets.QHBoxLayout(self.parent().ui.widgetObject_List_Add_Button_9)
            self.parent().ui.horizontalObject_List_CreateLayout_9.setContentsMargins(0, 0, 0, 0)
            self.parent().ui.horizontalObject_List_CreateLayout_9.setObjectName("horizontalObject_List_CreateLayout_9")
            self.parent().ui.Object_List_LineEdited_9 = QtWidgets.QLineEdit(self.parent().ui.widgetObject_List_Add_Button_9)
            self.parent().ui.Object_List_LineEdited_9.setObjectName("Object_List_LineEdited_9")
            self.parent().ui.horizontalObject_List_CreateLayout_9.addWidget(self.parent().ui.Object_List_LineEdited_9)
            self.parent().ui.Object_List_SpinBox_9 = QtWidgets.QSpinBox(self.parent().ui.widgetObject_List_Add_Button_9)
            self.parent().ui.Object_List_SpinBox_9.setObjectName("Object_List_SpinBox_9")
            self.parent().ui.Object_List_SpinBox_9.setValue(9)
            self.parent().ui.Object_List_SpinBox_0.setMaximum(9)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(9)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(9)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(9)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(9)
            self.parent().ui.Object_List_SpinBox_5.setMaximum(9)
            self.parent().ui.Object_List_SpinBox_6.setMaximum(9)
            self.parent().ui.Object_List_SpinBox_7.setMaximum(9)
            self.parent().ui.Object_List_SpinBox_8.setMaximum(9)
            self.parent().ui.Object_List_SpinBox_9.setMaximum(9)
            self.parent().ui.horizontalObject_List_CreateLayout_9.addWidget(self.parent().ui.Object_List_SpinBox_9)
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_9)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_9.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Manual_obj_label9 = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_label9.setObjectName("Manual_obj_label9")
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_label9, 10, 0, 1, 1)
            self.parent().ui.Manual_obj_min_LineEdited_9 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_min_LineEdited_9.setObjectName("Manual_obj_min_LineEdited_9")
            self.parent().ui.Manual_obj_min_LineEdited_9.setText("0")
            self.parent().ui.Manual_obj_min_LineEdited_9.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_min_LineEdited_9, 10, 2, 1, 1)
            self.parent().ui.Manual_obj_max_LineEdited_9 = QtWidgets.QLineEdit(self.parent().ui.scrollAreaWidgetContents_2)
            self.parent().ui.Manual_obj_max_LineEdited_9.setObjectName("Manual_obj_max_LineEdited_9")
            self.parent().ui.Manual_obj_max_LineEdited_9.setText("0")
            self.parent().ui.Manual_obj_max_LineEdited_9.setValidator(self.parent().Parameters.onlyInt)
            self.parent().ui.gridLayout_6.addWidget(self.parent().ui.Manual_obj_max_LineEdited_9, 10, 3, 1, 1)
            if self.parent().ui.Manual_checkBox.isChecked() is False :
                self.parent().ui.Manual_obj_label9.setEnabled(False)
                self.parent().ui.Manual_obj_min_LineEdited_9.setEnabled(False)
                self.parent().ui.Manual_obj_max_LineEdited_9.setEnabled(False)
            Input_text = self.parent().ui.Object_List_LineEdit.text()
            self.parent().ui.Object_List_LineEdited_9.setText(Input_text)
            self.parent().ui.Manual_obj_label9.setText(Input_text)
            self.parent().ui.Object_List_LineEdit.clear()

            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.Object_List_LineEdited_9.textChanged.connect(self.Object_List_State)
            self.parent().ui.Object_List_SpinBox_9.valueChanged.connect(self.Object_List_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_State)
            self.parent().ui.Object_List_LineEdited_9.textChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_SpinBox_9.valueChanged.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.Object_List_LineEdited_9.textChanged.connect(self.parent().Manual_parameters.object_name)
            self.parent().ui.Manual_obj_min_LineEdited_9.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Manual_obj_max_LineEdited_9.textChanged.connect(self.parent().Manual_parameters.Mask_value)
            self.parent().ui.Object_List_LineEdited_9.textChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Object_List_SpinBox_9.valueChanged.connect(self.parent().Run.Run_State)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().Run.Run_State)

    def Object_List_Delete (self) :
        self.Object_List_Create_Counter_i = self.Object_List_Create_Counter_i-1
        if self.Object_List_Create_Counter_i == 0 :
            self.parent().ui.horizontalObject_List_CreateLayout_0.removeWidget(self.parent().ui.Object_List_SpinBox_0)
            self.parent().ui.Object_List_SpinBox_0.deleteLater()
            self.parent().ui.Object_List_SpinBox_0 = None
            self.parent().ui.horizontalObject_List_CreateLayout_0.removeWidget(self.parent().ui.Object_List_LineEdited_0)
            self.parent().ui.Object_List_LineEdited_0.deleteLater()
            self.parent().ui.Object_List_LineEdited_0 = None
            self.parent().ui.horizontalObject_List_CreateLayout_0.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.horizontalObject_List_CreateLayout_0.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_0 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label0)
            self.parent().ui.Manual_obj_label0.deleteLater()
            self.parent().ui.Manual_obj_label0 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_0)
            self.parent().ui.Manual_obj_min_LineEdited_0.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_0 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_0)
            self.parent().ui.Manual_obj_max_LineEdited_0.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_0 = None

        elif self.Object_List_Create_Counter_i == 1 :
            self.parent().ui.Object_List_SpinBox_0.setMaximum(0)
            self.parent().ui.horizontalObject_List_CreateLayout_1.removeWidget(self.parent().ui.Object_List_SpinBox_1)
            self.parent().ui.Object_List_SpinBox_1.deleteLater()
            self.parent().ui.Object_List_SpinBox_1 = None
            self.parent().ui.horizontalObject_List_CreateLayout_1.removeWidget(self.parent().ui.Object_List_LineEdited_1)
            self.parent().ui.Object_List_LineEdited_1.deleteLater()
            self.parent().ui.Object_List_LineEdited_1 = None
            self.parent().ui.horizontalObject_List_CreateLayout_1.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_0)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_0.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.horizontalObject_List_CreateLayout_1.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_1 = None
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label1)
            self.parent().ui.Manual_obj_label1.deleteLater()
            self.parent().ui.Manual_obj_label1 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_1)
            self.parent().ui.Manual_obj_min_LineEdited_1.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_1 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_1)
            self.parent().ui.Manual_obj_max_LineEdited_1.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_1 = None

        elif self.Object_List_Create_Counter_i == 2 :
            self.parent().ui.Object_List_SpinBox_0.setMaximum(1)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(1)
            self.parent().ui.horizontalObject_List_CreateLayout_2.removeWidget(self.parent().ui.Object_List_SpinBox_2)
            self.parent().ui.Object_List_SpinBox_2.deleteLater()
            self.parent().ui.Object_List_SpinBox_2 = None
            self.parent().ui.horizontalObject_List_CreateLayout_2.removeWidget(self.parent().ui.Object_List_LineEdited_2)
            self.parent().ui.Object_List_LineEdited_2.deleteLater()
            self.parent().ui.Object_List_LineEdited_2 = None
            self.parent().ui.horizontalObject_List_CreateLayout_2.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_1)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_1.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.horizontalObject_List_CreateLayout_2.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_2 = None
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label2)
            self.parent().ui.Manual_obj_label2.deleteLater()
            self.parent().ui.Manual_obj_label2 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_2)
            self.parent().ui.Manual_obj_min_LineEdited_2.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_2 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_2)
            self.parent().ui.Manual_obj_max_LineEdited_2.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_2 = None

        elif self.Object_List_Create_Counter_i == 3 :
            self.parent().ui.Object_List_SpinBox_0.setMaximum(2)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(2)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(2)
            self.parent().ui.horizontalObject_List_CreateLayout_3.removeWidget(self.parent().ui.Object_List_SpinBox_3)
            self.parent().ui.Object_List_SpinBox_3.deleteLater()
            self.parent().ui.Object_List_SpinBox_3 = None
            self.parent().ui.horizontalObject_List_CreateLayout_3.removeWidget(self.parent().ui.Object_List_LineEdited_3)
            self.parent().ui.Object_List_LineEdited_3.deleteLater()
            self.parent().ui.Object_List_LineEdited_3 = None
            self.parent().ui.horizontalObject_List_CreateLayout_3.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_2)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_2.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.horizontalObject_List_CreateLayout_3.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_3 = None
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label3)
            self.parent().ui.Manual_obj_label3.deleteLater()
            self.parent().ui.Manual_obj_label3 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_3)
            self.parent().ui.Manual_obj_min_LineEdited_3.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_3 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_3)
            self.parent().ui.Manual_obj_max_LineEdited_3.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_3 = None

        elif self.Object_List_Create_Counter_i == 4  :
            self.parent().ui.Object_List_SpinBox_0.setMaximum(3)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(3)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(3)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(3)
            self.parent().ui.horizontalObject_List_CreateLayout_4.removeWidget(self.parent().ui.Object_List_SpinBox_4)
            self.parent().ui.Object_List_SpinBox_4.deleteLater()
            self.parent().ui.Object_List_SpinBox_4 = None
            self.parent().ui.horizontalObject_List_CreateLayout_4.removeWidget(self.parent().ui.Object_List_LineEdited_4)
            self.parent().ui.Object_List_LineEdited_4.deleteLater()
            self.parent().ui.Object_List_LineEdited_4 = None
            self.parent().ui.horizontalObject_List_CreateLayout_4.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_3)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_3.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.horizontalObject_List_CreateLayout_4.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_4 = None
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label4)
            self.parent().ui.Manual_obj_label4.deleteLater()
            self.parent().ui.Manual_obj_label4 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_4)
            self.parent().ui.Manual_obj_min_LineEdited_4.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_4 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_4)
            self.parent().ui.Manual_obj_max_LineEdited_4.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_4 = None

        elif self.Object_List_Create_Counter_i == 5  :
            self.parent().ui.Object_List_SpinBox_0.setMaximum(4)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(4)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(4)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(4)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(4)
            self.parent().ui.horizontalObject_List_CreateLayout_5.removeWidget(self.parent().ui.Object_List_SpinBox_5)
            self.parent().ui.Object_List_SpinBox_5.deleteLater()
            self.parent().ui.Object_List_SpinBox_5 = None
            self.parent().ui.horizontalObject_List_CreateLayout_5.removeWidget(self.parent().ui.Object_List_LineEdited_5)
            self.parent().ui.Object_List_LineEdited_5.deleteLater()
            self.parent().ui.Object_List_LineEdited_5 = None
            self.parent().ui.horizontalObject_List_CreateLayout_5.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_4)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_4.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.horizontalObject_List_CreateLayout_5.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_5 = None
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label5)
            self.parent().ui.Manual_obj_label5.deleteLater()
            self.parent().ui.Manual_obj_label5 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_5)
            self.parent().ui.Manual_obj_min_LineEdited_5.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_5 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_5)
            self.parent().ui.Manual_obj_max_LineEdited_5.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_5 = None

        elif self.Object_List_Create_Counter_i == 6 :
            self.parent().ui.Object_List_SpinBox_0.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(5)
            self.parent().ui.Object_List_SpinBox_5.setMaximum(5)
            self.parent().ui.horizontalObject_List_CreateLayout_6.removeWidget(self.parent().ui.Object_List_SpinBox_6)
            self.parent().ui.Object_List_SpinBox_6.deleteLater()
            self.parent().ui.Object_List_SpinBox_6 = None
            self.parent().ui.horizontalObject_List_CreateLayout_6.removeWidget(self.parent().ui.Object_List_LineEdited_6)
            self.parent().ui.Object_List_LineEdited_6.deleteLater()
            self.parent().ui.Object_List_LineEdited_6 = None
            self.parent().ui.horizontalObject_List_CreateLayout_6.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_5)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_5.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.horizontalObject_List_CreateLayout_6.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_6 = None
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label6)
            self.parent().ui.Manual_obj_label6.deleteLater()
            self.parent().ui.Manual_obj_label6 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_6)
            self.parent().ui.Manual_obj_min_LineEdited_6.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_6 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_6)
            self.parent().ui.Manual_obj_max_LineEdited_6.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_6 = None

        elif self.Object_List_Create_Counter_i == 7  :
            self.parent().ui.Object_List_SpinBox_0.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_5.setMaximum(6)
            self.parent().ui.Object_List_SpinBox_6.setMaximum(6)
            self.parent().ui.horizontalObject_List_CreateLayout_7.removeWidget(self.parent().ui.Object_List_SpinBox_7)
            self.parent().ui.Object_List_SpinBox_7.deleteLater()
            self.parent().ui.Object_List_SpinBox_7 = None
            self.parent().ui.horizontalObject_List_CreateLayout_7.removeWidget(self.parent().ui.Object_List_LineEdited_7)
            self.parent().ui.Object_List_LineEdited_7.deleteLater()
            self.parent().ui.Object_List_LineEdited_7 = None
            self.parent().ui.horizontalObject_List_CreateLayout_7.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_6)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_6.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.horizontalObject_List_CreateLayout_7.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_7 = None
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label7)
            self.parent().ui.Manual_obj_label7.deleteLater()
            self.parent().ui.Manual_obj_label7 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_7)
            self.parent().ui.Manual_obj_min_LineEdited_7.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_7 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_7)
            self.parent().ui.Manual_obj_max_LineEdited_7.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_7 = None

        elif self.Object_List_Create_Counter_i == 8  :
            self.parent().ui.Object_List_SpinBox_0.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_5.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_6.setMaximum(7)
            self.parent().ui.Object_List_SpinBox_7.setMaximum(7)
            self.parent().ui.horizontalObject_List_CreateLayout_8.removeWidget(self.parent().ui.Object_List_SpinBox_8)
            self.parent().ui.Object_List_SpinBox_8.deleteLater()
            self.parent().ui.Object_List_SpinBox_8 = None
            self.parent().ui.horizontalObject_List_CreateLayout_8.removeWidget(self.parent().ui.Object_List_LineEdited_8)
            self.parent().ui.Object_List_LineEdited_8.deleteLater()
            self.parent().ui.Object_List_LineEdited_8 = None
            self.parent().ui.horizontalObject_List_CreateLayout_8.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_7)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_7.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.horizontalObject_List_CreateLayout_8.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_8 = None
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label8)
            self.parent().ui.Manual_obj_label8.deleteLater()
            self.parent().ui.Manual_obj_label8 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_8)
            self.parent().ui.Manual_obj_min_LineEdited_8.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_8 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_8)
            self.parent().ui.Manual_obj_max_LineEdited_8.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_8 = None

        elif self.Object_List_Create_Counter_i == 9 :
            self.parent().ui.Object_List_SpinBox_0.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_1.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_2.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_3.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_4.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_5.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_6.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_7.setMaximum(8)
            self.parent().ui.Object_List_SpinBox_8.setMaximum(8)
            self.parent().ui.horizontalObject_List_CreateLayout_9.removeWidget(self.parent().ui.Object_List_SpinBox_9)
            self.parent().ui.Object_List_SpinBox_9.deleteLater()
            self.parent().ui.Object_List_SpinBox_9 = None
            self.parent().ui.horizontalObject_List_CreateLayout_9.removeWidget(self.parent().ui.Object_List_LineEdited_9)
            self.parent().ui.Object_List_LineEdited_9.deleteLater()
            self.parent().ui.Object_List_LineEdited_9 = None
            self.parent().ui.horizontalObject_List_CreateLayout_9.removeWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.deleteLater()
            self.parent().ui.Delete_Object_List_Button = None
            self.parent().ui.Delete_Object_List_Button = QtWidgets.QPushButton(self.parent().ui.widgetObject_List_Add_Button_8)
            self.parent().ui.Delete_Object_List_Button.setGeometry(QtCore.QRect(620, 30, 20, 23))
            self.parent().ui.Delete_Object_List_Button.setAutoFillBackground(False)
            self.parent().ui.Delete_Object_List_Button.setObjectName("Delete_Object_List_Button")
            self.parent().ui.Delete_Object_List_Button.setText("X")
            self.parent().ui.horizontalObject_List_CreateLayout_8.addWidget(self.parent().ui.Delete_Object_List_Button)
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.Object_List_Delete)
            self.parent().ui.horizontalObject_List_CreateLayout_9.deleteLater()
            self.parent().ui.horizontalObject_List_CreateLayout_9 = None
            self.parent().ui.Delete_Object_List_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_label9)
            self.parent().ui.Manual_obj_label9.deleteLater()
            self.parent().ui.Manual_obj_label9 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_min_LineEdited_9)
            self.parent().ui.Manual_obj_min_LineEdited_9.deleteLater()
            self.parent().ui.Manual_obj_min_LineEdited_9 = None
            self.parent().ui.gridLayout_6.removeWidget(self.parent().ui.Manual_obj_max_LineEdited_9)
            self.parent().ui.Manual_obj_max_LineEdited_9.deleteLater()
            self.parent().ui.Manual_obj_max_LineEdited_9 = None

    def Object_List_State (self):

        if self.parent().ui.Object_List_Button_Check.clicked :
            if self.Object_List_Create_Counter_i == 0 :
                self.parent().ui.Text_Object_List_State = (" /!\ Add object(s) /!\ ")
                self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 1 :
                if self.parent().ui.Object_List_LineEdited_0.text() !="" and self.parent().ui.Object_List_SpinBox_0.value() == 0 :
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0

                elif self.parent().ui.Object_List_LineEdited_0.text() =="" and self.parent().ui.Object_List_SpinBox_0.value() != 0 :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif self.parent().ui.Object_List_LineEdited_0.text() ==""  :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif self.parent().ui.Object_List_SpinBox_0.value() != 0 :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 2 :
                index_list = [0,1]

                if (all ([self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_1.text()])
                    and all([self.parent().ui.Object_List_LineEdited_0.text() !="",
                             self.parent().ui.Object_List_LineEdited_1.text() !=""])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value()])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_1.value() in index_list])):
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() ==""])
                      and all ([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value()])
                      and any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_1.value() not in index_list])):
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() ==""])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text()])
                     and any ([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value()])):
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects and spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text():
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value() :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs has the same value /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_1.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 3 :
                index_list = [0,1,2]

                if (all ([self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_1.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_2.text()])
                    and all([self.parent().ui.Object_List_LineEdited_0.text() !="",
                             self.parent().ui.Object_List_LineEdited_1.text() !="",
                             self.parent().ui.Object_List_LineEdited_2.text() !=""])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value()])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_1.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_2.value() in index_list] )) :
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0
                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() ==""])
                      and all ([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value()])
                      and any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_1.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_2.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() ==""])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text()])
                     and any ([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects and spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text()])):
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs has the same value /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_1.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_2.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 4 :
                index_list = [0,1,2,3]

                if (all ([self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_1.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_3.text()])
                    and all([self.parent().ui.Object_List_LineEdited_0.text() !="", self.parent().ui.Object_List_LineEdited_1.text() !="",
                             self.parent().ui.Object_List_LineEdited_2.text() !="", self.parent().ui.Object_List_LineEdited_3.text() !=""])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value()])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_1.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_2.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_3.value() in index_list])) :
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0
                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="", self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="", self.parent().ui.Object_List_LineEdited_3.text() ==""])
                      and all ([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value()])
                      and any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_1.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_2.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_3.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="",
                            self.parent().ui.Object_List_LineEdited_3.text() ==""])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text()])
                     and any ([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects and spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text()])):
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs has the same value /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_1.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_2.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_3.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 5 :
                index_list = [0,1,2,3,4]

                if (all ([self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_1.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_4.text()])
                    and all([self.parent().ui.Object_List_LineEdited_0.text() !="", self.parent().ui.Object_List_LineEdited_1.text() !="",
                             self.parent().ui.Object_List_LineEdited_2.text() !="", self.parent().ui.Object_List_LineEdited_3.text() !="",
                             self.parent().ui.Object_List_LineEdited_4.text() !=""])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value()])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_1.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_2.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_3.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_4.value() in index_list])) :
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="", self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="", self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() ==""])
                      and all ([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value()])
                      and any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_1.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_2.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_4.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="",
                            self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() ==""])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text()])
                     and any ([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects and spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text()])):
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs has the same value /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_1.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_2.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_4.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 6 :
                index_list = [0,1,2,3,4,5]

                if (all ([self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_1.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_5.text()])
                    and all([self.parent().ui.Object_List_LineEdited_0.text() !="", self.parent().ui.Object_List_LineEdited_1.text() !="",
                             self.parent().ui.Object_List_LineEdited_2.text() !="", self.parent().ui.Object_List_LineEdited_3.text() !="",
                             self.parent().ui.Object_List_LineEdited_4.text() !="", self.parent().ui.Object_List_LineEdited_5.text() !=""])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value()])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_1.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_2.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_3.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_4.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_5.value() in index_list])) :
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="", self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="", self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="", self.parent().ui.Object_List_LineEdited_5.text() ==""])
                      and all ([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value()])
                      and any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_1.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_2.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_5.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="",
                            self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="",
                            self.parent().ui.Object_List_LineEdited_5.text() ==""])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text()])
                     and any ([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects and spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1
                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text()])):
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs has the same value /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_1.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_2.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_5.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 7 :
                index_list = [0,1,2,3,4,5,6]

                if (all ([self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_1.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_6.text()])
                    and all([self.parent().ui.Object_List_LineEdited_0.text() !="", self.parent().ui.Object_List_LineEdited_1.text() !="",
                             self.parent().ui.Object_List_LineEdited_2.text() !="", self.parent().ui.Object_List_LineEdited_3.text() !="",
                             self.parent().ui.Object_List_LineEdited_4.text() !="", self.parent().ui.Object_List_LineEdited_5.text() !="",
                             self.parent().ui.Object_List_LineEdited_6.text() !=""])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_6.value()])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_1.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_2.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_3.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_4.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_5.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_6.value() in index_list])) :
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="", self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="", self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="", self.parent().ui.Object_List_LineEdited_5.text() =="",
                            self.parent().ui.Object_List_LineEdited_6.text() ==""])
                      and all ([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_6.value()])
                      and any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_1.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_2.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_5.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_6.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="",
                            self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="",
                            self.parent().ui.Object_List_LineEdited_5.text() =="",
                            self.parent().ui.Object_List_LineEdited_6.text() ==""])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_6.text()])
                     and any ([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_6.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects and spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_6.text()])):
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_6.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs has the same value /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_1.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_2.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_5.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_6.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 8 :
                index_list = [0,1,2,3,4,5,6,7]

                if (all ([self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_1.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_6.text() != self.parent().ui.Object_List_LineEdited_7.text()])
                    and all([self.parent().ui.Object_List_LineEdited_0.text() !="", self.parent().ui.Object_List_LineEdited_1.text() !="",
                             self.parent().ui.Object_List_LineEdited_2.text() !="", self.parent().ui.Object_List_LineEdited_3.text() !="",
                             self.parent().ui.Object_List_LineEdited_4.text() !="", self.parent().ui.Object_List_LineEdited_5.text() !="",
                             self.parent().ui.Object_List_LineEdited_6.text() !="", self.parent().ui.Object_List_LineEdited_7.text() !=""])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_7.value()])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_1.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_2.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_3.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_4.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_5.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_6.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_7.value() in index_list])) :
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="", self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="", self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="", self.parent().ui.Object_List_LineEdited_5.text() =="",
                            self.parent().ui.Object_List_LineEdited_6.text() =="", self.parent().ui.Object_List_LineEdited_7.text() ==""])
                      and all ([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_7.value()])
                      and any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_1.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_2.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_5.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_6.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_7.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="",
                            self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="",
                            self.parent().ui.Object_List_LineEdited_5.text() =="",
                            self.parent().ui.Object_List_LineEdited_6.text() =="",
                            self.parent().ui.Object_List_LineEdited_7.text() ==""])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_7.text()])
                     and any ([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_7.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects and spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_7.text()])):
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_7.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs has the same value /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_1.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_2.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_5.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_6.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_7.value() not in index_list])) :
                    self.parent().ui.Object_List_State_Text.setText
                    self.parent().ui.Object_List_State_Text.setStyleSheet("border : 2px solid red;")
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 9 :
                index_list = [0,1,2,3,4,5,6,7,8]

                if (all ([self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_1.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_6.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_6.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_7.text() != self.parent().ui.Object_List_LineEdited_8.text()])
                    and all([self.parent().ui.Object_List_LineEdited_0.text() !="", self.parent().ui.Object_List_LineEdited_1.text() !="",
                             self.parent().ui.Object_List_LineEdited_2.text() !="", self.parent().ui.Object_List_LineEdited_3.text() !="",
                             self.parent().ui.Object_List_LineEdited_4.text() !="", self.parent().ui.Object_List_LineEdited_5.text() !="",
                             self.parent().ui.Object_List_LineEdited_6.text() !="", self.parent().ui.Object_List_LineEdited_7.text() !="",
                             self.parent().ui.Object_List_LineEdited_8.text() !=""])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_7.value() != self.parent().ui.Object_List_SpinBox_8.value()])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_1.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_2.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_3.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_4.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_5.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_6.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_7.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_8.value() in index_list])) :
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="", self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="", self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="", self.parent().ui.Object_List_LineEdited_5.text() =="",
                            self.parent().ui.Object_List_LineEdited_6.text() =="", self.parent().ui.Object_List_LineEdited_7.text() =="",
                            self.parent().ui.Object_List_LineEdited_8.text() ==""])
                      and all ([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_7.value() != self.parent().ui.Object_List_SpinBox_8.value()])
                      and any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_1.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_2.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_5.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_6.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_7.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_8.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="",
                            self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="",
                            self.parent().ui.Object_List_LineEdited_5.text() =="",
                            self.parent().ui.Object_List_LineEdited_6.text() =="",
                            self.parent().ui.Object_List_LineEdited_7.text() =="",
                            self.parent().ui.Object_List_LineEdited_8.text() ==""])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_7.text() == self.parent().ui.Object_List_LineEdited_8.text()])
                     and any ([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_7.value() == self.parent().ui.Object_List_SpinBox_8.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects and spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_7.text() == self.parent().ui.Object_List_LineEdited_8.text()])):
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_7.value() == self.parent().ui.Object_List_SpinBox_8.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs has the same value /!\ " )
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_1.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_2.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_5.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_6.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_7.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_8.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

            if self.Object_List_Create_Counter_i == 10 :
                index_list = [0,1,2,3,4,5,6,7,8,9]

                if (all ([self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_1.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_0.text() != self.parent().ui.Object_List_LineEdited_9.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_2.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_1.text() != self.parent().ui.Object_List_LineEdited_9.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_3.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_2.text() != self.parent().ui.Object_List_LineEdited_9.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_4.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_3.text() != self.parent().ui.Object_List_LineEdited_9.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_5.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_4.text() != self.parent().ui.Object_List_LineEdited_9.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_6.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_5.text() != self.parent().ui.Object_List_LineEdited_9.text(),
                          self.parent().ui.Object_List_LineEdited_6.text() != self.parent().ui.Object_List_LineEdited_7.text(),
                          self.parent().ui.Object_List_LineEdited_6.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_6.text() != self.parent().ui.Object_List_LineEdited_9.text(),
                          self.parent().ui.Object_List_LineEdited_7.text() != self.parent().ui.Object_List_LineEdited_8.text(),
                          self.parent().ui.Object_List_LineEdited_7.text() != self.parent().ui.Object_List_LineEdited_9.text(),
                          self.parent().ui.Object_List_LineEdited_8.text() != self.parent().ui.Object_List_LineEdited_9.text()])
                    and all([self.parent().ui.Object_List_LineEdited_0.text() !="", self.parent().ui.Object_List_LineEdited_1.text() !="",
                             self.parent().ui.Object_List_LineEdited_2.text() !="", self.parent().ui.Object_List_LineEdited_3.text() !="",
                             self.parent().ui.Object_List_LineEdited_4.text() !="", self.parent().ui.Object_List_LineEdited_5.text() !="",
                             self.parent().ui.Object_List_LineEdited_6.text() !="", self.parent().ui.Object_List_LineEdited_7.text() !="",
                             self.parent().ui.Object_List_LineEdited_8.text() !="", self.parent().ui.Object_List_LineEdited_9.text() !=""])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                             self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                             self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                             self.parent().ui.Object_List_SpinBox_7.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                             self.parent().ui.Object_List_SpinBox_7.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                             self.parent().ui.Object_List_SpinBox_8.value() != self.parent().ui.Object_List_SpinBox_9.value()])
                    and all([self.parent().ui.Object_List_SpinBox_0.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_1.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_2.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_3.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_4.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_5.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_6.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_7.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_8.value() in index_list,
                             self.parent().ui.Object_List_SpinBox_9.value() in index_list])) :
                    self.parent().ui.Text_Object_List_State = ("All good")
                    self.parent().ui.Object_List_State_Value = 0
                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="", self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="", self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="", self.parent().ui.Object_List_LineEdited_5.text() =="",
                            self.parent().ui.Object_List_LineEdited_6.text() =="", self.parent().ui.Object_List_LineEdited_7.text() =="",
                            self.parent().ui.Object_List_LineEdited_8.text() =="", self.parent().ui.Object_List_LineEdited_9.text() ==""])
                      and all ([self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_1.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_0.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_2.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_1.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_3.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_2.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_4.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_3.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_5.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_4.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_6.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_5.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                                self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_7.value(),
                                self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_6.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                                self.parent().ui.Object_List_SpinBox_7.value() != self.parent().ui.Object_List_SpinBox_8.value(),
                                self.parent().ui.Object_List_SpinBox_7.value() != self.parent().ui.Object_List_SpinBox_9.value(),
                                self.parent().ui.Object_List_SpinBox_8.value() != self.parent().ui.Object_List_SpinBox_9.value()])
                      and any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_1.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_2.value()not in index_list,
                               self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_5.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_6.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_7.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_8.value() not in index_list,
                               self.parent().ui.Object_List_SpinBox_9.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled and spin boxs set from zero to n with one of range /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any ([self.parent().ui.Object_List_LineEdited_0.text() =="",
                            self.parent().ui.Object_List_LineEdited_1.text() =="",
                            self.parent().ui.Object_List_LineEdited_2.text() =="",
                            self.parent().ui.Object_List_LineEdited_3.text() =="",
                            self.parent().ui.Object_List_LineEdited_4.text() =="",
                            self.parent().ui.Object_List_LineEdited_5.text() =="",
                            self.parent().ui.Object_List_LineEdited_6.text() =="",
                            self.parent().ui.Object_List_LineEdited_7.text() =="",
                            self.parent().ui.Object_List_LineEdited_8.text() =="",
                            self.parent().ui.Object_List_LineEdited_9.text() ==""])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ All object list fields need to be filled /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_7.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_7.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_8.text() == self.parent().ui.Object_List_LineEdited_9.text()])
                     and any ([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                               self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                               self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                               self.parent().ui.Object_List_SpinBox_7.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                               self.parent().ui.Object_List_SpinBox_7.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                               self.parent().ui.Object_List_SpinBox_8.value() == self.parent().ui.Object_List_SpinBox_9.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects and spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_1.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_0.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_2.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_1.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_3.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_2.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_4.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_3.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_5.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_4.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_6.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_5.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_7.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_6.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_7.text() == self.parent().ui.Object_List_LineEdited_8.text(),
                           self.parent().ui.Object_List_LineEdited_7.text() == self.parent().ui.Object_List_LineEdited_9.text(),
                           self.parent().ui.Object_List_LineEdited_8.text() == self.parent().ui.Object_List_LineEdited_9.text()])):
                    self.parent().ui.Text_Object_List_State = (" /!\ Same name for objects /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_1.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_0.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_2.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_1.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_3.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_2.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_4.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_3.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_5.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_4.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_6.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_5.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                           self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_7.value(),
                           self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_6.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                           self.parent().ui.Object_List_SpinBox_7.value() == self.parent().ui.Object_List_SpinBox_8.value(),
                           self.parent().ui.Object_List_SpinBox_7.value() == self.parent().ui.Object_List_SpinBox_9.value(),
                           self.parent().ui.Object_List_SpinBox_8.value() == self.parent().ui.Object_List_SpinBox_9.value()])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs has the same value /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

                elif (any([self.parent().ui.Object_List_SpinBox_0.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_1.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_2.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_3.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_4.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_5.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_6.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_7.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_8.value() not in index_list,
                           self.parent().ui.Object_List_SpinBox_9.value() not in index_list])) :
                    self.parent().ui.Text_Object_List_State = (" /!\ Spin boxs set from zero to n with one of range /!\ ")
                    self.parent().ui.Object_List_State_Value = 1

    def connections(self):
        self.parent().ui.Object_List_LineEdit.textChanged.connect(self.Object_List_Input)
        self.parent().ui.Object_List_Add_Button.clicked.connect(self.Object_List_Create)
        self.parent().ui.Object_List_Add_Button.clicked.connect(self.parent().UpdateCheckableComboBox.Saved_Objects)
        self.parent().ui.Object_List_Button_Check.clicked.connect(self.Object_List_State)
        self.parent().ui.Object_List_Add_Button.clicked.connect(self.Object_List_State)
        self.parent().ui.Object_List_Add_Button.clicked.connect(self.parent().Manual_parameters.enabling)


class Check(QtCore.QObject):
    def __init__(self, parent):
        super(Check, self).__init__(parent)
        self.connections()

    def Check_State (self):

        self.parent().ui.msg = QMessageBox()
        self.parent().ui.msg.setWindowTitle("Program Status")
        self.parent().ui.msg.setStyleSheet("QLabel{font-size: 15px}");

        if self.parent().ui.Opening_Check_Box.isChecked() is True and self.parent().ui.Decimation_Process_Check_Box.isChecked() is True and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is True :
            if (any([self.parent().ui.Input_Output_State_Value == 1,
                 self.parent().ui.Object_List_State_Value == 1,
                 self.parent().ui.Voxel_State_Value == 1,
                 self.parent().ui.Opening_State_Value == 1,
                 self.parent().ui.Reduction_Coefficient_State_Value == 1,
                 self.parent().ui.Sphere_Size_State_Value ==1])):
                self.parent().ui.msg.setText("Wrong information input")
                self.parent().ui.msg.setIcon(QMessageBox.Warning)
                self.parent().ui.msg.setInformativeText("See detail error")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Opening : " + self.parent().ui.Text_Opening_State +'\n' +'\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Reduction Coefficient : " + self.parent().ui.Text_Reduction_Coefficient_State +'\n' + '\n' +
                                         "Sphere Size : " + self.parent().ui.Text_Sphere_Size_State)

            else :
                self.parent().ui.msg.setText("Ready to compute")
                self.parent().ui.msg.setInformativeText("All good")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Opening : " + self.parent().ui.Text_Opening_State +'\n' +'\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Reduction Coefficient : " + self.parent().ui.Text_Reduction_Coefficient_State +'\n' + '\n' +
                                         "Sphere Size : " + self.parent().ui.Text_Sphere_Size_State)
                self.parent().ui.msg.setIcon(QMessageBox.Information)

        elif self.parent().ui.Opening_Check_Box.isChecked() is True and self.parent().ui.Decimation_Process_Check_Box.isChecked() is True and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is False:
            if (any([self.parent().ui.Input_Output_State_Value == 1,
                 self.parent().ui.Object_List_State_Value == 1,
                 self.parent().ui.Voxel_State_Value == 1,
                 self.parent().ui.Opening_State_Value == 1,
                 self.parent().ui.Reduction_Coefficient_State_Value == 1])):
                self.parent().ui.msg.setText("Wrong information input")
                self.parent().ui.msg.setIcon(QMessageBox.Warning)
                self.parent().ui.msg.setInformativeText("See detail error")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Opening : " + self.parent().ui.Text_Opening_State +'\n' +'\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Reduction Coefficient : " + self.parent().ui.Text_Reduction_Coefficient_State)

            else :
                self.parent().ui.msg.setText("Ready to compute")
                self.parent().ui.msg.setInformativeText("All good")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Opening : " + self.parent().ui.Text_Opening_State +'\n' +'\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Reduction Coefficient : " + self.parent().ui.Text_Reduction_Coefficient_State)
                self.parent().ui.msg.setIcon(QMessageBox.Information)

        elif self.parent().ui.Opening_Check_Box.isChecked() is True and self.parent().ui.Decimation_Process_Check_Box.isChecked() is False and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is True :
            if (any([self.parent().ui.Input_Output_State_Value == 1,
                 self.parent().ui.Object_List_State_Value == 1,
                 self.parent().ui.Voxel_State_Value == 1,
                 self.parent().ui.Opening_State_Value == 1,
                 self.parent().ui.Sphere_Size_State_Value == 1])):
                self.parent().ui.msg.setText("Wrong information input")
                self.parent().ui.msg.setIcon(QMessageBox.Warning)
                self.parent().ui.msg.setInformativeText("See detail error")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Opening : " + self.parent().ui.Text_Opening_State +'\n' +'\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Sphere Size : " + self.parent().ui.Text_Sphere_Size_State)

            else :
                self.parent().ui.msg.setText("Ready to compute")
                self.parent().ui.msg.setInformativeText("All good")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Opening : " + self.parent().ui.Text_Opening_State +'\n' +'\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State  +'\n' + '\n' +
                                         "Sphere Size : " + self.parent().ui.Text_Sphere_Size_State)
                self.parent().ui.msg.setIcon(QMessageBox.Information)

        elif self.parent().ui.Opening_Check_Box.isChecked() is False and self.parent().ui.Decimation_Process_Check_Box.isChecked() is True and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is True :
            if (any([self.parent().ui.Input_Output_State_Value == 1,
                 self.parent().ui.Object_List_State_Value == 1,
                 self.parent().ui.Voxel_State_Value == 1,
                 self.parent().ui.Reduction_Coefficient_State_Value == 1,
                 self.parent().ui.Sphere_Size_State_Value == 1])):
                self.parent().ui.msg.setText("Wrong information input")
                self.parent().ui.msg.setIcon(QMessageBox.Warning)
                self.parent().ui.msg.setInformativeText("See detail error")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Reduction Coefficient : " + self.parent().ui.Text_Reduction_Coefficient_State +'\n' + '\n' +
                                         "Sphere Size : " + self.parent().ui.Text_Sphere_Size_State)

            else :
                self.parent().ui.msg.setText("Ready to compute")
                self.parent().ui.msg.setInformativeText("All good")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Reduction Coefficient : " + self.parent().ui.Text_Reduction_Coefficient_State +'\n' + '\n' +
                                         "Sphere Size : " + self.parent().ui.Text_Sphere_Size_State)
                self.parent().ui.msg.setIcon(QMessageBox.Information)

        elif self.parent().ui.Opening_Check_Box.isChecked() is False and self.parent().ui.Decimation_Process_Check_Box.isChecked() is False and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is True :
            if (any([self.parent().ui.Input_Output_State_Value == 1,
                 self.parent().ui.Object_List_State_Value == 1,
                 self.parent().ui.Voxel_State_Value == 1,
                 self.parent().ui.Sphere_Size_State_Value == 1])):
                self.parent().ui.msg.setText("Wrong information input")
                self.parent().ui.msg.setIcon(QMessageBox.Warning)
                self.parent().ui.msg.setInformativeText("See detail error")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Sphere Size : " + self.parent().ui.Text_Sphere_Size_State)

            else :
                self.parent().ui.msg.setText("Ready to compute")
                self.parent().ui.msg.setInformativeText("All good")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Sphere Size : " + self.parent().ui.Text_Sphere_Size_State)
                self.parent().ui.msg.setIcon(QMessageBox.Information)

        elif self.parent().ui.Opening_Check_Box.isChecked() is False and self.parent().ui.Decimation_Process_Check_Box.isChecked() is True and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is False :
            if (any([self.parent().ui.Input_Output_State_Value == 1,
                 self.parent().ui.Object_List_State_Value == 1,
                 self.parent().ui.Voxel_State_Value == 1,
                 self.parent().ui.Reduction_Coefficient_State_Value == 1])):
                self.parent().ui.msg.setText("Wrong information input")
                self.parent().ui.msg.setIcon(QMessageBox.Warning)
                self.parent().ui.msg.setInformativeText("See detail error")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Reduction Coefficient : " + self.parent().ui.Text_Reduction_Coefficient_State)


            else :
                self.parent().ui.msg.setText("Ready to compute")
                self.parent().ui.msg.setInformativeText("All good")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State +'\n' + '\n' +
                                         "Reduction Coefficient : " + self.parent().ui.Text_Reduction_Coefficient_State)

                self.parent().ui.msg.setIcon(QMessageBox.Information)

        elif self.parent().ui.Opening_Check_Box.isChecked() is True and self.parent().ui.Decimation_Process_Check_Box.isChecked() is False and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is False :
            if (any([self.parent().ui.Input_Output_State_Value == 1,
                 self.parent().ui.Object_List_State_Value == 1,
                 self.parent().ui.Voxel_State_Value == 1,
                 self.parent().ui.Opening_State_Value == 1])):
                self.parent().ui.msg.setText("Wrong information input")
                self.parent().ui.msg.setIcon(QMessageBox.Warning)
                self.parent().ui.msg.setInformativeText("See detail error")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Opening : " + self.parent().ui.Text_Opening_State +'\n' +'\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State)


            else :
                self.parent().ui.msg.setText("Ready to compute")
                self.parent().ui.msg.setInformativeText("All good")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Opening : " + self.parent().ui.Text_Opening_State +'\n' +'\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State)

                self.parent().ui.msg.setIcon(QMessageBox.Information)

        else :
            if (any([self.parent().ui.Input_Output_State_Value == 1,
                     self.parent().ui.Object_List_State_Value == 1,
                     self.parent().ui.Voxel_State_Value == 1])):
                self.parent().ui.msg.setText("Wrong information input")
                self.parent().ui.msg.setIcon(QMessageBox.Warning)
                self.parent().ui.msg.setInformativeText("See detail error")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State)

            else :
                self.parent().ui.msg.setText("Ready to compute")
                self.parent().ui.msg.setInformativeText("All good")
                self.parent().ui.msg.setDetailedText("Input/Output : " + self.parent().ui.Text_Input_Output_State +'\n' + '\n' +
                                         "Voxel : " + self.parent().ui.Text_Voxel_State +'\n' + '\n' +
                                         "Object List : " + self.parent().ui.Text_Object_List_State)
                self.parent().ui.msg.setIcon(QMessageBox.Information)

        x = self.parent().ui.msg.exec_()
        self.parent().ui.msg.setStandardButtons(QMessageBox.Cancel)

        self.parent().ui.msg.buttonClicked.connect(self.popup_button)


    def popup_button(self, i):
        print(i.text())

    def connections(self):
        self.parent().ui.Object_List_Button_Check.clicked.connect(self.parent().ObjectList.Object_List_State)
        self.parent().ui.Object_List_Button_Check.clicked.connect(self.parent().IO.Check_Input_Output_State)
        self.parent().ui.Object_List_Button_Check.clicked.connect(self.Check_State)


class Run(QtCore.QObject):
    def __init__(self, parent):
        super(Run, self).__init__(parent)
        self.connections()

    def Run_State (self):

        if (all([self.parent().ui.Object_List_State_Value == 0,
                 self.parent().ui.Voxel_State_Value == 0,
                 self.parent().ui.Input_Output_State_Value == 0])):
            if self.parent().ui.Opening_Check_Box.isChecked() is True and self.parent().ui.Decimation_Process_Check_Box.isChecked() is True and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is True :
                if all ([self.parent().ui.Sphere_Size_State_Value == 0,
                         self.parent().ui.Reduction_Coefficient_State_Value == 0,
                         self.parent().ui.Opening_State_Value == 0]):
                    self.parent().ui.Execute_Button.setEnabled(True)
                else :
                    self.parent().ui.Execute_Button.setEnabled(False)
            elif self.parent().ui.Opening_Check_Box.isChecked() is True and self.parent().ui.Decimation_Process_Check_Box.isChecked() is True and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is False :
                if all ([self.parent().ui.Reduction_Coefficient_State_Value == 0,
                         self.parent().ui.Opening_State_Value == 0]):
                    self.parent().ui.Execute_Button.setEnabled(True)
                else :
                    self.parent().ui.Execute_Button.setEnabled(False)
            elif self.parent().ui.Opening_Check_Box.isChecked() is True and self.parent().ui.Decimation_Process_Check_Box.isChecked() is False and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is True :
                if all ([self.parent().ui.Sphere_Size_State_Value == 0,
                         self.parent().ui.Opening_State_Value == 0]):
                    self.parent().ui.Execute_Button.setEnabled(True)
                else :
                    self.parent().ui.Execute_Button.setEnabled(False)
            elif self.parent().ui.Opening_Check_Box.isChecked() is False and self.parent().ui.Decimation_Process_Check_Box.isChecked() is True and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is True :
                if all ([self.parent().ui.Sphere_Size_State_Value == 0,
                         self.parent().ui.Reduction_Coefficient_State_Value == 0]):
                    self.parent().ui.Execute_Button.setEnabled(True)
                else :
                    self.parent().ui.Execute_Button.setEnabled(False)
            elif self.parent().ui.Opening_Check_Box.isChecked() is False and self.parent().ui.Decimation_Process_Check_Box.isChecked() is False and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is True :
                if self.parent().ui.Sphere_Size_State_Value == 0 :
                    self.parent().ui.Execute_Button.setEnabled(True)
                else :
                    self.parent().ui.Execute_Button.setEnabled(False)
            elif self.parent().ui.Opening_Check_Box.isChecked() is False and self.parent().ui.Decimation_Process_Check_Box.isChecked() is True and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is False :
                if self.parent().ui.Reduction_Coefficient_State_Value == 0 :
                    self.parent().ui.Execute_Button.setEnabled(True)
                else :
                    self.parent().ui.Execute_Button.setEnabled(False)
            elif self.parent().ui.Opening_Check_Box.isChecked() is True and self.parent().ui.Decimation_Process_Check_Box.isChecked() is False and self.parent().ui.Show_Mass_Center_Check_Box.isChecked() is False :
                if self.parent().ui.Opening_State_Value == 0 :
                    self.parent().ui.Execute_Button.setEnabled(True)
                else :
                    self.parent().ui.Execute_Button.setEnabled(False)
            else :
                    self.parent().ui.Execute_Button.setEnabled(True)
        else :
            self.parent().ui.Execute_Button.setEnabled(False)


    def reset(self):
        self.parent().ui.Input_Folder_Directory_Text.clear()
        self.parent().ui.Output_Folder_Directory_Text.clear()
        self.parent().ui.Input_Folder_Directory_Text.setStyleSheet("")
        self.parent().ui.Output_Folder_Directory_Text.setStyleSheet("")
        self.parent().ui.X_Size_Text_Box.setText("0.0")
        self.parent().ui.Borders_Off_Check_Box.setChecked(False)
        self.parent().ui.Gradient_value_lineEdit.setText("0.0")
        self.parent().ui.Segmentation_Methode_2_ComboBox.setCurrentIndex(0)
        self.parent().ui.Segmentation_Methode_1_ComboBox.setCurrentIndex(0)
        self.parent().ui.Opening_Check_Box.setChecked(False)
        self.parent().ui.Opening_Methode_Combo_Box.setCurrentIndex(0)
        self.parent().ui.Opening_Size_X_Text_Box.setText("1")
        self.parent().ui.Opening_Size_Y_Text_Box.setText("1")
        self.parent().ui.Connectivity_Check_Box.setChecked(False)
        self.parent().ui.Manual_checkBox.setChecked(False)
        self.parent().ui.Save_Frames_Check_Box.setChecked(True)
        self.parent().ui.Save_Frame_Methode_Combo_Box.setCurrentIndex(0)
        self.parent().ui.Decimation_Process_Check_Box.setChecked(False)
        self.parent().ui.Preserve_Topology_Check_Box.setChecked(True)
        self.parent().ui.Display_STL_Check_Box.setChecked(False)
        self.parent().ui.Mass_Center_Check_Box.setChecked(False)
        self.parent().ui.Show_Mass_Center_Check_Box.setChecked(False)
        self.parent().ui.Object_List_LineEdit.clear()
        self.parent().ui.Sphere_Size_Text_Box.setText("4.0")
        self.parent().ui.Picture_number_LineEdit.setText("0")
        self.parent().ui.min_threshold_lineEdit.setText("0")
        self.parent().ui.max_threshold_lineEdit.setText("65535")
        self.parent().ui.Activate_thresholding_checkBox.setChecked(False)
        self.parent().ui.Reduction_Coefficient_Text_Box.setText("0.0")
        self.parent().ui.Save_STL_Check_Box.setChecked(False)
        self.parent().ui.Picture_number_horizontalScrollBar.setMaximum(99)
        self.parent().ui.Frame_section_Check_Box.setChecked(False)
        for i in range(len(self.parent().ui.STL_Save_Combo_Box_List)):
            item = self.parent().ui.STL_Save_Combo_Box.model().item(i, 0)
            if i == 0 :
                item.setCheckState(Qt.Checked)
            else :
                item.setCheckState(Qt.Unchecked)
        self.parent().ui.STL_Save_Combo_Box.setCurrentIndex(0)
        while self.parent().ObjectList.Object_List_Create_Counter_i != 0 :
            self.parent().ObjectList.Object_List_Delete()
        self.parent().UpdateCheckableComboBox.Saved_Objects()


    def connections(self):
        self.parent().ui.X_Size_Text_Box.textChanged.connect(self.Run_State)
        self.parent().ui.Y_Size_Text_Box.textChanged.connect(self.Run_State)
        self.parent().ui.Z_Size_Text_Box.textChanged.connect(self.Run_State)
        self.parent().ui.Opening_Check_Box.stateChanged.connect(self.Run_State)
        self.parent().ui.Opening_Size_X_Text_Box.textChanged.connect(self.Run_State)
        self.parent().ui.Opening_Size_Y_Text_Box.textChanged.connect(self.Run_State)
        self.parent().ui.Save_STL_Check_Box.stateChanged.connect(self.Run_State)
        self.parent().ui.Input_Folder_Directory_Text.textChanged.connect(self.Run_State)
        self.parent().ui.Output_Folder_Directory_Text.textChanged.connect(self.Run_State)
        self.parent().ui.Reduction_Coefficient_Text_Box.textChanged.connect(self.Run_State)
        self.parent().ui.Decimation_Process_Check_Box.stateChanged.connect(self.Run_State)
        self.parent().ui.Object_List_Add_Button.clicked.connect(self.Run_State)
        self.parent().ui.Object_List_Button_Check.clicked.connect(self.Run_State)
        self.parent().ui.Sphere_Size_Text_Box.textChanged.connect(self.Run_State)
        self.parent().ui.Show_Mass_Center_Check_Box.stateChanged.connect(self.Run_State)
        self.parent().ui.Reset_pushButton.clicked.connect(self.reset)


class ProgressBar(QtCore.QObject):
    def __init__(self, parent):
        super(ProgressBar, self).__init__(parent)
        self.init_Parameters()
    #
    def init_Parameters(self):
        self.Re_run = 0
        self.Sum_All_Frames_Re_run = 0
        self.Borders_Off_Re_run = 0
        self.Segmentation_Label_Re_run = 0
        self.Connectivity_3D_Re_run = 0
        self.Opening_Re_run = 0
        self.Frame_saved_Re_run = 0
        self.Frame_section_Re_run = 0
        self.Stl_maker_Re_run = 0
        self.Decimation_process_Re_run = 0
        self.Mass_centerDf_Re_run = 0

    def Creation_progress_bar(self):
        if self.Re_run == 1 :
            self.parent().ui.gridLayout_2.deleteLater()
            self.parent().ui.gridLayout_2 = None
            if self.Sum_All_Frames_Re_run == 1 :
                self.parent().ui.Sum_All_Frames_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Sum_All_Frames_Label)
                self.parent().ui.Sum_All_Frames_Label.deleteLater()
                self.parent().ui.Sum_All_Frames_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Sum_All_Frames_ProgBar)
                self.parent().ui.Sum_All_Frames_ProgBar.deleteLater()
                self.parent().ui.Sum_All_Frames_ProgBar = None
                self.Sum_All_Frames_Re_run = 0

            if self.Borders_Off_Re_run == 1 :
                self.parent().ui.Borders_Off_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Borders_Off_Label)
                self.parent().ui.Borders_Off_Label.deleteLater()
                self.parent().ui.Borders_Off_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Borders_Off_ProgBar)
                self.parent().ui.Borders_Off_ProgBar.deleteLater()
                self.parent().ui.Borders_Off_ProgBar = None
                self.Borders_Off_Re_run = 0

            if self.Segmentation_Label_Re_run == 1 :
                self.parent().ui.Segmentation_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Segmentation_Label)
                self.parent().ui.Segmentation_Label.deleteLater()
                self.parent().ui.Segmentation_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Segmentation_ProgBar)
                self.parent().ui.Segmentation_ProgBar.deleteLater()
                self.parent().ui.Segmentation_ProgBar = None
                self.Segmentation_Label_Re_run = 0

            if self.Connectivity_3D_Re_run == 1 :
                self.parent().ui.Connectivity_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Connectivity_Label)
                self.parent().ui.Connectivity_Label.deleteLater()
                self.parent().ui.Connectivity_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Connectivity_ProgBar)
                self.parent().ui.Connectivity_ProgBar.deleteLater()
                self.parent().ui.Connectivity_ProgBar = None
                self.Connectivity_3D_Re_run = 0

            if self.Opening_Re_run == 1 :
                self.parent().ui.Opening_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Opening_Label)
                self.parent().ui.Opening_Label.deleteLater()
                self.parent().ui.Opening_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Opening_ProgBar)
                self.parent().ui.Opening_ProgBar.deleteLater()
                self.parent().ui.Opening_ProgBar = None
                self.Opening_Re_run = 0

            if self.Frame_saved_Re_run == 1 :
                self.parent().ui.Save_Frames_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Save_Frames_Label)
                self.parent().ui.Save_Frames_Label.deleteLater()
                self.parent().ui.Save_Frames_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Save_Frames_ProgBar)
                self.parent().ui.Save_Frames_ProgBar.deleteLater()
                self.parent().ui.Save_Frames_ProgBar = None
                self.Frame_saved_Re_run = 0


            if self.Frame_section_Re_run == 1 :
                self.parent().ui.Frame_section_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Frame_section_Label)
                self.parent().ui.Frame_section_Label.deleteLater()
                self.parent().ui.Frame_section_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Frame_section_ProgBar)
                self.parent().ui.Frame_section_ProgBar.deleteLater()
                self.parent().ui.Frame_section_ProgBar = None
                self.Frame_section_Re_run = 0

            if self.Stl_maker_Re_run == 1 :
                self.parent().ui.Save_STL_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Save_STL_Label)
                self.parent().ui.Save_STL_Label.deleteLater()
                self.parent().ui.Save_STL_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Save_STL_ProgBar)
                self.parent().ui.Save_STL_ProgBar.deleteLater()
                self.parent().ui.Save_STL_ProgBar = None
                self.Stl_maker_Re_run = 0

            if self.Decimation_process_Re_run == 1 :
                self.parent().ui.Decimation_process_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Decimation_process_Label)
                self.parent().ui.Decimation_process_Label.deleteLater()
                self.parent().ui.Decimation_process_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Decimation_process_ProgBar)
                self.parent().ui.Decimation_process_ProgBar.deleteLater()
                self.parent().ui.Decimation_process_ProgBar = None
                self.Decimation_process_Re_run = 0

            if self.Mass_centerDf_Re_run == 1 :
                self.parent().ui.Mass_centerDf_ProgBar.setProperty("value", 0)
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Mass_centerDf_Label)
                self.parent().ui.Mass_centerDf_Label.deleteLater()
                self.parent().ui.Mass_centerDf_Label = None
                self.parent().ui.gridLayout_2.removeWidget(self.parent().ui.Mass_centerDf_ProgBar)
                self.parent().ui.Mass_centerDf_ProgBar.deleteLater()
                self.parent().ui.Mass_centerDf_ProgBar = None
                self.Mass_centerDf_Re_run = 0

            self.parent().ui.gridLayout_2 = QtWidgets.QGridLayout(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.gridLayout_2.setContentsMargins(5, 5, 5, 5)
            self.parent().ui.gridLayout_2.setSpacing(10)
            self.parent().ui.gridLayout_2.setObjectName("gridLayout_2")
            self.parent().ui.scrollArea.setWidget(self.parent().ui.scrollAreaWidgetContents)

        Y_position = 0
        self.parent().ui.Sum_All_Frames_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
        self.parent().ui.Sum_All_Frames_Label.setObjectName("Sum_All_Frames_Label")
        self.parent().ui.Sum_All_Frames_Label.setText("Sum all frames")
        self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Sum_All_Frames_Label, Y_position, 0, 1, 1)
        self.parent().ui.Sum_All_Frames_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
        self.parent().ui.Sum_All_Frames_ProgBar.setProperty("value", 0)
        self.parent().ui.Sum_All_Frames_ProgBar.setObjectName("Sum_All_Frames_ProgBar")
        self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Sum_All_Frames_ProgBar, Y_position, 1, 1, 1)
        self.Sum_All_Frames_Re_run = 1
        Y_position = Y_position + 1

        if self.parent().Tomography.Borders_Off is True :
            self.parent().ui.Borders_Off_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Borders_Off_Label.setObjectName("Borders_Off_Label")
            self.parent().ui.Borders_Off_Label.setText("Borders off")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Borders_Off_Label, Y_position, 0, 1, 1)
            self.parent().ui.Borders_Off_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Borders_Off_ProgBar.setProperty("value", 0)
            self.parent().ui.Borders_Off_ProgBar.setObjectName("Borders_Off_ProgBar")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Borders_Off_ProgBar, Y_position, 1, 1, 1)
            self.Borders_Off_Re_run = 1
            Y_position = Y_position + 1

        self.parent().ui.Segmentation_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
        self.parent().ui.Segmentation_Label.setObjectName("Segmentation_Label")
        self.parent().ui.Segmentation_Label.setText("Segmentation")
        self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Segmentation_Label, Y_position, 0, 1, 1)
        self.parent().ui.Segmentation_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
        self.parent().ui.Segmentation_ProgBar.setProperty("value", 0)
        self.parent().ui.Segmentation_ProgBar.setObjectName("Segmentation_ProgBar")
        self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Segmentation_ProgBar, Y_position, 1, 1, 1)
        self.Segmentation_Label_Re_run = 1

        Y_position = Y_position + 1

        if self.parent().Tomography.Connectivity_3D is True :
            self.parent().ui.Connectivity_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Connectivity_Label.setObjectName("Connectivity_Label")
            self.parent().ui.Connectivity_Label.setText("Connectivity")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Connectivity_Label, Y_position, 0, 1, 1)
            self.parent().ui.Connectivity_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Connectivity_ProgBar.setProperty("value", 0)
            self.parent().ui.Connectivity_ProgBar.setObjectName("Connectivity_ProgBar")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Connectivity_ProgBar, Y_position, 1, 1, 1)
            self.Connectivity_3D_Re_run = 1

            Y_position = Y_position + 1

        if self.parent().Tomography.Opening is True :
            self.parent().ui.Opening_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Opening_Label.setObjectName("Opening_Label")
            self.parent().ui.Opening_Label.setText("Erode/Dilate")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Opening_Label, Y_position, 0, 1, 1)
            self.parent().ui.Opening_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Opening_ProgBar.setProperty("value", 0)
            self.parent().ui.Opening_ProgBar.setObjectName("Opening_ProgBar")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Opening_ProgBar, Y_position, 1, 1, 1)
            self.Opening_Re_run = 1

            Y_position = Y_position + 1

        if self.parent().Tomography.Frame_saved is True :
            self.parent().ui.Save_Frames_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Save_Frames_Label.setObjectName("Save_Frames_Label")
            self.parent().ui.Save_Frames_Label.setText("Data back-up")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Save_Frames_Label, Y_position, 0, 1, 1)
            self.parent().ui.Save_Frames_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Save_Frames_ProgBar.setProperty("value", 0)
            self.parent().ui.Save_Frames_ProgBar.setObjectName("Save_Frames_ProgBar")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Save_Frames_ProgBar, Y_position, 1, 1, 1)
            self.Frame_saved_Re_run = 1

            Y_position = Y_position + 1

        if self.parent().Tomography.Frame_section is True :
            self.parent().ui.Frame_section_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Frame_section_Label.setObjectName("Frame_section_Label")
            self.parent().ui.Frame_section_Label.setText("Frame section")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Frame_section_Label, Y_position, 0, 1, 1)
            self.parent().ui.Frame_section_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Frame_section_ProgBar.setProperty("value", 0)
            self.parent().ui.Frame_section_ProgBar.setObjectName("Frame_section_ProgBar")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Frame_section_ProgBar, Y_position, 1, 1, 1)
            self.Frame_section_Re_run = 1

            Y_position = Y_position + 1

        if self.parent().Tomography.Stl_maker is True :
            self.parent().ui.Save_STL_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Save_STL_Label.setObjectName("Save_STL_Label")
            self.parent().ui.Save_STL_Label.setText("STL build")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Save_STL_Label, Y_position, 0, 1, 1)
            self.parent().ui.Save_STL_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Save_STL_ProgBar.setProperty("value", 0)
            self.parent().ui.Save_STL_ProgBar.setObjectName("Save_STL_ProgBar")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Save_STL_ProgBar, Y_position, 1, 1, 1)
            self.Stl_maker_Re_run = 1

            Y_position += 1

        if self.parent().Tomography.Decimation_process is True :
            self.parent().ui.Decimation_process_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Decimation_process_Label.setObjectName("Decimation_process_Label")
            self.parent().ui.Decimation_process_Label.setText("STL decimation process")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Decimation_process_Label, Y_position, 0, 1, 1)
            self.parent().ui.Decimation_process_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Decimation_process_ProgBar.setProperty("value", 0)
            self.parent().ui.Decimation_process_ProgBar.setObjectName("Decimation_process_ProgBar")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Decimation_process_ProgBar, Y_position, 1, 1, 1)
            self.Decimation_process_Re_run = 1

            Y_position += 1

        if self.parent().Tomography.Mass_centerDf is True :
            self.parent().ui.Mass_centerDf_Label = QtWidgets.QLabel(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Mass_centerDf_Label.setObjectName("Mass_centerDf_Label")
            self.parent().ui.Mass_centerDf_Label.setText("Mass center")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Mass_centerDf_Label, Y_position, 0, 1, 1)
            self.parent().ui.Mass_centerDf_ProgBar = QtWidgets.QProgressBar(self.parent().ui.scrollAreaWidgetContents)
            self.parent().ui.Mass_centerDf_ProgBar.setProperty("value", 0)
            self.parent().ui.Mass_centerDf_ProgBar.setObjectName("Mass_centerDf_ProgBar")
            self.parent().ui.gridLayout_2.addWidget(self.parent().ui.Mass_centerDf_ProgBar, Y_position, 1, 1, 1)
            self.Mass_centerDf_Re_run = 1
            Y_position += 1

        self.Re_run = 1


    def connections(self):
        pass



class Tomography(QThread) :
    Sum_All_Frames_progressChanged = QtCore.pyqtSignal(int)
    Borders_Off_progressChanged = QtCore.pyqtSignal(int)
    Segmentation_progressChanged = QtCore.pyqtSignal(int)
    Connectivity_progressChanged = QtCore.pyqtSignal(int)
    Opening_progressChanged = QtCore.pyqtSignal(int)
    Save_Frames_progressChanged = QtCore.pyqtSignal(int)
    Frame_section_progressChanged = QtCore.pyqtSignal(int)
    Save_STL_progressChanged = QtCore.pyqtSignal(int)
    Decimation_process_progressChanged = QtCore.pyqtSignal(int)
    Mass_centerDf_progressChanged = QtCore.pyqtSignal(int)

    def __init__(self, parent):
        super(Tomography, self).__init__(parent)

        self.init_Parameters()
        self.connections()


    def init_Parameters(self):
        self.a = 0
        pass


    def connections(self):
        self.parent().ui.Execute_Button.released.connect(self.Execution)
        self.parent().ui.Execute_Button.pressed.connect(self.SetUp)
        self.parent().ui.Visualize_border_off_pushButton.clicked.connect(self.borders_off_visualization)

    def Execution(self):

        self.start()

    def SetUp(self):
        self.settings()
        self.progress_bar = ProgressBar(parent=self.parent())
        self.progress_bar.Creation_progress_bar()
        while not self.parent().ui.Sum_All_Frames_ProgBar.isTextVisible():
            self.progress_bar = ProgressBar(parent=self.parent())
            self.progress_bar.Creation_progress_bar()


        self.Sum_All_Frames_progressChanged.connect(self.parent().ui.Sum_All_Frames_ProgBar.setValue)
        if self.Borders_Off is True :
            self.Borders_Off_progressChanged.connect(self.parent().ui.Borders_Off_ProgBar.setValue)
        self.Segmentation_progressChanged.connect(self.parent().ui.Segmentation_ProgBar.setValue)
        if self.Opening is True :
            self.Opening_progressChanged.connect(self.parent().ui.Opening_ProgBar.setValue)
        if self.Connectivity_3D is True :
            self.Connectivity_progressChanged.connect(self.parent().ui.Connectivity_ProgBar.setValue)
        if self.Frame_saved is True :
            self.Save_Frames_progressChanged.connect(self.parent().ui.Save_Frames_ProgBar.setValue)
        if self.Frame_section is True :
            self.Frame_section_progressChanged.connect(self.parent().ui.Frame_section_ProgBar.setValue)
        if self.Stl_maker is True :
            self.Save_STL_progressChanged.connect(self.parent().ui.Save_STL_ProgBar.setValue)
        if self.Decimation_process is True :
            self.Decimation_process_progressChanged.connect(self.parent().ui.Decimation_process_ProgBar.setValue)
        if self.Mass_centerDf is True :
            self.Mass_centerDf_progressChanged.connect(self.parent().ui.Mass_centerDf_ProgBar.setValue)


    def settings(self):
        self.Input_dir = self.parent().ui.Input_Folder_Directory_Text.text()
        self.format_img = ".tif"
        self.impathes = sorted([f for f in os.listdir(self.Input_dir) if f.endswith(self.format_img)])
        self.Output_dir = self.parent().ui.Output_Folder_Directory_Text.text()
        self.methode1 = str(self.parent().ui.Segmentation_Methode_1_ComboBox.currentText())
        self.methode2 = str(self.parent().ui.Segmentation_Methode_2_ComboBox.currentText())
        self.object_list = [None]*self.parent().ObjectList.Object_List_Create_Counter_i

        self.Save_Frames_List = [None]*self.parent().ObjectList.Object_List_Create_Counter_i
        self.Stl_list = [None]*self.parent().ObjectList.Object_List_Create_Counter_i
        self.Display_STL_List = [None]*self.parent().ObjectList.Object_List_Create_Counter_i

        if self.parent().ObjectList.Object_List_Create_Counter_i == 1 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 2 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
            self.Save_Frames_List[1] = self.parent().ui.Save_Frames_Combo_Box.item_checked(1)
            self.Stl_list[1] = self.parent().ui.Save_STL_Combo_Box.item_checked(1)
            self.Display_STL_List[1] = self.parent().ui.STL_Display_Combo_Box.item_checked(1)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 3 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
            self.Save_Frames_List[1] = self.parent().ui.Save_Frames_Combo_Box.item_checked(1)
            self.Stl_list[1] = self.parent().ui.Save_STL_Combo_Box.item_checked(1)
            self.Display_STL_List[1] = self.parent().ui.STL_Display_Combo_Box.item_checked(1)
            self.Save_Frames_List[2] = self.parent().ui.Save_Frames_Combo_Box.item_checked(2)
            self.Stl_list[2] = self.parent().ui.Save_STL_Combo_Box.item_checked(2)
            self.Display_STL_List[2] = self.parent().ui.STL_Display_Combo_Box.item_checked(2)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 4 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
            self.Save_Frames_List[1] = self.parent().ui.Save_Frames_Combo_Box.item_checked(1)
            self.Stl_list[1] = self.parent().ui.Save_STL_Combo_Box.item_checked(1)
            self.Display_STL_List[1] = self.parent().ui.STL_Display_Combo_Box.item_checked(1)
            self.Save_Frames_List[2] = self.parent().ui.Save_Frames_Combo_Box.item_checked(2)
            self.Stl_list[2] = self.parent().ui.Save_STL_Combo_Box.item_checked(2)
            self.Display_STL_List[2] = self.parent().ui.STL_Display_Combo_Box.item_checked(2)
            self.Save_Frames_List[3] = self.parent().ui.Save_Frames_Combo_Box.item_checked(3)
            self.Stl_list[3] = self.parent().ui.Save_STL_Combo_Box.item_checked(3)
            self.Display_STL_List[3] = self.parent().ui.STL_Display_Combo_Box.item_checked(3)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 5 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
            self.Save_Frames_List[1] = self.parent().ui.Save_Frames_Combo_Box.item_checked(1)
            self.Stl_list[1] = self.parent().ui.Save_STL_Combo_Box.item_checked(1)
            self.Display_STL_List[1] = self.parent().ui.STL_Display_Combo_Box.item_checked(1)
            self.Save_Frames_List[2] = self.parent().ui.Save_Frames_Combo_Box.item_checked(2)
            self.Stl_list[2] = self.parent().ui.Save_STL_Combo_Box.item_checked(2)
            self.Display_STL_List[2] = self.parent().ui.STL_Display_Combo_Box.item_checked(2)
            self.Save_Frames_List[3] = self.parent().ui.Save_Frames_Combo_Box.item_checked(3)
            self.Stl_list[3] = self.parent().ui.Save_STL_Combo_Box.item_checked(3)
            self.Display_STL_List[3] = self.parent().ui.STL_Display_Combo_Box.item_checked(3)
            self.Save_Frames_List[4] = self.parent().ui.Save_Frames_Combo_Box.item_checked(4)
            self.Stl_list[4] = self.parent().ui.Save_STL_Combo_Box.item_checked(4)
            self.Display_STL_List[4] = self.parent().ui.STL_Display_Combo_Box.item_checked(4)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 6 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
            self.Save_Frames_List[1] = self.parent().ui.Save_Frames_Combo_Box.item_checked(1)
            self.Stl_list[1] = self.parent().ui.Save_STL_Combo_Box.item_checked(1)
            self.Display_STL_List[1] = self.parent().ui.STL_Display_Combo_Box.item_checked(1)
            self.Save_Frames_List[2] = self.parent().ui.Save_Frames_Combo_Box.item_checked(2)
            self.Stl_list[2] = self.parent().ui.Save_STL_Combo_Box.item_checked(2)
            self.Display_STL_List[2] = self.parent().ui.STL_Display_Combo_Box.item_checked(2)
            self.Save_Frames_List[3] = self.parent().ui.Save_Frames_Combo_Box.item_checked(3)
            self.Stl_list[3] = self.parent().ui.Save_STL_Combo_Box.item_checked(3)
            self.Display_STL_List[3] = self.parent().ui.STL_Display_Combo_Box.item_checked(3)
            self.Save_Frames_List[4] = self.parent().ui.Save_Frames_Combo_Box.item_checked(4)
            self.Stl_list[4] = self.parent().ui.Save_STL_Combo_Box.item_checked(4)
            self.Display_STL_List[4] = self.parent().ui.STL_Display_Combo_Box.item_checked(4)
            self.Save_Frames_List[5] = self.parent().ui.Save_Frames_Combo_Box.item_checked(5)
            self.Stl_list[5] = self.parent().ui.Save_STL_Combo_Box.item_checked(5)
            self.Display_STL_List[5] = self.parent().ui.STL_Display_Combo_Box.item_checked(5)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 7 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_6.value()] = self.parent().ui.Object_List_LineEdited_6.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
            self.Save_Frames_List[1] = self.parent().ui.Save_Frames_Combo_Box.item_checked(1)
            self.Stl_list[1] = self.parent().ui.Save_STL_Combo_Box.item_checked(1)
            self.Display_STL_List[1] = self.parent().ui.STL_Display_Combo_Box.item_checked(1)
            self.Save_Frames_List[2] = self.parent().ui.Save_Frames_Combo_Box.item_checked(2)
            self.Stl_list[2] = self.parent().ui.Save_STL_Combo_Box.item_checked(2)
            self.Display_STL_List[2] = self.parent().ui.STL_Display_Combo_Box.item_checked(2)
            self.Save_Frames_List[3] = self.parent().ui.Save_Frames_Combo_Box.item_checked(3)
            self.Stl_list[3] = self.parent().ui.Save_STL_Combo_Box.item_checked(3)
            self.Display_STL_List[3] = self.parent().ui.STL_Display_Combo_Box.item_checked(3)
            self.Save_Frames_List[4] = self.parent().ui.Save_Frames_Combo_Box.item_checked(4)
            self.Stl_list[4] = self.parent().ui.Save_STL_Combo_Box.item_checked(4)
            self.Display_STL_List[4] = self.parent().ui.STL_Display_Combo_Box.item_checked(4)
            self.Save_Frames_List[5] = self.parent().ui.Save_Frames_Combo_Box.item_checked(5)
            self.Stl_list[5] = self.parent().ui.Save_STL_Combo_Box.item_checked(5)
            self.Display_STL_List[5] = self.parent().ui.STL_Display_Combo_Box.item_checked(5)
            self.Save_Frames_List[6] = self.parent().ui.Save_Frames_Combo_Box.item_checked(6)
            self.Stl_list[6] = self.parent().ui.Save_STL_Combo_Box.item_checked(6)
            self.Display_STL_List[6] = self.parent().ui.STL_Display_Combo_Box.item_checked(6)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 8 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_6.value()] = self.parent().ui.Object_List_LineEdited_6.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_7.value()] = self.parent().ui.Object_List_LineEdited_7.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
            self.Save_Frames_List[1] = self.parent().ui.Save_Frames_Combo_Box.item_checked(1)
            self.Stl_list[1] = self.parent().ui.Save_STL_Combo_Box.item_checked(1)
            self.Display_STL_List[1] = self.parent().ui.STL_Display_Combo_Box.item_checked(1)
            self.Save_Frames_List[2] = self.parent().ui.Save_Frames_Combo_Box.item_checked(2)
            self.Stl_list[2] = self.parent().ui.Save_STL_Combo_Box.item_checked(2)
            self.Display_STL_List[2] = self.parent().ui.STL_Display_Combo_Box.item_checked(2)
            self.Save_Frames_List[3] = self.parent().ui.Save_Frames_Combo_Box.item_checked(3)
            self.Stl_list[3] = self.parent().ui.Save_STL_Combo_Box.item_checked(3)
            self.Display_STL_List[3] = self.parent().ui.STL_Display_Combo_Box.item_checked(3)
            self.Save_Frames_List[4] = self.parent().ui.Save_Frames_Combo_Box.item_checked(4)
            self.Stl_list[4] = self.parent().ui.Save_STL_Combo_Box.item_checked(4)
            self.Display_STL_List[4] = self.parent().ui.STL_Display_Combo_Box.item_checked(4)
            self.Save_Frames_List[5] = self.parent().ui.Save_Frames_Combo_Box.item_checked(5)
            self.Stl_list[5] = self.parent().ui.Save_STL_Combo_Box.item_checked(5)
            self.Display_STL_List[5] = self.parent().ui.STL_Display_Combo_Box.item_checked(5)
            self.Save_Frames_List[6] = self.parent().ui.Save_Frames_Combo_Box.item_checked(6)
            self.Stl_list[6] = self.parent().ui.Save_STL_Combo_Box.item_checked(6)
            self.Display_STL_List[6] = self.parent().ui.STL_Display_Combo_Box.item_checked(6)
            self.Save_Frames_List[7] = self.parent().ui.Save_Frames_Combo_Box.item_checked(7)
            self.Stl_list[7] = self.parent().ui.Save_STL_Combo_Box.item_checked(7)
            self.Display_STL_List[7] = self.parent().ui.STL_Display_Combo_Box.item_checked(7)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 9 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_6.value()] = self.parent().ui.Object_List_LineEdited_6.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_7.value()] = self.parent().ui.Object_List_LineEdited_7.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_8.value()] = self.parent().ui.Object_List_LineEdited_8.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
            self.Save_Frames_List[1] = self.parent().ui.Save_Frames_Combo_Box.item_checked(1)
            self.Stl_list[1] = self.parent().ui.Save_STL_Combo_Box.item_checked(1)
            self.Display_STL_List[1] = self.parent().ui.STL_Display_Combo_Box.item_checked(1)
            self.Save_Frames_List[2] = self.parent().ui.Save_Frames_Combo_Box.item_checked(2)
            self.Stl_list[2] = self.parent().ui.Save_STL_Combo_Box.item_checked(2)
            self.Display_STL_List[2] = self.parent().ui.STL_Display_Combo_Box.item_checked(2)
            self.Save_Frames_List[3] = self.parent().ui.Save_Frames_Combo_Box.item_checked(3)
            self.Stl_list[3] = self.parent().ui.Save_STL_Combo_Box.item_checked(3)
            self.Display_STL_List[3] = self.parent().ui.STL_Display_Combo_Box.item_checked(3)
            self.Save_Frames_List[4] = self.parent().ui.Save_Frames_Combo_Box.item_checked(4)
            self.Stl_list[4] = self.parent().ui.Save_STL_Combo_Box.item_checked(4)
            self.Display_STL_List[4] = self.parent().ui.STL_Display_Combo_Box.item_checked(4)
            self.Save_Frames_List[5] = self.parent().ui.Save_Frames_Combo_Box.item_checked(5)
            self.Stl_list[5] = self.parent().ui.Save_STL_Combo_Box.item_checked(5)
            self.Display_STL_List[5] = self.parent().ui.STL_Display_Combo_Box.item_checked(5)
            self.Save_Frames_List[6] = self.parent().ui.Save_Frames_Combo_Box.item_checked(6)
            self.Stl_list[6] = self.parent().ui.Save_STL_Combo_Box.item_checked(6)
            self.Display_STL_List[6] = self.parent().ui.STL_Display_Combo_Box.item_checked(6)
            self.Save_Frames_List[7] = self.parent().ui.Save_Frames_Combo_Box.item_checked(7)
            self.Stl_list[7] = self.parent().ui.Save_STL_Combo_Box.item_checked(7)
            self.Display_STL_List[7] = self.parent().ui.STL_Display_Combo_Box.item_checked(7)
            self.Save_Frames_List[8] = self.parent().ui.Save_Frames_Combo_Box.item_checked(8)
            self.Stl_list[8] = self.parent().ui.Save_STL_Combo_Box.item_checked(8)
            self.Display_STL_List[8] = self.parent().ui.STL_Display_Combo_Box.item_checked(8)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 10 :
            self.object_list[self.parent().ui.Object_List_SpinBox_0.value()] = self.parent().ui.Object_List_LineEdited_0.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_1.value()] = self.parent().ui.Object_List_LineEdited_1.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_2.value()] = self.parent().ui.Object_List_LineEdited_2.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_3.value()] = self.parent().ui.Object_List_LineEdited_3.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_4.value()] = self.parent().ui.Object_List_LineEdited_4.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_5.value()] = self.parent().ui.Object_List_LineEdited_5.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_6.value()] = self.parent().ui.Object_List_LineEdited_6.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_7.value()] = self.parent().ui.Object_List_LineEdited_7.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_8.value()] = self.parent().ui.Object_List_LineEdited_8.text()
            self.object_list[self.parent().ui.Object_List_SpinBox_9.value()] = self.parent().ui.Object_List_LineEdited_9.text()
            self.Save_Frames_List[0] = self.parent().ui.Save_Frames_Combo_Box.item_checked(0)
            self.Stl_list[0] = self.parent().ui.Save_STL_Combo_Box.item_checked(0)
            self.Display_STL_List[0] = self.parent().ui.STL_Display_Combo_Box.item_checked(0)
            self.Save_Frames_List[1] = self.parent().ui.Save_Frames_Combo_Box.item_checked(1)
            self.Stl_list[1] = self.parent().ui.Save_STL_Combo_Box.item_checked(1)
            self.Display_STL_List[1] = self.parent().ui.STL_Display_Combo_Box.item_checked(1)
            self.Save_Frames_List[2] = self.parent().ui.Save_Frames_Combo_Box.item_checked(2)
            self.Stl_list[2] = self.parent().ui.Save_STL_Combo_Box.item_checked(2)
            self.Display_STL_List[2] = self.parent().ui.STL_Display_Combo_Box.item_checked(2)
            self.Save_Frames_List[3] = self.parent().ui.Save_Frames_Combo_Box.item_checked(3)
            self.Stl_list[3] = self.parent().ui.Save_STL_Combo_Box.item_checked(3)
            self.Display_STL_List[3] = self.parent().ui.STL_Display_Combo_Box.item_checked(3)
            self.Save_Frames_List[4] = self.parent().ui.Save_Frames_Combo_Box.item_checked(4)
            self.Stl_list[4] = self.parent().ui.Save_STL_Combo_Box.item_checked(4)
            self.Display_STL_List[4] = self.parent().ui.STL_Display_Combo_Box.item_checked(4)
            self.Save_Frames_List[5] = self.parent().ui.Save_Frames_Combo_Box.item_checked(5)
            self.Stl_list[5] = self.parent().ui.Save_STL_Combo_Box.item_checked(5)
            self.Display_STL_List[5] = self.parent().ui.STL_Display_Combo_Box.item_checked(5)
            self.Save_Frames_List[6] = self.parent().ui.Save_Frames_Combo_Box.item_checked(6)
            self.Stl_list[6] = self.parent().ui.Save_STL_Combo_Box.item_checked(6)
            self.Display_STL_List[6] = self.parent().ui.STL_Display_Combo_Box.item_checked(6)
            self.Save_Frames_List[7] = self.parent().ui.Save_Frames_Combo_Box.item_checked(7)
            self.Stl_list[7] = self.parent().ui.Save_STL_Combo_Box.item_checked(7)
            self.Display_STL_List[7] = self.parent().ui.STL_Display_Combo_Box.item_checked(7)
            self.Save_Frames_List[8] = self.parent().ui.Save_Frames_Combo_Box.item_checked(8)
            self.Stl_list[8] = self.parent().ui.Save_STL_Combo_Box.item_checked(8)
            self.Display_STL_List[8] = self.parent().ui.STL_Display_Combo_Box.item_checked(8)
            self.Save_Frames_List[9] = self.parent().ui.Save_Frames_Combo_Box.item_checked(9)
            self.Stl_list[9] = self.parent().ui.Save_STL_Combo_Box.item_checked(9)
            self.Display_STL_List[9] = self.parent().ui.STL_Display_Combo_Box.item_checked(9)

        for i in range(len(self.Display_STL_List)):
            if self.Display_STL_List[i] is True and self.Stl_list[i] is False :
                self.Display_STL_List[i] is False

        self.vx = float(self.parent().ui.X_Size_Text_Box.text())
        self.vy = float(self.parent().ui.Y_Size_Text_Box.text())
        self.vz = float(self.parent().ui.Z_Size_Text_Box.text())
        self.pixel_volume = self.vx*self.vy*self.vz

        self.Borders_Off = self.parent().ui.Borders_Off_Check_Box.isChecked()
        self.Opening = self.parent().ui.Opening_Check_Box.isChecked()
        self.Opening_methode = self.parent().ui.Opening_Methode_Combo_Box.currentText()
        self.Opening_size = (int(self.parent().ui.Opening_Size_X_Text_Box.text()),int(self.parent().ui.Opening_Size_Y_Text_Box.text()))
        self.Connectivity_3D = self.parent().ui.Connectivity_Check_Box.isChecked()
        self.Frame_saved = self.parent().ui.Save_Frames_Check_Box.isChecked()
        self.format_save = self.parent().ui.Save_Frame_Methode_Combo_Box.currentText()
        self.Frame_section = self.parent().ui.Frame_section_Check_Box.isChecked()

        self.Stl_maker = self.parent().ui.Save_STL_Check_Box.isChecked()
        self.Decimation_process = self.parent().ui.Decimation_Process_Check_Box.isChecked()
        self.Reduction_coeff = float(self.parent().ui.Reduction_Coefficient_Text_Box.text())
        self.Save_decimation = self.parent().ui.STL_Save_Combo_Box.item_checked(0)
        self.Save_filling = self.parent().ui.STL_Save_Combo_Box.item_checked(1)
        self.Save_connectivity = self.parent().ui.STL_Save_Combo_Box.item_checked(2)
        self.Preserve_topology = self.parent().ui.Preserve_Topology_Check_Box.isChecked()
        self.Mass_centerDf = self.parent().ui.Mass_Center_Check_Box.isChecked()
        self.Print_Mass_center = self.parent().ui.Show_Mass_Center_Check_Box.isChecked()
        self.Stl_display = self.parent().ui.Display_STL_Check_Box.isChecked()
        self.sphere_size = float(self.parent().ui.Sphere_Size_Text_Box.text())

        self.Manual_segmentation = self.parent().ui.Manual_checkBox.isChecked()
        if self.Manual_segmentation is True :

            if self.parent().ObjectList.Object_List_Create_Counter_i == 1 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())]]

            elif self.parent().ObjectList.Object_List_Create_Counter_i == 2 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_1.text()),int(self.parent().ui.Manual_obj_max_LineEdited_1.text())]]

            elif self.parent().ObjectList.Object_List_Create_Counter_i == 3 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_1.text()),int(self.parent().ui.Manual_obj_max_LineEdited_1.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_2.text()),int(self.parent().ui.Manual_obj_max_LineEdited_2.text())]]
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 4 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_1.text()),int(self.parent().ui.Manual_obj_max_LineEdited_1.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_2.text()),int(self.parent().ui.Manual_obj_max_LineEdited_2.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_3.text()),int(self.parent().ui.Manual_obj_max_LineEdited_3.text())]]
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 5 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_1.text()),int(self.parent().ui.Manual_obj_max_LineEdited_1.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_2.text()),int(self.parent().ui.Manual_obj_max_LineEdited_2.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_3.text()),int(self.parent().ui.Manual_obj_max_LineEdited_3.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_4.text()),int(self.parent().ui.Manual_obj_max_LineEdited_4.text())]]
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 6 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_1.text()),int(self.parent().ui.Manual_obj_max_LineEdited_1.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_2.text()),int(self.parent().ui.Manual_obj_max_LineEdited_2.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_3.text()),int(self.parent().ui.Manual_obj_max_LineEdited_3.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_4.text()),int(self.parent().ui.Manual_obj_max_LineEdited_4.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_5.text()),int(self.parent().ui.Manual_obj_max_LineEdited_5.text())]]
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 7 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_1.text()),int(self.parent().ui.Manual_obj_max_LineEdited_1.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_2.text()),int(self.parent().ui.Manual_obj_max_LineEdited_2.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_3.text()),int(self.parent().ui.Manual_obj_max_LineEdited_3.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_4.text()),int(self.parent().ui.Manual_obj_max_LineEdited_4.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_5.text()),int(self.parent().ui.Manual_obj_max_LineEdited_5.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_6.text()),int(self.parent().ui.Manual_obj_max_LineEdited_6.text())]]
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 8 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_1.text()),int(self.parent().ui.Manual_obj_max_LineEdited_1.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_2.text()),int(self.parent().ui.Manual_obj_max_LineEdited_2.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_3.text()),int(self.parent().ui.Manual_obj_max_LineEdited_3.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_4.text()),int(self.parent().ui.Manual_obj_max_LineEdited_4.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_5.text()),int(self.parent().ui.Manual_obj_max_LineEdited_5.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_6.text()),int(self.parent().ui.Manual_obj_max_LineEdited_6.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_7.text()),int(self.parent().ui.Manual_obj_max_LineEdited_7.text())]]
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 9 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_1.text()),int(self.parent().ui.Manual_obj_max_LineEdited_1.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_2.text()),int(self.parent().ui.Manual_obj_max_LineEdited_2.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_3.text()),int(self.parent().ui.Manual_obj_max_LineEdited_3.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_4.text()),int(self.parent().ui.Manual_obj_max_LineEdited_4.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_5.text()),int(self.parent().ui.Manual_obj_max_LineEdited_5.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_6.text()),int(self.parent().ui.Manual_obj_max_LineEdited_6.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_7.text()),int(self.parent().ui.Manual_obj_max_LineEdited_7.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_8.text()),int(self.parent().ui.Manual_obj_max_LineEdited_8.text())]]
            elif self.parent().ObjectList.Object_List_Create_Counter_i == 10 :
                self.Manual_segmentation_List = [[int(self.parent().ui.Manual_obj_min_LineEdited_0.text()),int(self.parent().ui.Manual_obj_max_LineEdited_0.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_1.text()),int(self.parent().ui.Manual_obj_max_LineEdited_1.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_2.text()),int(self.parent().ui.Manual_obj_max_LineEdited_2.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_3.text()),int(self.parent().ui.Manual_obj_max_LineEdited_3.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_4.text()),int(self.parent().ui.Manual_obj_max_LineEdited_4.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_5.text()),int(self.parent().ui.Manual_obj_max_LineEdited_5.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_6.text()),int(self.parent().ui.Manual_obj_max_LineEdited_6.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_7.text()),int(self.parent().ui.Manual_obj_max_LineEdited_7.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_8.text()),int(self.parent().ui.Manual_obj_max_LineEdited_8.text())],
                                                [int(self.parent().ui.Manual_obj_min_LineEdited_9.text()),int(self.parent().ui.Manual_obj_max_LineEdited_9.text())]]

    def run(self):
        self.lock()
        # self.settings()
        if self.Manual_segmentation is False :
            self.dossier = "Segmentation"+'_'+self.methode1+'_'+self.methode2
        else :
            self.dossier = "Manual_Segmentation"

        if os.path.exists(os.path.join(self.Output_dir, self.dossier)):
            self.unlock()
            return print("File '% s' already exists, can not proceed to segmentation" % self.dossier)

        self.create_out_dirs()

        self.all_frames = self.SumAllFrame()
        if self.Borders_Off is True :
            self.all_borders_off = self.borders_off()
            self.studied_frames = self.all_borders_off
        if self.Manual_segmentation is False :
            self.segmentation()
        else :
            self.segmentation_manual()
        if self.Connectivity_3D is True :
            self.Connectivity3D()
            self.all_frames_segmented = self.studied_connectivity
            del self.studied_connectivity
            gc.collect()
        if self.Opening is True :
            self.opening()
            self.all_frames_segmented = self.studied_opening
            del self.studied_opening
            gc.collect()
        if self.Frame_saved is True :
            self.save_images()
        self.volume()
        if self.Frame_section is True :
            self.frame_section()
        if self.Stl_maker is True :
            for i in range(len(self.object_list)):
                if self.Stl_list[i] is True :
                    self.array_to_stl(i)
                else :
                    self.a = self.a + len(self.impathes)*4
        del self.all_frames_segmented
        gc.collect()
        if self.Decimation_process is True :
            for i in range(len(self.object_list)):
                if self.Stl_list[i] is True :
                    self.stl_process(i)
            self.Decimation_process_progressChanged.emit(100)
        if self.Mass_centerDf is True :
            self.Mass_centerDf_progressChanged.emit(1)
            self.filename_list = []
            self.center_list = []
            for i in range(len(self.object_list)):
                if self.Stl_list[i] is True :
                    G, self.filename = self.Mass_center(i)
                    self.filename_list.append(self.filename)
                    self.center_list.append(G)
            self.Display_mass_center()
            self.Mass_centerDf_progressChanged.emit(100)
        if self.Stl_display is True :
            self.filename_list = []
            self.center_list = []
            for i in range(len(self.object_list)):
                if self.Stl_list[i] is True and self.Display_STL_List[i] is True:
                    self.filename_list.append(self.object_list[i])
                    G = self.Mass_center(i)[0]
                    self.center_list.append(G)
            self.display_stl()
        self.unlock()

    def create_out_dirs(self):

        if not os.path.exists(os.path.join(self.Output_dir, self.dossier)):
            os.mkdir(os.path.join(self.Output_dir, self.dossier))
            self.simple_dir = os.path.join(self.Output_dir, self.dossier)

            for i in range(len(self.object_list)):
                if self.Save_Frames_List[i] is False:
                    continue
                os.mkdir(os.path.join(self.simple_dir, self.object_list[i]))
            print("Folder '% s' has been created in '%s'" % (self.dossier, self.Output_dir))


        else:
            print("Folder(s) already created {0}".format(os.path.join(self.Output_dir, self.dossier)))

    @staticmethod
    @jit(nopython=True)
    def ChangePixelValue(source, coordonates, value):
        image = source.copy()
        for i in range(len(coordonates)):
            image[coordonates[i][0],coordonates[i][1]] = value
        return image

    @staticmethod
    @jit(nopython=True)
    def Wrapper(coordonates, value_r, value_g, value_b,image_rgb):
        for i in range(len(coordonates)):
            image_rgb[:,:,:3][coordonates[i][0],coordonates[i][1]] = [value_r, value_g, value_b]
        image_rgb = (255.0 / (2**16-1) * (image_rgb - image_rgb.min())).astype(np.uint8)
        return image_rgb


    def ChangePixelValueGrayToRGB(self, source, coordonates, value_r, value_g, value_b ):
        frame = source.copy()
        image_rgb = np.dstack([frame, frame, frame]).reshape(frame.shape[0], frame.shape[1],3)
        image_rgb = self.Wrapper(coordonates, value_r, value_g, value_b,image_rgb)
        return image_rgb


    def ImageGrayToRGB(self, source):
        frame = source.copy()
        image_rgb = np.dstack([frame, frame, frame]).reshape(frame.shape[0],frame.shape[1],3)
        if image_rgb.max() == 0:
            image_rgb = (image_rgb).astype(np.uint8)
        else :
            image_rgb = (255.0 / image_rgb.max() * (image_rgb - image_rgb.min())).astype(np.uint8)
        return image_rgb

    @staticmethod
    @jit(nopython=True)
    def GetPixelValue(source, value):
        position = np.where(source==value)
        zipped = np.column_stack((position))
        return zipped

    @staticmethod
    @jit(nopython=True)
    def GetPixelValueBetween(source, infvalue, suppvalue):
        position = np.where(np.logical_and(infvalue<=source, source<=suppvalue))
        zipped = np.column_stack((position))
        return zipped

    @staticmethod
    @jit(nopython=True)
    def GetPixelValueOutside(source, infvalue, suppvalue):
        position = np.where(np.logical_or(infvalue>source, source>suppvalue))
        zipped = np.column_stack((position))
        return zipped


    def GetPixelValueBetweenSeveral(self, source, limits):
        l_0 = None
        for limit in limits :
            l_1 = self.GetPixelValueBetween(source, limit[0], limit[1])
            if l_0 is None:
                l_0 = l_1
                continue
            else:
                l_0 = np.concatenate([l_0, l_1], axis=0)
        return l_0



    def limits(self, limnew, q):
        if q<len(self.object_list)-1 and q>0 and (limnew[q][1]>limnew[q+1][0] and limnew[q][0]<limnew[q-1][1]) :
            limin_inf = limnew[q-1][1]
            limin_supp = limnew[q+1][0]
            between = [[limnew[q][0], limnew[q-1][1]],[limnew[q+1][0], limnew[q][1]]]
            cond = 2
            return limin_inf, limin_supp, limin_inf, limin_supp, cond, between
        elif q<len(self.object_list)-1 and (limnew[q][1]>limnew[q+1][0]) :
            limin_inf = limnew[q][0]
            limin_supp = limnew[q+1][0]
            between_inf = limnew[q+1][0]
            between_supp = limnew[q][1]
            cond = 1
            return limin_inf, limin_supp, between_inf, between_supp, cond, None
        elif q>0 and (limnew[q][0]<limnew[q-1][1]) :
            limin_inf = limnew[q-1][1]
            limin_supp = limnew[q][1]
            between_inf = limnew[q][0]
            between_supp = limnew[q-1][1]
            cond = 1
            return limin_inf, limin_supp, between_inf, between_supp, cond, None
        else :
            limin_inf = limnew[q][0]
            limin_supp = limnew[q][1]
            cond = 0
            return limin_inf, limin_supp, limin_inf, limin_supp, cond, None


    def rescaler(self, value):
        dist = value.max() - value.min()
        out_value = value / (dist) - (value.min() / dist)
        return out_value

    @staticmethod
    @jit(nopython=True)
    def min_val_above (array):
        min_val = 2**16
        for i in range(len(array)) :
            if array[i] > 0 and min_val > array[i] :
                min_val = array[i]
        return min_val

    def Max_occur(self, array, number=None) :
        if not number is None :
            a_list = array[array != number]
            if len(a_list) == 0 :
                b = 0
            else :
                b = np.bincount(a_list).argmax()
        else :
            b = np.bincount(array).argmax()
        return b

    def SumAllFrame(self):
        all_frames = []

        for i in tqdm(range(len(self.impathes)), desc='Count of all frames pixels'):
            frame = np.asarray(Image.open(self.Input_dir+'/'+self.impathes[i]))
            all_frames.append(frame)
            self.Sum_All_Frames_progressChanged.emit(int((i/len(self.impathes))*100))
        self.Sum_All_Frames_progressChanged.emit(100)
        return np.array(all_frames)

    def SegGlobaleHist(self):
        from scipy import interpolate
        m = np.bincount(self.studied_frames.flatten(), minlength = 2**16)
        minV = self.min_val_above (self.studied_frames.flatten())
        maxV = self.studied_frames.flatten().max()

        interV = (maxV - minV)
        yp = m[0:2**16]
        xp = list(range(0,2**16))
        f = interpolate.interp1d(xp, ndimage.gaussian_filter(yp, 150), kind='cubic')

        Xi = np.linspace(minV, maxV, interV)
        Yi = f(Xi)
        h= int(interV*0.01)
        dYi = (f(Xi+h)-f(Xi-h))/(2*h)
        ddYi = np.gradient(ndimage.gaussian_filter(dYi, 100))

        # for local maxima
        j = argrelextrema(Yi, np.greater, order = 100)
        j = np.array(j).flatten()
        j1 =[]
        for i in range(len(j)):
            if Yi[j[i]] > 100 :
                j1.append(int(j[i]))
        j = j1

        # for local minima
        g = argrelextrema(Yi, np.less, order = 100)
        g = np.array(g).flatten()
        g1 = []
        for i in range(len(g)):
            if Yi[g[i]] > 5 :
                g1.append(int(g[i]))
        g = g1

        # for local mimima on ddYi
        b = argrelextrema(ddYi, np.less, order = 100)
        b = np.array(b).flatten()
        b1 = []
        for i in range(len(b)):
            if Yi[b[i]] > 100 :
                b1.append(int(b[i]))
        b = b1

        j2 = []
        for i in range(len(j)):
            for l in range(len(b)):
                if abs(Xi[j[i]]-Xi[b[l]])<300:
                    j2.append(j[i])
                    break
        j=j2

        while len(j)>len(self.object_list):
            index = list(Yi[j]).index(Yi[j].min())
            j = np.delete(j, index)

        lim1 = []
        lim = []
        limnew = []

        for i in range(len(self.object_list)):

            ainf = np.array(g)[np.array(g) < j[i]].max()
            asupp = np.array(g)[np.array(g) > j[i]].min()
            lim.append([int(Xi[ainf]), int(Xi[asupp])])
            lim1.append([ainf, asupp])

            pente1 = (Yi[j[i]] - Yi[lim1[i][0]]) / (Xi[j[i]] - Xi[lim1[i][0]]) #pente de la droite
            pente2 = (Yi[lim1[i][1]] - Yi[j[i]]) / (Xi[lim1[i][1]] - Xi[j[i]]) #pente de la droite
            b1 = Yi[lim1[i][0]] #ax + 'b'
            b2 = Yi[j[i]] #ax + 'b'
            nx1 = (-b1)/(pente1)+Xi[lim1[i][0]] #nx lorsque y=0
            nx2 = (-b2)/(pente2)+Xi[j[i]] #nx lorsque y=0
            limnew.append([int(nx1), int(nx2)])

        extremum = Xi[j]
        return lim, limnew, extremum

    def SegUniqueHist(self, frame, Extremum_Global):

        from scipy import interpolate
        m = np.bincount(frame.flatten(), minlength = 2**16)
        minV = self.min_val_above (frame.flatten())
        maxV = frame.flatten().max()

        interV = (maxV - minV)
        yp = m[0:2**16]
        xp = list(range(0,2**16))
        f = interpolate.interp1d(xp, ndimage.gaussian_filter(yp, 100), kind='cubic')

        Xi = np.linspace(minV, maxV, interV)
        Yi = f(Xi)
        h= int(interV*0.01)
        dYi = (f(Xi+h)-f(Xi-h))/(2*h)
        ddYi = np.gradient(ndimage.gaussian_filter(dYi, 100))

        # for local maxima
        j = argrelextrema(Yi, np.greater, order = 1000) #order 9 ou 8
        j = np.array(j).flatten()

        # for local minima
        g = argrelextrema(Yi, np.less, order = 800)
        g = np.array(g).flatten()

        Extremum = Xi[j]
        j3 = []
        j1 = []

        c = 0

        for k in range(len(Extremum_Global)):
            c = c+1
            j3 = []
            for i in range(len(j)):
                if Extremum[i]<Extremum_Global[k]+1000 and Extremum[i]>Extremum_Global[k]-1000 :
                    j3.append(j[i])

            while len(j3) !=1 and len(j3) !=0:
                index = list(Yi[j3]).index(Yi[j3].min())
                j3 = np.delete(j3, index)
            if len(j3) == 1:
                j1.append(j3[0])
            if len(j1)<c :
                j1.append(0)
        j = j1

        lim1 = []
        lim = []
        limnew = []

        for i in range(len(self.object_list)):
            if j[i]>0 and np.count_nonzero(np.array(g) < j[i]) != 0 and np.count_nonzero(np.array(g) > j[i]) != 0 :
                ainf = np.array(g)[np.array(g) < j[i]].max()
                asupp = np.array(g)[np.array(g) > j[i]].min()
                lim.append([int(Xi[ainf]), int(Xi[asupp])])
                lim1.append([ainf, asupp])
            elif j[i]>0 and np.count_nonzero(np.array(g) < j[i]) == 0 and np.count_nonzero(np.array(g) > j[i]) != 0 :
                ainf = 0
                asupp = np.array(g)[np.array(g) > j[i]].min()
                lim.append([int(Xi[ainf]), int(Xi[asupp])])
                lim1.append([ainf, asupp])
            elif j[i]>0 and np.count_nonzero(np.array(g) < j[i]) != 0 and np.count_nonzero(np.array(g) > j[i]) == 0 :
                ainf = np.array(g)[np.array(g) < j[i]].max()
                asupp = interV-1
                lim.append([int(Xi[ainf]), int(Xi[asupp])])
                lim1.append([ainf, asupp])
            else :
                lim.append([0, 0])
                lim1.append([0, 0])

        for i in range(len(self.object_list)):
            if j[i]>0 :
                pente1 = (Yi[j[i]] - Yi[lim1[i][0]]) / (Xi[j[i]] - Xi[lim1[i][0]]) #pente de la droite
                pente2 = (Yi[lim1[i][1]] - Yi[j[i]]) / (Xi[lim1[i][1]] - Xi[j[i]]) #pente de la droite
                b1 = Yi[lim1[i][0]] #ax + 'b'
                b2 = Yi[j[i]] #ax + 'b'
                nx1 = (-b1)/(pente1)+Xi[lim1[i][0]] #nx lorsque y=0
                nx2 = (-b2)/(pente2)+Xi[j[i]] #nx lorsque y=0
                limnew.append([int(nx1), int(nx2)])
            else :
                limnew.append([0, 0])


        return lim, limnew


    def segmentation_manual(self):
        self.all_frames_segmented = []
        a = 0
        if self.Borders_Off == True :
            self.studied_frames = self.all_borders_off
            del self.all_borders_off
            gc.collect()
        else :
            self.studied_frames = self.all_frames

        for q in range(len(self.object_list)) :
            limmin = self.Manual_segmentation_List[q][0]
            limmax = self.Manual_segmentation_List[q][1]
            object_list_segmented = []
            for i in tqdm(range(len(self.impathes)), desc='Segmentation of %s' % self.object_list[q]):
                a = a + 1
                frame = self.studied_frames[i]
                coordonates_in = self.GetPixelValueBetween(frame, limmin, limmax)
                coordonates_out = self.GetPixelValueOutside(frame, limmin, limmax)
                frame1 = self.ChangePixelValue(frame, coordonates_in, 2**16-1)
                frame2 = self.ChangePixelValue(frame1, coordonates_out, 0)
                object_list_segmented.append(frame2)

                self.Segmentation_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))))
            self.all_frames_segmented.append(object_list_segmented)
        self.Segmentation_progressChanged.emit(100)


    def segmentation(self):
        self.all_frames_segmented = []
        a = 0
        if self.Borders_Off == True :
            self.studied_frames = self.all_borders_off
            del self.all_borders_off
            gc.collect()
        else :
            self.studied_frames = self.all_frames

        if self.methode2 == 'Global' :

            if self.methode1 == 'Without overlap' :
                lim = self.SegGlobaleHist()[0]

                for q in range(len(self.object_list)) :
                    limmin = lim[q][0]
                    limmax = lim[q][1]
                    object_list_segmented = []
                    for i in tqdm(range(len(self.impathes)), desc='Segmentation of %s' % self.object_list[q]):
                        a = a + 1
                        frame = self.studied_frames[i]
                        coordonates_in = self.GetPixelValueBetween(frame, limmin, limmax)
                        coordonates_out = self.GetPixelValueOutside(frame, limmin, limmax)
                        frame1 = self.ChangePixelValue(frame, coordonates_in, 2**16-1)
                        frame2 = self.ChangePixelValue(frame1, coordonates_out, 0)
                        object_list_segmented.append(frame2)

                        self.Segmentation_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))))
                    self.all_frames_segmented.append(object_list_segmented)
                self.Segmentation_progressChanged.emit(100)
            elif self.methode1 == 'With overlap' :
                limnew = self.SegGlobaleHist()[1]

                for q in range(len(self.object_list)) :
                    limin_inf, limin_supp, between_inf, between_supp, cond, between = self.limits(limnew, q)
                    object_list_segmented = []
                    for i in tqdm(range(len(self.impathes)), desc='Segmentation of %s' % self.object_list[q]):
                        a = a + 1
                        frame = self.studied_frames[i]
                        coordonates_out = self.GetPixelValueOutside(frame, limnew[q][0], limnew[q][1])
                        frame1 = self.ChangePixelValue(frame, coordonates_out, 0)
                        coordonates_in = self.GetPixelValueBetween(frame1, limin_inf, limin_supp)
                        frame2 = self.ChangePixelValue(frame1, coordonates_in, 2**16-1)
                        if cond==0 :
                            frame_rgb = self.ImageGrayToRGB(frame2)
                        if cond==1 :
                            coordonates_between = self.GetPixelValueBetween(frame2, between_inf, between_supp)
                            frame_rgb = self.ChangePixelValueGrayToRGB(frame2, coordonates_between, 2**16-1,0,0)
                        if cond==2 :
                            coordonates_between = self.GetPixelValueBetweenSeveral(frame2, between)
                            frame_rgb = self.ChangePixelValueGrayToRGB(frame2, coordonates_between, 2**16-1,0,0)
                        object_list_segmented.append(frame_rgb)
                        self.Segmentation_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))))
                    self.all_frames_segmented.append(object_list_segmented)
                self.Segmentation_progressChanged.emit(100)
            else :
                print("Wrong information input")

        elif self.methode2 == 'Image per image' :
            Extremum_Global = self.SegGlobaleHist()[2]

            if self.methode1 == 'Without overlap' :

                for q in range(len(self.object_list)) :
                    object_list_segmented = []
                    for i in tqdm(range(len(self.impathes)), desc='Segmentation of %s' % self.object_list[q]):
                        a = a + 1
                        frame = self.studied_frames[i]
                        lim = self.SegUniqueHist(frame, Extremum_Global)[0]
                        limmin = lim[q][0]
                        limmax = lim[q][1]
                        coordonates_in = self.GetPixelValueBetween(frame, limmin, limmax)
                        coordonates_out = self.GetPixelValueOutside(frame, limmin, limmax)
                        frame1 = self.ChangePixelValue(frame, coordonates_in, 2**16-1)
                        frame2 = self.ChangePixelValue(frame1, coordonates_out, 0)
                        object_list_segmented.append(frame2)
                        self.Segmentation_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))))
                    self.all_frames_segmented.append(object_list_segmented)
                self.Segmentation_progressChanged.emit(100)
            elif self.methode1 == 'With overlap' :

                for q in range(len(self.object_list)) :
                    object_list_segmented = []
                    for i in tqdm(range(len(self.impathes)), desc='Segmentation of %s' % self.object_list[q]):
                        a = a + 1
                        frame = self.studied_frames[i]
                        limnew = self.SegUniqueHist(frame, Extremum_Global)[1]
                        limin_inf, limin_supp, between_inf, between_supp, cond, between = self.limits(limnew, q)

                        coordonates_out = self.GetPixelValueOutside(frame, limnew[q][0], limnew[q][1])
                        frame1 = self.ChangePixelValue(frame, coordonates_out, 0)
                        coordonates_in = self.GetPixelValueBetween(frame1, limin_inf, limin_supp)
                        frame2 = self.ChangePixelValue(frame1, coordonates_in, 2**16-1)
                        if cond==0 :
                            frame_rgb = self.ImageGrayToRGB(frame2)
                        if cond==1 :
                            coordonates_between = self.GetPixelValueBetween(frame2, between_inf, between_supp)
                            frame_rgb = self.ChangePixelValueGrayToRGB(frame2, coordonates_between, 2**16-1,0,0)
                        if cond==2 :
                            coordonates_between = self.GetPixelValueBetweenSeveral(frame2, between)
                            frame_rgb = self.ChangePixelValueGrayToRGB(frame2, coordonates_between, 2**16-1,0,0)
                        object_list_segmented.append(frame_rgb)
                        self.Segmentation_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))))

                    self.all_frames_segmented.append(object_list_segmented)
                self.Segmentation_progressChanged.emit(100)
            else :
                print("Wrong information input")
        else :
                print("Wrong information input")

        return

    def volume(self):

        for z in range(len(self.object_list)) :
            k = 0
            for i in range(len(self.impathes)):
                frame = self.all_frames_segmented[z][i]
                frame1 = frame.astype(np.uint8)
                frame = frame.flatten().astype(np.uint8)
                b =  np.count_nonzero(frame == 255)
                k = np.add(k, b)

            if len(frame)> frame1.shape[0]*frame1.shape[1] :
                k = k/3
            volume = (k.sum())*self.pixel_volume
            print('Size of ' + self.object_list[z] + ' = ' + str(volume) + 'mm^3')

    def frame_section(self):
        self.section_array = []
        self.sorted_section = []
        self.sorted_index = []
        a = 0
        for z in range(len(self.object_list)) :
            self.new_section_array = []
            for i in range(len(self.impathes)):
                a = a + i
                frame = self.all_frames_segmented[z][i]
                frame1 = frame.astype(np.uint8)
                frame = frame.flatten().astype(np.uint8)
                b =  np.count_nonzero(frame == 255)
                if len(frame)> frame1.shape[0]*frame1.shape[1] :
                    b = b/3
                section = b*self.vx*self.vy
                self.new_section_array.append(section)
                self.Frame_section_progressChanged.emit(int(((a/len(self.impathes))/len(self.object_list))*100))
            self.new_sorted_section = sorted(self.new_section_array)
            self.new_sorted_index = sorted(range(len(self.new_section_array)), key=lambda k: self.new_section_array[k])
            self.section_array.append(self.new_section_array)
            self.sorted_section.append(self.new_sorted_section)
            self.sorted_index.append(self.new_sorted_index)

        list_impathes = list(range(len(self.impathes)))
        if len(self.object_list) == 1 :
            section_df = {'Frame n°': list_impathes, 'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0]}
            df = pd.DataFrame(section_df, columns = ['Frame n°', 'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0]])
        elif len(self.object_list) == 2 :
            section_df = {'Frame n°': list_impathes,'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0],
                          'Section of '+self.object_list[1]+' in mm²': self.section_array[1], 'Sorted section of '+self.object_list[1] : self.sorted_section[1], 'Sorted section index of '+self.object_list[1] : self.sorted_index[1]}
            df = pd.DataFrame(section_df, columns = ['Frame n°', 'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0],
                                                     'Section of '+self.object_list[1]+' in mm²', 'Sorted section of '+self.object_list[1], 'Sorted section index of '+self.object_list[1]])
        elif len(self.object_list) == 3 :
            section_df = {'Frame n°': list_impathes,'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0],
                          'Section of '+self.object_list[1]+' in mm²': self.section_array[1], 'Sorted section of '+self.object_list[1] : self.sorted_section[1], 'Sorted section index of '+self.object_list[1] : self.sorted_index[1],
                          'Section of '+self.object_list[2]+' in mm²': self.section_array[2], 'Sorted section of '+self.object_list[2] : self.sorted_section[2], 'Sorted section index of '+self.object_list[2] : self.sorted_index[2]}
            df = pd.DataFrame(section_df, columns = ['Frame n°',  'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0],
                                                     'Section of '+self.object_list[1]+' in mm²', 'Sorted section of '+self.object_list[1], 'Sorted section index of '+self.object_list[1],
                                                     'Section of '+self.object_list[2]+' in mm²', 'Sorted section of '+self.object_list[2], 'Sorted section index of '+self.object_list[2]])
        elif len(self.object_list) == 4 :
            section_df = {'Frame n°': list_impathes,'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0],
                          'Section of '+self.object_list[1]+' in mm²': self.section_array[1], 'Sorted section of '+self.object_list[1] : self.sorted_section[1], 'Sorted section index of '+self.object_list[1] : self.sorted_index[1],
                          'Section of '+self.object_list[2]+' in mm²': self.section_array[2], 'Sorted section of '+self.object_list[2] : self.sorted_section[2], 'Sorted section index of '+self.object_list[2] : self.sorted_index[2],
                          'Section of '+self.object_list[3]+' in mm²': self.section_array[3], 'Sorted section of '+self.object_list[3] : self.sorted_section[3], 'Sorted section index of '+self.object_list[3] : self.sorted_index[3]}
            df = pd.DataFrame(section_df, columns = ['Frame n°',  'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0],
                                                     'Section of '+self.object_list[1]+' in mm²', 'Sorted section of '+self.object_list[1], 'Sorted section index of '+self.object_list[1],
                                                     'Section of '+self.object_list[2]+' in mm²', 'Sorted section of '+self.object_list[2], 'Sorted section index of '+self.object_list[2],
                                                     'Section of '+self.object_list[3]+' in mm²', 'Sorted section of '+self.object_list[3], 'Sorted section index of '+self.object_list[3]])
        elif len(self.object_list) == 5 :
            section_df = {'Frame n°': list_impathes,'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0],
                          'Section of '+self.object_list[1]+' in mm²': self.section_array[1], 'Sorted section of '+self.object_list[1] : self.sorted_section[1], 'Sorted section index of '+self.object_list[1] : self.sorted_index[1],
                          'Section of '+self.object_list[2]+' in mm²': self.section_array[2], 'Sorted section of '+self.object_list[2] : self.sorted_section[2], 'Sorted section index of '+self.object_list[2] : self.sorted_index[2],
                          'Section of '+self.object_list[3]+' in mm²': self.section_array[3], 'Sorted section of '+self.object_list[3] : self.sorted_section[3], 'Sorted section index of '+self.object_list[3] : self.sorted_index[3],
                          'Section of '+self.object_list[4]+' in mm²': self.section_array[4], 'Sorted section of '+self.object_list[4] : self.sorted_section[4], 'Sorted section index of '+self.object_list[4] : self.sorted_index[4]}
            df = pd.DataFrame(section_df, columns = ['Frame n°',  'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0],
                                                     'Section of '+self.object_list[1]+' in mm²', 'Sorted section of '+self.object_list[1], 'Sorted section index of '+self.object_list[1],
                                                     'Section of '+self.object_list[2]+' in mm²', 'Sorted section of '+self.object_list[2], 'Sorted section index of '+self.object_list[2],
                                                     'Section of '+self.object_list[3]+' in mm²', 'Sorted section of '+self.object_list[3], 'Sorted section index of '+self.object_list[3],
                                                     'Section of '+self.object_list[4]+' in mm²', 'Sorted section of '+self.object_list[4], 'Sorted section index of '+self.object_list[4]])
        elif len(self.object_list) == 6 :
            section_df = {'Frame n°': list_impathes,'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0],
                          'Section of '+self.object_list[1]+' in mm²': self.section_array[1], 'Sorted section of '+self.object_list[1] : self.sorted_section[1], 'Sorted section index of '+self.object_list[1] : self.sorted_index[1],
                          'Section of '+self.object_list[2]+' in mm²': self.section_array[2], 'Sorted section of '+self.object_list[2] : self.sorted_section[2], 'Sorted section index of '+self.object_list[2] : self.sorted_index[2],
                          'Section of '+self.object_list[3]+' in mm²': self.section_array[3], 'Sorted section of '+self.object_list[3] : self.sorted_section[3], 'Sorted section index of '+self.object_list[3] : self.sorted_index[3],
                          'Section of '+self.object_list[4]+' in mm²': self.section_array[4], 'Sorted section of '+self.object_list[4] : self.sorted_section[4], 'Sorted section index of '+self.object_list[4] : self.sorted_index[4],
                          'Section of '+self.object_list[5]+' in mm²': self.section_array[5], 'Sorted section of '+self.object_list[5] : self.sorted_section[5], 'Sorted section index of '+self.object_list[5] : self.sorted_index[5]}
            df = pd.DataFrame(section_df, columns = ['Frame n°',  'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0],
                                                     'Section of '+self.object_list[1]+' in mm²', 'Sorted section of '+self.object_list[1], 'Sorted section index of '+self.object_list[1],
                                                     'Section of '+self.object_list[2]+' in mm²', 'Sorted section of '+self.object_list[2], 'Sorted section index of '+self.object_list[2],
                                                     'Section of '+self.object_list[3]+' in mm²', 'Sorted section of '+self.object_list[3], 'Sorted section index of '+self.object_list[3],
                                                     'Section of '+self.object_list[4]+' in mm²', 'Sorted section of '+self.object_list[4], 'Sorted section index of '+self.object_list[4],
                                                     'Section of '+self.object_list[5]+' in mm²', 'Sorted section of '+self.object_list[5], 'Sorted section index of '+self.object_list[5]])
        elif len(self.object_list) == 7 :
            section_df = {'Frame n°': list_impathes,'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0],
                          'Section of '+self.object_list[1]+' in mm²': self.section_array[1], 'Sorted section of '+self.object_list[1] : self.sorted_section[1], 'Sorted section index of '+self.object_list[1] : self.sorted_index[1],
                          'Section of '+self.object_list[2]+' in mm²': self.section_array[2], 'Sorted section of '+self.object_list[2] : self.sorted_section[2], 'Sorted section index of '+self.object_list[2] : self.sorted_index[2],
                          'Section of '+self.object_list[3]+' in mm²': self.section_array[3], 'Sorted section of '+self.object_list[3] : self.sorted_section[3], 'Sorted section index of '+self.object_list[3] : self.sorted_index[3],
                          'Section of '+self.object_list[4]+' in mm²': self.section_array[4], 'Sorted section of '+self.object_list[4] : self.sorted_section[4], 'Sorted section index of '+self.object_list[4] : self.sorted_index[4],
                          'Section of '+self.object_list[5]+' in mm²': self.section_array[5], 'Sorted section of '+self.object_list[5] : self.sorted_section[5], 'Sorted section index of '+self.object_list[5] : self.sorted_index[5],
                          'Section of '+self.object_list[6]+' in mm²': self.section_array[6], 'Sorted section of '+self.object_list[6] : self.sorted_section[6], 'Sorted section index of '+self.object_list[6] : self.sorted_index[6]}
            df = pd.DataFrame(section_df, columns = ['Frame n°',  'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0],
                                                     'Section of '+self.object_list[1]+' in mm²', 'Sorted section of '+self.object_list[1], 'Sorted section index of '+self.object_list[1],
                                                     'Section of '+self.object_list[2]+' in mm²', 'Sorted section of '+self.object_list[2], 'Sorted section index of '+self.object_list[2],
                                                     'Section of '+self.object_list[3]+' in mm²', 'Sorted section of '+self.object_list[3], 'Sorted section index of '+self.object_list[3],
                                                     'Section of '+self.object_list[4]+' in mm²', 'Sorted section of '+self.object_list[4], 'Sorted section index of '+self.object_list[4],
                                                     'Section of '+self.object_list[5]+' in mm²', 'Sorted section of '+self.object_list[5], 'Sorted section index of '+self.object_list[5],
                                                     'Section of '+self.object_list[6]+' in mm²', 'Sorted section of '+self.object_list[6], 'Sorted section index of '+self.object_list[6]])
        elif len(self.object_list) == 8 :
            section_df = {'Frame n°': list_impathes,'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0],
                          'Section of '+self.object_list[1]+' in mm²': self.section_array[1], 'Sorted section of '+self.object_list[1] : self.sorted_section[1], 'Sorted section index of '+self.object_list[1] : self.sorted_index[1],
                          'Section of '+self.object_list[2]+' in mm²': self.section_array[2], 'Sorted section of '+self.object_list[2] : self.sorted_section[2], 'Sorted section index of '+self.object_list[2] : self.sorted_index[2],
                          'Section of '+self.object_list[3]+' in mm²': self.section_array[3], 'Sorted section of '+self.object_list[3] : self.sorted_section[3], 'Sorted section index of '+self.object_list[3] : self.sorted_index[3],
                          'Section of '+self.object_list[4]+' in mm²': self.section_array[4], 'Sorted section of '+self.object_list[4] : self.sorted_section[4], 'Sorted section index of '+self.object_list[4] : self.sorted_index[4],
                          'Section of '+self.object_list[5]+' in mm²': self.section_array[5], 'Sorted section of '+self.object_list[5] : self.sorted_section[5], 'Sorted section index of '+self.object_list[5] : self.sorted_index[5],
                          'Section of '+self.object_list[6]+' in mm²': self.section_array[6], 'Sorted section of '+self.object_list[6] : self.sorted_section[6], 'Sorted section index of '+self.object_list[6] : self.sorted_index[6],
                          'Section of '+self.object_list[7]+' in mm²': self.section_array[7], 'Sorted section of '+self.object_list[7] : self.sorted_section[7], 'Sorted section index of '+self.object_list[7] : self.sorted_index[7]}
            df = pd.DataFrame(section_df, columns = ['Frame n°',  'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0],
                                                     'Section of '+self.object_list[1]+' in mm²', 'Sorted section of '+self.object_list[1], 'Sorted section index of '+self.object_list[1],
                                                     'Section of '+self.object_list[2]+' in mm²', 'Sorted section of '+self.object_list[2], 'Sorted section index of '+self.object_list[2],
                                                     'Section of '+self.object_list[3]+' in mm²', 'Sorted section of '+self.object_list[3], 'Sorted section index of '+self.object_list[3],
                                                     'Section of '+self.object_list[4]+' in mm²', 'Sorted section of '+self.object_list[4], 'Sorted section index of '+self.object_list[4],
                                                     'Section of '+self.object_list[5]+' in mm²', 'Sorted section of '+self.object_list[5], 'Sorted section index of '+self.object_list[5],
                                                     'Section of '+self.object_list[6]+' in mm²', 'Sorted section of '+self.object_list[6], 'Sorted section index of '+self.object_list[6],
                                                     'Section of '+self.object_list[7]+' in mm²', 'Sorted section of '+self.object_list[7], 'Sorted section index of '+self.object_list[7]])
        elif len(self.object_list) == 9 :
            section_df = {'Frame n°': list_impathes,'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0],
                          'Section of '+self.object_list[1]+' in mm²': self.section_array[1], 'Sorted section of '+self.object_list[1] : self.sorted_section[1], 'Sorted section index of '+self.object_list[1] : self.sorted_index[1],
                          'Section of '+self.object_list[2]+' in mm²': self.section_array[2], 'Sorted section of '+self.object_list[2] : self.sorted_section[2], 'Sorted section index of '+self.object_list[2] : self.sorted_index[2],
                          'Section of '+self.object_list[3]+' in mm²': self.section_array[3], 'Sorted section of '+self.object_list[3] : self.sorted_section[3], 'Sorted section index of '+self.object_list[3] : self.sorted_index[3],
                          'Section of '+self.object_list[4]+' in mm²': self.section_array[4], 'Sorted section of '+self.object_list[4] : self.sorted_section[4], 'Sorted section index of '+self.object_list[4] : self.sorted_index[4],
                          'Section of '+self.object_list[5]+' in mm²': self.section_array[5], 'Sorted section of '+self.object_list[5] : self.sorted_section[5], 'Sorted section index of '+self.object_list[5] : self.sorted_index[5],
                          'Section of '+self.object_list[6]+' in mm²': self.section_array[6], 'Sorted section of '+self.object_list[6] : self.sorted_section[6], 'Sorted section index of '+self.object_list[6] : self.sorted_index[6],
                          'Section of '+self.object_list[7]+' in mm²': self.section_array[7], 'Sorted section of '+self.object_list[7] : self.sorted_section[7], 'Sorted section index of '+self.object_list[7] : self.sorted_index[7],
                          'Section of '+self.object_list[8]+' in mm²': self.section_array[8], 'Sorted section of '+self.object_list[8] : self.sorted_section[8], 'Sorted section index of '+self.object_list[8] : self.sorted_index[8]}
            df = pd.DataFrame(section_df, columns = ['Frame n°',  'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0],
                                                     'Section of '+self.object_list[1]+' in mm²', 'Sorted section of '+self.object_list[1], 'Sorted section index of '+self.object_list[1],
                                                     'Section of '+self.object_list[2]+' in mm²', 'Sorted section of '+self.object_list[2], 'Sorted section index of '+self.object_list[2],
                                                     'Section of '+self.object_list[3]+' in mm²', 'Sorted section of '+self.object_list[3], 'Sorted section index of '+self.object_list[3],
                                                     'Section of '+self.object_list[4]+' in mm²', 'Sorted section of '+self.object_list[4], 'Sorted section index of '+self.object_list[4],
                                                     'Section of '+self.object_list[5]+' in mm²', 'Sorted section of '+self.object_list[5], 'Sorted section index of '+self.object_list[5],
                                                     'Section of '+self.object_list[6]+' in mm²', 'Sorted section of '+self.object_list[6], 'Sorted section index of '+self.object_list[6],
                                                     'Section of '+self.object_list[7]+' in mm²', 'Sorted section of '+self.object_list[7], 'Sorted section index of '+self.object_list[7],
                                                     'Section of '+self.object_list[8]+' in mm²', 'Sorted section of '+self.object_list[8], 'Sorted section index of '+self.object_list[8]])
        elif len(self.object_list) == 10 :
            section_df = {'Frame n°': list_impathes,'Section of '+self.object_list[0]+' in mm²': self.section_array[0], 'Sorted section of '+self.object_list[0] : self.sorted_section[0], 'Sorted section index of '+self.object_list[0] : self.sorted_index[0],
                          'Section of '+self.object_list[1]+' in mm²': self.section_array[1], 'Sorted section of '+self.object_list[1] : self.sorted_section[1], 'Sorted section index of '+self.object_list[1] : self.sorted_index[1],
                          'Section of '+self.object_list[2]+' in mm²': self.section_array[2], 'Sorted section of '+self.object_list[2] : self.sorted_section[2], 'Sorted section index of '+self.object_list[2] : self.sorted_index[2],
                          'Section of '+self.object_list[3]+' in mm²': self.section_array[3], 'Sorted section of '+self.object_list[3] : self.sorted_section[3], 'Sorted section index of '+self.object_list[3] : self.sorted_index[3],
                          'Section of '+self.object_list[4]+' in mm²': self.section_array[4], 'Sorted section of '+self.object_list[4] : self.sorted_section[4], 'Sorted section index of '+self.object_list[4] : self.sorted_index[4],
                          'Section of '+self.object_list[5]+' in mm²': self.section_array[5], 'Sorted section of '+self.object_list[5] : self.sorted_section[5], 'Sorted section index of '+self.object_list[5] : self.sorted_index[5],
                          'Section of '+self.object_list[6]+' in mm²': self.section_array[6], 'Sorted section of '+self.object_list[6] : self.sorted_section[6], 'Sorted section index of '+self.object_list[6] : self.sorted_index[6],
                          'Section of '+self.object_list[7]+' in mm²': self.section_array[7], 'Sorted section of '+self.object_list[7] : self.sorted_section[7], 'Sorted section index of '+self.object_list[7] : self.sorted_index[7],
                          'Section of '+self.object_list[8]+' in mm²': self.section_array[8], 'Sorted section of '+self.object_list[8] : self.sorted_section[8], 'Sorted section index of '+self.object_list[8] : self.sorted_index[8],
                          'Section of '+self.object_list[9]+' in mm²': self.section_array[9], 'Sorted section of '+self.object_list[9] : self.sorted_section[9], 'Sorted section index of '+self.object_list[9] : self.sorted_index[9]}
            df = pd.DataFrame(section_df, columns = ['Frame n°',  'Section of '+self.object_list[0]+' in mm²', 'Sorted section of '+self.object_list[0], 'Sorted section index of '+self.object_list[0],
                                                     'Section of '+self.object_list[1]+' in mm²', 'Sorted section of '+self.object_list[1], 'Sorted section index of '+self.object_list[1],
                                                     'Section of '+self.object_list[2]+' in mm²', 'Sorted section of '+self.object_list[2], 'Sorted section index of '+self.object_list[2],
                                                     'Section of '+self.object_list[3]+' in mm²', 'Sorted section of '+self.object_list[3], 'Sorted section index of '+self.object_list[3],
                                                     'Section of '+self.object_list[4]+' in mm²', 'Sorted section of '+self.object_list[4], 'Sorted section index of '+self.object_list[4],
                                                     'Section of '+self.object_list[5]+' in mm²', 'Sorted section of '+self.object_list[5], 'Sorted section index of '+self.object_list[5],
                                                     'Section of '+self.object_list[6]+' in mm²', 'Sorted section of '+self.object_list[6], 'Sorted section index of '+self.object_list[6],
                                                     'Section of '+self.object_list[7]+' in mm²', 'Sorted section of '+self.object_list[7], 'Sorted section index of '+self.object_list[7],
                                                     'Section of '+self.object_list[8]+' in mm²', 'Sorted section of '+self.object_list[8], 'Sorted section index of '+self.object_list[8],
                                                     'Section of '+self.object_list[9]+' in mm²', 'Sorted section of '+self.object_list[9], 'Sorted section index of '+self.object_list[9]])

        writer = pd.ExcelWriter(self.Output_dir+'/'+self.dossier+'/'+'Section_dataframe.xlsx')
        df.to_excel(writer, sheet_name='Section_analysis', index=False, na_rep='NaN')

        # Auto-adjust columns' width
        for column in df:
            column_width = max(df[column].astype(str).map(len).max(), len(column))
            col_idx = df.columns.get_loc(column)
            writer.sheets['Section_analysis'].set_column(col_idx, col_idx, column_width)

        writer.save()
        self.Frame_section_progressChanged.emit(100)


    def borders_off(self):
        self.all_borders_off = []
        for i in tqdm(range(len(self.impathes)), desc='Borders Off'):
            frame = self.all_frames[i]
            grad_x, grad_y = np.gradient(self.rescaler(frame))
            img_grad = np.sqrt(grad_x**2 + grad_y**2)
            coordonates_grad = self.GetPixelValueBetween(img_grad, 0.02, 1)
            frame1 = self.ChangePixelValue(frame, coordonates_grad, 0)
            self.all_borders_off.append(frame1)
            self.Borders_Off_progressChanged.emit(int((i/len(self.impathes))*100))
        self.Borders_Off_progressChanged.emit(100)
        return np.array(self.all_borders_off)

    def borders_off_visualization(self):
        frame = np.asarray(Image.open(self.parent().ui.Input_Folder_Directory_Text.text()+'/'+self.parent().Picture.impathes[int(self.parent().ui.Picture_number_LineEdit.text())]))
        grad_x, grad_y = np.gradient(self.rescaler(frame))
        img_grad = np.sqrt(grad_x**2 + grad_y**2)
        coordonates_grad = self.GetPixelValueBetween(img_grad, float(self.parent().ui.Gradient_value_lineEdit.text()), 1)
        frame1 = self.ChangePixelValue(frame, coordonates_grad, 0)
        ims = Image.fromarray(frame1)
        ims.show()

    def opening(self):
        a = 0
        self.studied_opening = []
        if self.Opening_methode == 'Ellipse':
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, self.Opening_size)

        elif self.Opening_methode == 'Cross':
            kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, self.Opening_size)

        elif self.Opening_methode == 'Rectangle':
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, self.Opening_size)

        else :
            return print("Wrong opening information")

        for q in range(len(self.object_list)):
            opening_list = []
            for i in tqdm(range(len(self.impathes)), desc='Opening of %s' % self.object_list[q]):
                a = a + 1
                frame = self.all_frames_segmented[q][i]
                opening = cv2.morphologyEx(frame, cv2.MORPH_OPEN, kernel)
                opening_list.append(opening)
                self.Opening_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))))
            self.studied_opening.append(opening_list)
        self.Opening_progressChanged.emit(100)

    def save_images(self):
        a = 0
        if self.format_save == ".Tif":
            for i in range(len(self.object_list)):
                if self.Save_Frames_List[i] is False :
                    a = a + len(self.impathes)
                    continue

                else :
                    for j in tqdm(range(len(self.impathes)), desc='Data back-up of %s' % self.object_list[i]):
                        a = a + 1
                        ims = Image.fromarray(self.all_frames_segmented[i][j])

                        if j<10 :
                            ims.save(self.Output_dir+'/'+self.dossier+'/'+self.object_list[i]+'/'+self.object_list[i]+'0000'+"{0}.tif".format(j), compression = None)
                        if j<100 and j>=10 :
                            ims.save(self.Output_dir+'/'+self.dossier+'/'+self.object_list[i]+'/'+self.object_list[i]+'000'+"{0}.tif".format(j), compression = None)
                        if j<1000 and j>=100 :
                            ims.save(self.Output_dir+'/'+self.dossier+'/'+self.object_list[i]+'/'+self.object_list[i]+'00'+"{0}.tif".format(j), compression = None)
                        if j<10000 and j>=1000 :
                            ims.save(self.Output_dir+'/'+self.dossier+'/'+self.object_list[i]+'/'+self.object_list[i]+'0'+"{0}.tif".format(j), compression = None)
                        if j<100000 and j>=10000 :
                            ims.save(self.Output_dir+'/'+self.dossier+'/'+self.object_list[i]+'/'+self.object_list[i]+"{0}.tif".format(j), compression = None)
                        self.Save_Frames_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))))
            self.Save_Frames_progressChanged.emit(100)

        if self.format_save == ".Raw":
            self.Save_Frames_progressChanged.emit(50)
            for i in range(len(self.object_list)):
                if self.Save_Frames_List[i] is False :
                    continue
                self.all_frames_segmented = np.array(self.all_frames_segmented[i])
                self.all_frames_segmented.astype('int16').tofile(self.Output_dir+'/'+self.dossier+'/'+self.object_list[i]+'/'+self.object_list[i]+'.raw')
                self.Save_Frames_progressChanged.emit(100)

    def Connectivity3D(self):
        a = 0
        self.studied_connectivity = []
        for f in range(len(self.object_list)):
            all_segmentation_labels = []
            all_max_label = []
            object_list_connectivity = []

            for i in tqdm(range(len(self.impathes)), desc='3D_Connectivity of %s' % self.object_list[f]+' 1/4'):
                a = a + 1
                frame = self.all_frames_segmented[f][i]
                segmentation_labels = measure.label(frame, background=0)
                max_label = self.Max_occur(segmentation_labels.flatten(), 0)
                all_segmentation_labels.append(segmentation_labels)
                all_max_label.append(max_label)
                self.Connectivity_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))/4))

            black_frame = np.zeros((frame.shape[0], frame.shape[1]), dtype='uint16')
            for j in tqdm(range(len(self.impathes)), desc='3D_Connectivity of %s' % self.object_list[f]+' 2/4'):
                if j ==0 :
                    a = a + 1
                    coord_label = np.where(all_segmentation_labels[j] == all_max_label[j])
                    zipped_coord_label = np.column_stack((coord_label))
                    label_frame = self.ChangePixelValue(black_frame, zipped_coord_label, 2**16-1)
                    object_list_connectivity.append(label_frame)
                    continue
                a = a + 1
                connectivity = np.where(np.logical_and(np.logical_and(all_segmentation_labels[j-1]==all_max_label[j-1], all_segmentation_labels[j]!=0), all_segmentation_labels[j]!=all_max_label[j]))
                zipped_connectivity = np.column_stack((connectivity))

                for k in range(len(zipped_connectivity)):
                    if len(zipped_connectivity) == 0 :
                        label_frame = all_segmentation_labels[j]
                        continue
                    value = all_segmentation_labels[j][zipped_connectivity[k][0]][zipped_connectivity[k][1]]
                    if value == all_max_label[j]:
                        continue
                    else:

                        change = np.where(all_segmentation_labels[j] == value)
                        zipped_change = np.column_stack((change))
                        for g in range(len(zipped_change)):
                            all_segmentation_labels[j][zipped_change[g][0]][zipped_change[g][1]] = all_max_label[j]

                self.Connectivity_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))/4))



            for p in tqdm(range(len(self.impathes),0,-1), desc='reverse 3D_Connectivity of %s' % self.object_list[f]+' 3/4'):
                if p == len(self.impathes) or p == len(self.impathes)-1:
                    a = a + 1
                    continue
                a = a + 1
                connectivity = np.where(np.logical_and(np.logical_and(all_segmentation_labels[p+1]==all_max_label[p+1], all_segmentation_labels[p]!=0), all_segmentation_labels[p]!=all_max_label[p]))
                zipped_connectivity = np.column_stack((connectivity))
                for m in range(len(zipped_connectivity)):
                    if len(zipped_connectivity) == 0 :
                        label_frame = all_segmentation_labels[p]
                        continue
                    value = all_segmentation_labels[p][zipped_connectivity[m][0]][zipped_connectivity[m][1]]
                    if value == all_max_label[p]:
                        continue
                    else:

                        change = np.where(all_segmentation_labels[p] == value)
                        zipped_change = np.column_stack((change))
                        for o in range(len(zipped_change)):
                            all_segmentation_labels[p][zipped_change[o][0]][zipped_change[o][1]] = all_max_label[p]
                self.Connectivity_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))/4))

            for e in tqdm(range(len(self.impathes)), desc='fabrication 3D_Connectivity of %s' % self.object_list[f]+' 4/4'):
                a = a + 1
                if all_max_label[e] == 0 :
                    object_list_connectivity.append(black_frame)
                else :
                    coord_label = np.where((all_segmentation_labels[e] == all_max_label[e]))
                    zipped_coord_label = np.column_stack((coord_label))
                    label_frame = self.ChangePixelValue(black_frame, zipped_coord_label, 2**16-1)
                    object_list_connectivity.append(label_frame)
                self.Connectivity_progressChanged.emit(int(((a/len(self.impathes))*100)*(1/len(self.object_list))/4))
            self.studied_connectivity.append(object_list_connectivity)

        self.Connectivity_progressChanged.emit(100)

    def array_to_vtkimagedata (self, i) :
        if self.methode1 == 'Without overlap':
            frame_shape_x, frame_shape_y = np.asarray(self.all_frames_segmented[i][0]).shape
        array = []
        array.append(np.zeros((frame_shape_x+2, frame_shape_y+2), dtype=int))
        for j in tqdm(range(len(self.impathes)), desc='building the 3d array'):
            self.a = self.a + 1
            a = np.pad(self.all_frames_segmented[i][j], pad_width=1, mode='constant', constant_values=0)
            array.append(a)
            self.Save_STL_progressChanged.emit(int(((self.a/len(self.impathes))*100)*(1/len(self.object_list))/4))
        array.append(np.zeros((frame_shape_x+2, frame_shape_y+2), dtype=int))


        array = np.array(array)
        nb_stack = array.shape[0]
        height = array.shape[1]
        width = array.shape[2]

        vtk_array = numpy_support.numpy_to_vtk(np.array(array).ravel(), deep=True, array_type=vtk.VTK_DOUBLE)
        vtkImg = vtk.vtkImageData()
        vtkImg.SetDimensions(np.array([width,height,nb_stack]))
        vtkImg.AllocateScalars(vtk.VTK_DOUBLE, 1)
        vtkImg.SetSpacing([self.vx,self.vy,self.vz])
        vtkImg.SetOrigin([0,0,0])
        vtkImg.GetPointData().SetScalars(vtk_array)

        return vtkImg

    def vtkimagedata_to_vtkpolydata(self,i):
        self.a = self.a + len(self.impathes)
        writer = vtk.vtkXMLImageDataWriter()
        writer.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename+".vti")
        if vtk.VTK_MAJOR_VERSION <= 5:
            writer.SetInputConnection(self.vtkImg.GetProducerPort())
        else:
            writer.SetInputData(self.vtkImg)
        writer.Write()
        self.Save_STL_progressChanged.emit(int(((self.a/len(self.impathes))*100)*(1/len(self.object_list))/4))

    def vtkpolydata_to_stl (self,i):
        self.a = self.a + len(self.impathes)
        reader = vtk.vtkXMLImageDataReader()
        reader.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename+".vti")
        reader.Update()

        threshold = vtk.vtkImageThreshold ()
        threshold.SetInputConnection(reader.GetOutputPort())
        threshold.ThresholdByLower(400)  # remove all soft tissue
        threshold.ReplaceInOn()
        threshold.SetInValue(0)  # set all values below 400 to 0
        threshold.ReplaceOutOn()
        threshold.SetOutValue(1)  # set all values above 400 to 1
        threshold.Update()

        dmc = vtk.vtkMarchingCubes()
        dmc.SetInputConnection(reader.GetOutputPort())
        dmc.SetValue(0,400)
        dmc.Update()

        writer = vtk.vtkSTLWriter()
        writer.SetInputConnection(dmc.GetOutputPort())
        writer.SetFileTypeToBinary()
        writer.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename+".stl")
        writer.Write()
        self.Save_STL_progressChanged.emit(int(((self.a/len(self.impathes))*100)*(1/len(self.object_list))/4))

    def array_to_stl (self, i):
        self.a = self.a + len(self.impathes)
        self.filename = self.object_list[i]
        self.vtkImg = self.array_to_vtkimagedata (i)
        self.vtkimagedata_to_vtkpolydata(i)
        self.vtkpolydata_to_stl (i)
        self.Save_STL_progressChanged.emit(int(((self.a/len(self.impathes))*100)*(1/len(self.object_list))/4))
        if i == len(self.object_list):
            self.Save_STL_progressChanged.emit(100)

    def NumberOfTriangles(self, pd):
        cells = pd.GetPolys()
        numOfTriangles = 0
        idList = vtk.vtkIdList()
        for i in range(0, cells.GetNumberOfCells()):
            cells.GetNextCell(idList)
            # If a cell has three points it is a triangle.
            if idList.GetNumberOfIds() == 3:
                numOfTriangles += 1
        return numOfTriangles

    def stl_process(self, i):
        bar = 3*i
        if i == 0 :
            self.Decimation_process_progressChanged.emit(1)
        self.filename = self.object_list[i]
        reader = vtk.vtkSTLReader()
        reader.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename+".stl")
        reader.Update()
        poly_data = reader.GetOutput()

        triangles = vtk.vtkTriangleFilter()
        triangles.SetInputData(poly_data)
        triangles.Update()
        inputPolyData = triangles.GetOutput()

        t = self.NumberOfTriangles(triangles.GetOutput())

        decimation = vtk.vtkDecimatePro()
        decimation.SetInputData(inputPolyData)
        if self.Preserve_topology is True :
            decimation.PreserveTopologyOn()
        decimation.SetTargetReduction(self.Reduction_coeff)
        decimation.GetOutput()
        decimation.Update()
        d = self.NumberOfTriangles(decimation.GetOutput())
        decimated = vtk.vtkPolyData()
        decimated.ShallowCopy(decimation.GetOutput())

        r = (inputPolyData.GetNumberOfPolys() - decimated.GetNumberOfPolys()) / inputPolyData.GetNumberOfPolys()
        self.Decimation_process_progressChanged.emit(int((((1+bar)/3)*100)*(1/len(self.object_list))))
        if self.Save_connectivity is True or (self.Save_connectivity is False and self.Save_filling is True):
            filler = vtk.vtkFillHolesFilter()
            filler.SetInputData(decimated)
            filler.SetHoleSize(20)
            filler.Update()
            f = self.NumberOfTriangles(filler.GetOutput())


        if self.Save_connectivity is True :
            connect = vtk.vtkConnectivityFilter()
            connect.SetInputConnection(filler.GetOutputPort())
            connect.SetExtractionModeToLargestRegion()
            connect.Update()
            c = self.NumberOfTriangles(connect.GetOutput())


        if self.Save_decimation is True :
            writer = vtk.vtkSTLWriter()
            writer.SetInputConnection(decimation.GetOutputPort())
            writer.SetFileTypeToBinary()
            writer.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename+"_decimate.stl")
            writer.Write()

        if self.Save_filling is True :
            writer = vtk.vtkSTLWriter()
            writer.SetInputConnection(filler.GetOutputPort())
            writer.SetFileTypeToBinary()
            writer.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename+"_filling.stl")
            writer.Write()

        if self.Save_connectivity is True :
            writer = vtk.vtkSTLWriter()
            writer.SetInputConnection(connect.GetOutputPort())
            writer.SetFileTypeToBinary()
            writer.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename+"_3Dconnectivity.stl")
            writer.Write()

        self.Decimation_process_progressChanged.emit(int((((2+bar)/3)*100)*(1/len(self.object_list))))
        print ("Values from decimation of : ", self.object_list[i])
        print ("")
        if self.Save_filling and self.Save_connectivity is False :
            table = Texttable()
            table.add_rows([['Steps', 'Triangles', 'Reduction factor'], ['Before decimation', t, '/'], ['After decimation', d, '/'], ['Reduction factor', '/', r]])
            return print(table.draw())
        elif self.Save_connectivity is False :
            if f == 0 :
                a = d
            else :
                a = f
            table = Texttable()
            table.add_rows([['Steps', 'Triangles', 'Reduction factor'], ['Before decimation', t, '/'], ['After decimation', d, '/'], ['After filling', a, '/'],['Reduction factor', '/', r]])
            return print(table.draw())

        else :
            if f == 0 :
                a = d
            else :
                a = f
            table = Texttable()
            table.add_rows([['Steps', 'Triangles', 'Reduction factor'], ['Before decimation', t, '/'], ['After decimation', d, '/'], ['After filling', a, '/'],['After connectivity', c, '/'],['Reduction factor', '/', r]])
            return print(table.draw())
        self.Decimation_process_progressChanged.emit(int((((3+bar)/3)*100)*(1/len(self.object_list))))
        if i == len(self.object_list):
            self.Decimation_process_progressChanged.emit(int((((3+bar)/3)*100)*(1/len(self.object_list))))

    def Mass_center(self, i):
        self.filename = self.object_list[i]
        Reader = vtk.vtkSTLReader()
        Reader.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename+'.stl')
        Reader.Update()
        Polydata = Reader.GetOutput()
        centerOfMass = vtk.vtkCenterOfMass()
        centerOfMass.SetInputData(Polydata)
        centerOfMass.SetUseScalarsAsWeights(False)
        centerOfMass.Update()
        G = centerOfMass.GetCenter()
        self.Mass_centerDf_progressChanged.emit(int((i+1)/(len(self.object_list)+1)))
        return G, self.filename

    def Display_mass_center (self, value_type=np.float32):
        if len(self.filename_list) != len(self.center_list):
            return print ('Inputs has different lenght')

        Titles = ['Filename (.stl)', 'X (mm)', 'Y (mm)' , 'Z (mm)', 'Lenght (mm)']
        Data = []
        Data.append(Titles)
        value_type=np.float32

        for i in range(len(self.filename_list)):
            row = [self.filename_list[i], self.center_list[i][0], self.center_list[i][1], self.center_list[i][2], " ||"+str(np.mean(np.sqrt(self.center_list[i][0]**2 + self.center_list[i][1]**2 + self.center_list[i][2]**2), dtype = value_type))+"|| " ]
            Data.append(row)
        table = Texttable()
        table.add_rows(Data)
        print('Distance between the object mass center and the origin of the .stl file :')
        print('')
        return print(table.draw())

    # for a maximum of 5 stl files
    def display_stl (self) :

        if self.Print_Mass_center is True :
            if self.center_list is None :
                return print('Need a list of mass center')
            if len(self.filename_list) != len(self.center_list):
                return print ('Inputs has different lenght')

        # Define colors
        colors = vtk.vtkNamedColors()
        backFace = vtk.vtkProperty()
        backFaceColor = colors.GetColor3d('Gold')
        backFace.SetColor(backFaceColor)
        sphereColor = colors.GetColor3d('Red')
        ActorColor = colors.GetColor3d('NavajoWhite')

        if len(self.filename_list) == 1 :
            reader0 = vtk.vtkSTLReader()
            reader0.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[0]+".stl")
            reader0.Update()
            poly_data0 = reader0.GetOutput()
            Mapper0 = vtk.vtkPolyDataMapper()
            Mapper0.SetInputData(poly_data0)
            Actor0 = vtk.vtkActor()
            Actor0.SetMapper(Mapper0)
            Actor0.GetProperty().SetInterpolationToFlat()
            Actor0.GetProperty().SetColor(ActorColor)
            Actor0.SetBackfaceProperty(backFace)
            if self.Print_Mass_center is True :
                sphereSource0 = vtk.vtkSphereSource()
                sphereSource0.SetCenter(self.center_list[0][0], self.center_list[0][1], self.center_list[0][2])
                sphereSource0.SetRadius(self.sphere_size)
                sphereSource0.SetThetaResolution(64)
                sphereSource0.SetPhiResolution(64)
                sphereMapper0 = vtk.vtkPolyDataMapper()
                sphereMapper0.SetInputConnection(sphereSource0.GetOutputPort())
                sphereActor0 = vtk.vtkActor()
                sphereActor0.SetMapper(sphereMapper0)
                sphereActor0.GetProperty().SetColor(sphereColor)

            # There will be one render window
            renderWindow = vtk.vtkRenderWindow()
            renderWindow.SetSize(300, 300)
            renderWindow.SetWindowName('STL_Display');

            # And one interactor
            interactor = vtk.vtkRenderWindowInteractor()
            interactor.SetRenderWindow(renderWindow)

            # Setup both renderers
            Renderer0 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer0)
            Renderer0.SetBackground((colors.GetColor3d('Peru')))

            # Add the sphere to the left and the cube to the right
            Renderer0.AddActor(Actor0)
            if self.Print_Mass_center is True :
                Renderer0.AddActor(sphereActor0)

            # Shared camera
            # Shared camera looking down the -y axis
            camera = vtk.vtkCamera()
            camera.SetPosition(0, -1, 0)
            camera.SetFocalPoint(0, 0, 0)
            camera.SetViewUp(0, 0, 1)
            camera.Elevation(30)
            camera.Azimuth(30)

            Renderer0.SetActiveCamera(camera)

            Renderer0.ResetCamera()
            Renderer0.ResetCameraClippingRange()

            renderWindow.Render()
            renderWindow.SetWindowName('STL_Viewer of ' + self.filename_list[0])

        elif len(self.filename_list) == 2 :

            Reader0 = vtk.vtkSTLReader()
            Reader0.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[0]+".stl")
            Reader0.Update()
            Poly_data0 = Reader0.GetOutput()
            Mapper0 = vtk.vtkPolyDataMapper()
            Mapper0.SetInputData(Poly_data0)
            Actor0 = vtk.vtkActor()
            Actor0.SetMapper(Mapper0)
            Actor0.GetProperty().SetInterpolationToFlat()
            Actor0.GetProperty().SetColor(ActorColor)
            Actor0.SetBackfaceProperty(backFace)

            Reader1 = vtk.vtkSTLReader()
            Reader1.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[1]+".stl")
            Reader1.Update()
            Poly_data1 = Reader1.GetOutput()
            Mapper1 = vtk.vtkPolyDataMapper()
            Mapper1.SetInputData(Poly_data1)
            Actor1 = vtk.vtkActor()
            Actor1.SetMapper(Mapper1)
            Actor1.GetProperty().SetInterpolationToFlat()
            Actor1.GetProperty().SetColor(ActorColor)
            Actor1.SetBackfaceProperty(backFace)
            if self.Print_Mass_center is True :
                sphereSource0 = vtk.vtkSphereSource()
                sphereSource0.SetCenter(self.center_list[0][0], self.center_list[0][1], self.center_list[0][2])
                sphereSource0.SetRadius(self.sphere_size)
                sphereSource0.SetThetaResolution(64)
                sphereSource0.SetPhiResolution(64)
                sphereMapper0 = vtk.vtkPolyDataMapper()
                sphereMapper0.SetInputConnection(sphereSource0.GetOutputPort())
                sphereActor0 = vtk.vtkActor()
                sphereActor0.SetMapper(sphereMapper0)
                sphereActor0.GetProperty().SetColor(sphereColor)

                sphereSource1 = vtk.vtkSphereSource()
                sphereSource1.SetCenter(self.center_list[1][0], self.center_list[1][1], self.center_list[1][2])
                sphereSource1.SetRadius(self.sphere_size)
                sphereSource1.SetThetaResolution(64)
                sphereSource1.SetPhiResolution(64)
                sphereMapper1 = vtk.vtkPolyDataMapper()
                sphereMapper1.SetInputConnection(sphereSource1.GetOutputPort())
                sphereActor1 = vtk.vtkActor()
                sphereActor1.SetMapper(sphereMapper1)
                sphereActor1.GetProperty().SetColor(sphereColor)


            # There will be one render window
            renderWindow = vtk.vtkRenderWindow()
            renderWindow.SetSize(600, 300)
            renderWindow.SetWindowName('STL_Display');

            # And one interactor
            interactor = vtk.vtkRenderWindowInteractor()
            interactor.SetRenderWindow(renderWindow)

            # Define viewport ranges
            # (xmin, ymin, xmax, ymax)
            Viewport0 = [0.0, 0.0, 0.5, 1.0]
            Viewport1 = [0.5, 0.0, 1.0, 1.0]

            # Setup both renderers
            Renderer0 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer0)
            Renderer0.SetViewport(Viewport0)
            Renderer0.SetBackground((colors.GetColor3d('Peru')))


            Renderer1 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer1)
            Renderer1.SetViewport(Viewport1)
            Renderer1.SetBackground((colors.GetColor3d('CornflowerBlue')))


            # Add the sphere to the left and the cube to the right
            Renderer0.AddActor(Actor0)
            Renderer1.AddActor(Actor1)
            if self.Print_Mass_center is True :
                Renderer0.AddActor(sphereActor0)
                Renderer1.AddActor(sphereActor1)

            # Add the cameras

            camera0 = vtk.vtkCamera()
            camera0.SetPosition(0, -1, 0)
            camera0.SetFocalPoint(0, 0, 0)
            camera0.SetViewUp(0, 0, 1)
            camera0.Elevation(30)
            camera0.Azimuth(30)

            camera1 = vtk.vtkCamera()
            camera1.SetPosition(0, -1, 0)
            camera1.SetFocalPoint(0, 0, 0)
            camera1.SetViewUp(0, 0, 1)
            camera1.Elevation(30)
            camera1.Azimuth(30)

            Renderer0.SetActiveCamera(camera0)
            Renderer1.SetActiveCamera(camera1)

            Renderer0.ResetCamera()
            Renderer0.ResetCameraClippingRange()
            Renderer1.ResetCamera()
            Renderer1.ResetCameraClippingRange()

            renderWindow.Render()
            renderWindow.SetWindowName('STL_Viewer of '+ self.filename_list[0] + ' / ' + self.filename_list[1] )

        elif len(self.filename_list) == 3 :
            Reader0 = vtk.vtkSTLReader()
            Reader0.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[0]+".stl")
            Reader0.Update()
            Poly_data0 = Reader0.GetOutput()
            Mapper0 = vtk.vtkPolyDataMapper()
            Mapper0.SetInputData(Poly_data0)
            Actor0 = vtk.vtkActor()
            Actor0.SetMapper(Mapper0)
            Actor0.GetProperty().SetInterpolationToFlat()
            Actor0.GetProperty().SetColor(ActorColor)
            Actor0.SetBackfaceProperty(backFace)

            Reader1 = vtk.vtkSTLReader()
            Reader1.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[1]+".stl")
            Reader1.Update()
            Poly_data1 = Reader1.GetOutput()
            Mapper1 = vtk.vtkPolyDataMapper()
            Mapper1.SetInputData(Poly_data1)
            Actor1 = vtk.vtkActor()
            Actor1.SetMapper(Mapper1)
            Actor1.GetProperty().SetInterpolationToFlat()
            Actor1.GetProperty().SetColor(ActorColor)
            Actor1.SetBackfaceProperty(backFace)

            Reader2 = vtk.vtkSTLReader()
            Reader2.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[2]+".stl")
            Reader2.Update()
            Poly_data2 = Reader2.GetOutput()
            Mapper2 = vtk.vtkPolyDataMapper()
            Mapper2.SetInputData(Poly_data2)
            Actor2 = vtk.vtkActor()
            Actor2.SetMapper(Mapper2)
            Actor2.GetProperty().SetInterpolationToFlat()
            Actor2.GetProperty().SetColor(ActorColor)
            Actor2.SetBackfaceProperty(backFace)

            if self.Print_Mass_center is True :
                sphereSource0 = vtk.vtkSphereSource()
                sphereSource0.SetCenter(self.center_list[0][0], self.center_list[0][1], self.center_list[0][2])
                sphereSource0.SetRadius(self.sphere_size)
                sphereSource0.SetThetaResolution(64)
                sphereSource0.SetPhiResolution(64)
                sphereMapper0 = vtk.vtkPolyDataMapper()
                sphereMapper0.SetInputConnection(sphereSource0.GetOutputPort())
                sphereActor0 = vtk.vtkActor()
                sphereActor0.SetMapper(sphereMapper0)
                sphereActor0.GetProperty().SetColor(sphereColor)

                sphereSource1 = vtk.vtkSphereSource()
                sphereSource1.SetCenter(self.center_list[1][0], self.center_list[1][1], self.center_list[1][2])
                sphereSource1.SetRadius(self.sphere_size)
                sphereSource1.SetThetaResolution(64)
                sphereSource1.SetPhiResolution(64)
                sphereMapper1 = vtk.vtkPolyDataMapper()
                sphereMapper1.SetInputConnection(sphereSource1.GetOutputPort())
                sphereActor1 = vtk.vtkActor()
                sphereActor1.SetMapper(sphereMapper1)
                sphereActor1.GetProperty().SetColor(sphereColor)

                sphereSource2 = vtk.vtkSphereSource()
                sphereSource2.SetCenter(self.center_list[2][0], self.center_list[2][1], self.center_list[2][2])
                sphereSource2.SetRadius(self.sphere_size)
                sphereSource2.SetThetaResolution(64)
                sphereSource2.SetPhiResolution(64)
                sphereMapper2 = vtk.vtkPolyDataMapper()
                sphereMapper2.SetInputConnection(sphereSource2.GetOutputPort())
                sphereActor2 = vtk.vtkActor()
                sphereActor2.SetMapper(sphereMapper2)
                sphereActor2.GetProperty().SetColor(sphereColor)

            # There will be one render window
            renderWindow = vtk.vtkRenderWindow()
            renderWindow.SetSize(900, 300)
            renderWindow.SetWindowName('STL_Display');

            # And one interactor
            interactor = vtk.vtkRenderWindowInteractor()
            interactor.SetRenderWindow(renderWindow)

            # Define viewport ranges
            # (xmin, ymin, xmax, ymax)
            Viewport0 = [0.0, 0.0, 0.33, 1.0]
            Viewport1 = [0.33, 0.0, 0.66, 1.0]
            Viewport2 = [0.66, 0.0, 1.0, 1.0]

            # Setup both renderers
            Renderer0 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer0)
            Renderer0.SetViewport(Viewport0)
            Renderer0.SetBackground((colors.GetColor3d('Peru')))


            Renderer1 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer1)
            Renderer1.SetViewport(Viewport1)
            Renderer1.SetBackground((colors.GetColor3d('CornflowerBlue')))

            Renderer2 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer2)
            Renderer2.SetViewport(Viewport2)
            Renderer2.SetBackground((colors.GetColor3d('DarkMagenta')))

            # Add the sphere to the left and the cube to the right
            Renderer0.AddActor(Actor0)
            Renderer1.AddActor(Actor1)
            Renderer2.AddActor(Actor2)

            if self.Print_Mass_center is True :
                Renderer0.AddActor(sphereActor0)
                Renderer1.AddActor(sphereActor1)
                Renderer2.AddActor(sphereActor2)

            # Add the cameras

            camera0 = vtk.vtkCamera()
            camera0.SetPosition(0, -1, 0)
            camera0.SetFocalPoint(0, 0, 0)
            camera0.SetViewUp(0, 0, 1)
            camera0.Elevation(30)
            camera0.Azimuth(30)

            camera1 = vtk.vtkCamera()
            camera1.SetPosition(0, -1, 0)
            camera1.SetFocalPoint(0, 0, 0)
            camera1.SetViewUp(0, 0, 1)
            camera1.Elevation(30)
            camera1.Azimuth(30)

            camera2 = vtk.vtkCamera()
            camera2.SetPosition(0, -1, 0)
            camera2.SetFocalPoint(0, 0, 0)
            camera2.SetViewUp(0, 0, 1)
            camera2.Elevation(30)
            camera2.Azimuth(30)

            Renderer0.SetActiveCamera(camera0)
            Renderer1.SetActiveCamera(camera1)
            Renderer2.SetActiveCamera(camera2)

            Renderer0.ResetCamera()
            Renderer0.ResetCameraClippingRange()
            Renderer1.ResetCamera()
            Renderer1.ResetCameraClippingRange()
            Renderer2.ResetCamera()
            Renderer2.ResetCameraClippingRange()

            renderWindow.Render()
            renderWindow.SetWindowName('STL_Viewer of '+ self.filename_list[0] + ' / ' + self.filename_list[1] + ' / ' + self.filename_list[2] )

        elif len(self.filename_list) == 4 :
            Reader0 = vtk.vtkSTLReader()
            Reader0.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[0]+".stl")
            Reader0.Update()
            Poly_data0 = Reader0.GetOutput()
            Mapper0 = vtk.vtkPolyDataMapper()
            Mapper0.SetInputData(Poly_data0)
            Actor0 = vtk.vtkActor()
            Actor0.SetMapper(Mapper0)
            Actor0.GetProperty().SetInterpolationToFlat()
            Actor0.GetProperty().SetColor(ActorColor)
            Actor0.SetBackfaceProperty(backFace)

            Reader1 = vtk.vtkSTLReader()
            Reader1.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[1]+".stl")
            Reader1.Update()
            Poly_data1 = Reader1.GetOutput()
            Mapper1 = vtk.vtkPolyDataMapper()
            Mapper1.SetInputData(Poly_data1)
            Actor1 = vtk.vtkActor()
            Actor1.SetMapper(Mapper1)
            Actor1.GetProperty().SetInterpolationToFlat()
            Actor1.GetProperty().SetColor(ActorColor)
            Actor1.SetBackfaceProperty(backFace)

            Reader2 = vtk.vtkSTLReader()
            Reader2.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[2]+".stl")
            Reader2.Update()
            Poly_data2 = Reader2.GetOutput()
            Mapper2 = vtk.vtkPolyDataMapper()
            Mapper2.SetInputData(Poly_data2)
            Actor2 = vtk.vtkActor()
            Actor2.SetMapper(Mapper2)
            Actor2.GetProperty().SetInterpolationToFlat()
            Actor2.GetProperty().SetColor(ActorColor)
            Actor2.SetBackfaceProperty(backFace)

            Reader3 = vtk.vtkSTLReader()
            Reader3.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[3]+".stl")
            Reader3.Update()
            Poly_data3 = Reader3.GetOutput()
            Mapper3 = vtk.vtkPolyDataMapper()
            Mapper3.SetInputData(Poly_data3)
            Actor3 = vtk.vtkActor()
            Actor3.SetMapper(Mapper3)
            Actor3.GetProperty().SetInterpolationToFlat()
            Actor3.GetProperty().SetColor(ActorColor)
            Actor3.SetBackfaceProperty(backFace)

            if self.Print_Mass_center is True :
                sphereSource0 = vtk.vtkSphereSource()
                sphereSource0.SetCenter(self.center_list[0][0], self.center_list[0][1], self.center_list[0][2])
                sphereSource0.SetRadius(self.sphere_size)
                sphereSource0.SetThetaResolution(64)
                sphereSource0.SetPhiResolution(64)
                sphereMapper0 = vtk.vtkPolyDataMapper()
                sphereMapper0.SetInputConnection(sphereSource0.GetOutputPort())
                sphereActor0 = vtk.vtkActor()
                sphereActor0.SetMapper(sphereMapper0)
                sphereActor0.GetProperty().SetColor(sphereColor)

                sphereSource1 = vtk.vtkSphereSource()
                sphereSource1.SetCenter(self.center_list[1][0], self.center_list[1][1], self.center_list[1][2])
                sphereSource1.SetRadius(self.sphere_size)
                sphereSource1.SetThetaResolution(64)
                sphereSource1.SetPhiResolution(64)
                sphereMapper1 = vtk.vtkPolyDataMapper()
                sphereMapper1.SetInputConnection(sphereSource1.GetOutputPort())
                sphereActor1 = vtk.vtkActor()
                sphereActor1.SetMapper(sphereMapper1)
                sphereActor1.GetProperty().SetColor(sphereColor)

                sphereSource2 = vtk.vtkSphereSource()
                sphereSource2.SetCenter(self.center_list[2][0], self.center_list[2][1], self.center_list[2][2])
                sphereSource2.SetRadius(self.sphere_size)
                sphereSource2.SetThetaResolution(64)
                sphereSource2.SetPhiResolution(64)
                sphereMapper2 = vtk.vtkPolyDataMapper()
                sphereMapper2.SetInputConnection(sphereSource2.GetOutputPort())
                sphereActor2 = vtk.vtkActor()
                sphereActor2.SetMapper(sphereMapper2)
                sphereActor2.GetProperty().SetColor(sphereColor)

                sphereSource3 = vtk.vtkSphereSource()
                sphereSource3.SetCenter(self.center_list[3][0], self.center_list[3][1], self.center_list[3][2])
                sphereSource3.SetRadius(self.sphere_size)
                sphereSource3.SetThetaResolution(64)
                sphereSource3.SetPhiResolution(64)
                sphereMapper3 = vtk.vtkPolyDataMapper()
                sphereMapper3.SetInputConnection(sphereSource3.GetOutputPort())
                sphereActor3 = vtk.vtkActor()
                sphereActor3.SetMapper(sphereMapper3)
                sphereActor3.GetProperty().SetColor(sphereColor)

            # There will be one render window
            renderWindow = vtk.vtkRenderWindow()
            renderWindow.SetSize(1200, 300)
            renderWindow.SetWindowName('STL_Display');

            # And one interactor
            interactor = vtk.vtkRenderWindowInteractor()
            interactor.SetRenderWindow(renderWindow)

            # Define viewport ranges
            # (xmin, ymin, xmax, ymax)
            Viewport0 = [0.0, 0.0, 0.25, 1.0]
            Viewport1 = [0.25, 0.0, 0.5, 1.0]
            Viewport2 = [0.5, 0.0, 0.75, 1.0]
            Viewport3 = [0.75, 0.0, 1.0, 1.0]

            # Setup both renderers
            Renderer0 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer0)
            Renderer0.SetViewport(Viewport0)
            Renderer0.SetBackground((colors.GetColor3d('Peru')))


            Renderer1 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer1)
            Renderer1.SetViewport(Viewport1)
            Renderer1.SetBackground((colors.GetColor3d('CornflowerBlue')))

            Renderer2 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer2)
            Renderer2.SetViewport(Viewport2)
            Renderer2.SetBackground((colors.GetColor3d('DarkMagenta')))

            Renderer3 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer3)
            Renderer3.SetViewport(Viewport3)
            Renderer3.SetBackground((colors.GetColor3d('LimeGreen')))

            # Add the sphere to the left and the cube to the right
            Renderer0.AddActor(Actor0)
            Renderer1.AddActor(Actor1)
            Renderer2.AddActor(Actor2)
            Renderer3.AddActor(Actor3)
            if self.Print_Mass_center is True :
                Renderer0.AddActor(sphereActor0)
                Renderer1.AddActor(sphereActor1)
                Renderer2.AddActor(sphereActor2)
                Renderer3.AddActor(sphereActor3)

            # Add the cameras
            camera0 = vtk.vtkCamera()
            camera0.SetPosition(0, -1, 0)
            camera0.SetFocalPoint(0, 0, 0)
            camera0.SetViewUp(0, 0, 1)
            camera0.Elevation(30)
            camera0.Azimuth(30)

            camera1 = vtk.vtkCamera()
            camera1.SetPosition(0, -1, 0)
            camera1.SetFocalPoint(0, 0, 0)
            camera1.SetViewUp(0, 0, 1)
            camera1.Elevation(30)
            camera1.Azimuth(30)

            camera2 = vtk.vtkCamera()
            camera2.SetPosition(0, -1, 0)
            camera2.SetFocalPoint(0, 0, 0)
            camera2.SetViewUp(0, 0, 1)
            camera2.Elevation(30)
            camera2.Azimuth(30)

            camera3 = vtk.vtkCamera()
            camera3.SetPosition(0, -1, 0)
            camera3.SetFocalPoint(0, 0, 0)
            camera3.SetViewUp(0, 0, 1)
            camera3.Elevation(30)
            camera3.Azimuth(30)

            Renderer0.SetActiveCamera(camera0)
            Renderer1.SetActiveCamera(camera1)
            Renderer2.SetActiveCamera(camera2)
            Renderer3.SetActiveCamera(camera3)

            Renderer0.ResetCamera()
            Renderer0.ResetCameraClippingRange()
            Renderer1.ResetCamera()
            Renderer1.ResetCameraClippingRange()
            Renderer2.ResetCamera()
            Renderer2.ResetCameraClippingRange()
            Renderer3.ResetCamera()
            Renderer3.ResetCameraClippingRange()

            renderWindow.Render()
            renderWindow.SetWindowName('STL_Viewer of '+ self.filename_list[0] + ' / ' + self.filename_list[1] + ' / ' + self.filename_list[2] + ' / ' + self.filename_list[3])


        elif len(self.filename_list) == 5 :

            Reader0 = vtk.vtkSTLReader()
            Reader0.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[0]+".stl")
            Reader0.Update()
            Poly_data0 = Reader0.GetOutput()
            Mapper0 = vtk.vtkPolyDataMapper()
            Mapper0.SetInputData(Poly_data0)
            Actor0 = vtk.vtkActor()
            Actor0.SetMapper(Mapper0)
            Actor0.GetProperty().SetInterpolationToFlat()
            Actor0.GetProperty().SetColor(ActorColor)
            Actor0.SetBackfaceProperty(backFace)

            Reader1 = vtk.vtkSTLReader()
            Reader1.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[1]+".stl")
            Reader1.Update()
            Poly_data1 = Reader1.GetOutput()
            Mapper1 = vtk.vtkPolyDataMapper()
            Mapper1.SetInputData(Poly_data1)
            Actor1 = vtk.vtkActor()
            Actor1.SetMapper(Mapper1)
            Actor1.GetProperty().SetInterpolationToFlat()
            Actor1.GetProperty().SetColor(ActorColor)
            Actor1.SetBackfaceProperty(backFace)

            Reader2 = vtk.vtkSTLReader()
            Reader2.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[2]+".stl")
            Reader2.Update()
            Poly_data2 = Reader2.GetOutput()
            Mapper2 = vtk.vtkPolyDataMapper()
            Mapper2.SetInputData(Poly_data2)
            Actor2 = vtk.vtkActor()
            Actor2.SetMapper(Mapper2)
            Actor2.GetProperty().SetInterpolationToFlat()
            Actor2.GetProperty().SetColor(ActorColor)
            Actor2.SetBackfaceProperty(backFace)

            Reader3 = vtk.vtkSTLReader()
            Reader3.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[3]+".stl")
            Reader3.Update()
            Poly_data3 = Reader3.GetOutput()
            Mapper3 = vtk.vtkPolyDataMapper()
            Mapper3.SetInputData(Poly_data3)
            Actor3 = vtk.vtkActor()
            Actor3.SetMapper(Mapper3)
            Actor3.GetProperty().SetInterpolationToFlat()
            Actor3.GetProperty().SetColor(ActorColor)
            Actor3.SetBackfaceProperty(backFace)

            Reader4 = vtk.vtkSTLReader()
            Reader4.SetFileName(self.Output_dir+'/'+self.dossier+'/'+self.filename_list[4]+".stl")
            Reader4.Update()
            Poly_data4 = Reader4.GetOutput()
            Mapper4 = vtk.vtkPolyDataMapper()
            Mapper4.SetInputData(Poly_data4)
            Actor4 = vtk.vtkActor()
            Actor4.SetMapper(Mapper4)
            Actor4.GetProperty().SetInterpolationToFlat()
            Actor4.GetProperty().SetColor(ActorColor)
            Actor4.SetBackfaceProperty(backFace)

            if self.Print_Mass_center is True :
                sphereSource0 = vtk.vtkSphereSource()
                sphereSource0.SetCenter(self.center_list[0][0], self.center_list[0][1], self.center_list[0][2])
                sphereSource0.SetRadius(self.sphere_size)
                sphereSource0.SetThetaResolution(64)
                sphereSource0.SetPhiResolution(64)
                sphereMapper0 = vtk.vtkPolyDataMapper()
                sphereMapper0.SetInputConnection(sphereSource0.GetOutputPort())
                sphereActor0 = vtk.vtkActor()
                sphereActor0.SetMapper(sphereMapper0)
                sphereActor0.GetProperty().SetColor(sphereColor)

                sphereSource1 = vtk.vtkSphereSource()
                sphereSource1.SetCenter(self.center_list[1][0], self.center_list[1][1], self.center_list[1][2])
                sphereSource1.SetRadius(self.sphere_size)
                sphereSource1.SetThetaResolution(64)
                sphereSource1.SetPhiResolution(64)
                sphereMapper1 = vtk.vtkPolyDataMapper()
                sphereMapper1.SetInputConnection(sphereSource1.GetOutputPort())
                sphereActor1 = vtk.vtkActor()
                sphereActor1.SetMapper(sphereMapper1)
                sphereActor1.GetProperty().SetColor(sphereColor)

                sphereSource2 = vtk.vtkSphereSource()
                sphereSource2.SetCenter(self.center_list[2][0], self.center_list[2][1], self.center_list[2][2])
                sphereSource2.SetRadius(self.sphere_size)
                sphereSource2.SetThetaResolution(64)
                sphereSource2.SetPhiResolution(64)
                sphereMapper2 = vtk.vtkPolyDataMapper()
                sphereMapper2.SetInputConnection(sphereSource2.GetOutputPort())
                sphereActor2 = vtk.vtkActor()
                sphereActor2.SetMapper(sphereMapper2)
                sphereActor2.GetProperty().SetColor(sphereColor)

                sphereSource3 = vtk.vtkSphereSource()
                sphereSource3.SetCenter(self.center_list[3][0], self.center_list[3][1], self.center_list[3][2])
                sphereSource3.SetRadius(self.sphere_size)
                sphereSource3.SetThetaResolution(64)
                sphereSource3.SetPhiResolution(64)
                sphereMapper3 = vtk.vtkPolyDataMapper()
                sphereMapper3.SetInputConnection(sphereSource3.GetOutputPort())
                sphereActor3 = vtk.vtkActor()
                sphereActor3.SetMapper(sphereMapper3)
                sphereActor3.GetProperty().SetColor(sphereColor)

                sphereSource4 = vtk.vtkSphereSource()
                sphereSource4.SetCenter(self.center_list[4][0], self.center_list[4][1], self.center_list[4][2])
                sphereSource4.SetRadius(self.sphere_size)
                sphereSource4.SetThetaResolution(64)
                sphereSource4.SetPhiResolution(64)
                sphereMapper4 = vtk.vtkPolyDataMapper()
                sphereMapper4.SetInputConnection(sphereSource4.GetOutputPort())
                sphereActor4 = vtk.vtkActor()
                sphereActor4.SetMapper(sphereMapper4)
                sphereActor4.GetProperty().SetColor(sphereColor)

            # There will be one render window
            renderWindow = vtk.vtkRenderWindow()
            renderWindow.SetSize(1500, 300)
            renderWindow.SetWindowName('STL_Display');

            # And one interactor
            interactor = vtk.vtkRenderWindowInteractor()
            interactor.SetRenderWindow(renderWindow)

            # Define viewport ranges
            # (xmin, ymin, xmax, ymax)
            Viewport0 = [0.0, 0.0, 0.2, 1.0]
            Viewport1 = [0.2, 0.0, 0.4, 1.0]
            Viewport2 = [0.4, 0.0, 0.6, 1.0]
            Viewport3 = [0.6, 0.0, 0.8, 1.0]
            Viewport4 = [0.8, 0.0, 1.0, 1.0]

            # Setup both renderers
            Renderer0 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer0)
            Renderer0.SetViewport(Viewport0)
            Renderer0.SetBackground((colors.GetColor3d('Peru')))


            Renderer1 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer1)
            Renderer1.SetViewport(Viewport1)
            Renderer1.SetBackground((colors.GetColor3d('CornflowerBlue')))

            Renderer2 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer2)
            Renderer2.SetViewport(Viewport2)
            Renderer2.SetBackground((colors.GetColor3d('DarkMagenta')))

            Renderer3 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer3)
            Renderer3.SetViewport(Viewport3)
            Renderer3.SetBackground((colors.GetColor3d('LimeGreen')))

            Renderer4 = vtk.vtkRenderer()
            renderWindow.AddRenderer(Renderer4)
            Renderer4.SetViewport(Viewport4)
            Renderer4.SetBackground((colors.GetColor3d('DarkSlateGray')))

            # Add the sphere to the left and the cube to the right
            Renderer0.AddActor(Actor0)
            Renderer1.AddActor(Actor1)
            Renderer2.AddActor(Actor2)
            Renderer3.AddActor(Actor3)
            Renderer4.AddActor(Actor4)
            if self.Print_Mass_center is True :
                Renderer0.AddActor(sphereActor0)
                Renderer1.AddActor(sphereActor1)
                Renderer2.AddActor(sphereActor2)
                Renderer3.AddActor(sphereActor3)
                Renderer4.AddActor(sphereActor4)

            # Add the cameras
            camera0 = vtk.vtkCamera()
            camera0.SetPosition(0, -1, 0)
            camera0.SetFocalPoint(0, 0, 0)
            camera0.SetViewUp(0, 0, 1)
            camera0.Elevation(30)
            camera0.Azimuth(30)

            camera1 = vtk.vtkCamera()
            camera1.SetPosition(0, -1, 0)
            camera1.SetFocalPoint(0, 0, 0)
            camera1.SetViewUp(0, 0, 1)
            camera1.Elevation(30)
            camera1.Azimuth(30)

            camera2 = vtk.vtkCamera()
            camera2.SetPosition(0, -1, 0)
            camera2.SetFocalPoint(0, 0, 0)
            camera2.SetViewUp(0, 0, 1)
            camera2.Elevation(30)
            camera2.Azimuth(30)

            camera3 = vtk.vtkCamera()
            camera3.SetPosition(0, -1, 0)
            camera3.SetFocalPoint(0, 0, 0)
            camera3.SetViewUp(0, 0, 1)
            camera3.Elevation(30)
            camera3.Azimuth(30)

            camera4 = vtk.vtkCamera()
            camera4.SetPosition(0, -1, 0)
            camera4.SetFocalPoint(0, 0, 0)
            camera4.SetViewUp(0, 0, 1)
            camera4.Elevation(30)
            camera4.Azimuth(30)

            Renderer0.SetActiveCamera(camera0)
            Renderer1.SetActiveCamera(camera1)
            Renderer2.SetActiveCamera(camera2)
            Renderer3.SetActiveCamera(camera3)
            Renderer4.SetActiveCamera(camera4)

            Renderer0.ResetCamera()
            Renderer0.ResetCameraClippingRange()
            Renderer1.ResetCamera()
            Renderer1.ResetCameraClippingRange()
            Renderer2.ResetCamera()
            Renderer2.ResetCameraClippingRange()
            Renderer3.ResetCamera()
            Renderer3.ResetCameraClippingRange()
            Renderer4.ResetCamera()
            Renderer4.ResetCameraClippingRange()

            renderWindow.Render()
            renderWindow.SetWindowName('STL_Viewer of '+ self.filename_list[0] + ' / ' + self.filename_list[1] + ' / ' + self.filename_list[2] + ' / ' + self.filename_list[3] + ' / ' + self.filename_list[4])

        interactor.Start()


    def lock(self):
        self.parent().ui.Object_List_LineEdit.clear()
        self.parent().ui.Reset_pushButton.setEnabled(False)
        self.parent().ui.Execute_Button.setEnabled(False)
        self.parent().ui.Object_List_Button_Check.setEnabled(False)
        self.parent().ui.Input_Picture_Folder.setEnabled(False)
        self.parent().ui.Output_Picture_Folder.setEnabled(False)
        self.parent().ui.Segmentation_Methode_1_Label.setEnabled(False)
        self.parent().ui.Segmentation_Methode_2_Label.setEnabled(False)
        self.parent().ui.Segmentation_Methode_1_ComboBox.setEnabled(False)
        self.parent().ui.Segmentation_Methode_2_ComboBox.setEnabled(False)
        self.parent().ui.Voxel_Size_Label.setEnabled(False)
        self.parent().ui.X_Size_Text_Box.setEnabled(False)
        self.parent().ui.Y_Size_Text_Box.setEnabled(False)
        self.parent().ui.Z_Size_Text_Box.setEnabled(False)
        self.parent().ui.Opening_Check_Box.setEnabled(False)
        self.parent().ui.Opening_Methode_Label.setEnabled(False)
        self.parent().ui.Opening_Methode_Combo_Box.setEnabled(False)
        self.parent().ui.Opening_Size_Label.setEnabled(False)
        self.parent().ui.Opening_Size_X_Text_Box.setEnabled(False)
        self.parent().ui.Opening_Size_Y_Text_Box.setEnabled(False)
        self.parent().ui.X_opening_label.setEnabled(False)
        self.parent().ui.Y_opening_label.setEnabled(False)
        self.parent().ui.Borders_Off_Check_Box.setEnabled(False)
        self.parent().ui.Gradient_value_label.setEnabled(False)
        self.parent().ui.Visualize_border_off_pushButton.setEnabled(False)
        self.parent().ui.Gradient_value_lineEdit.setEnabled(False)
        self.parent().ui.Connectivity_Check_Box.setEnabled(False)
        self.parent().ui.Save_Frames_Check_Box.setEnabled(False)
        self.parent().ui.Save_Frames_Combo_Box_Widget_Label.setEnabled(False)
        self.parent().ui.Save_Frame_Methode_Label.setEnabled(False)
        self.parent().ui.Save_Frame_Methode_Combo_Box.setEnabled(False)
        self.parent().ui.Frame_section_Check_Box.setEnabled(False)
        self.parent().ui.Save_STL_Check_Box.setEnabled(False)
        self.parent().ui.Save_STL_Combo_Box_Widget_Label.setEnabled(False)
        self.parent().ui.Decimation_Process_Check_Box.setEnabled(False)
        self.parent().ui.Reduction_Coefficient_Label.setEnabled(False)
        self.parent().ui.Reduction_Coefficient_Text_Box.setEnabled(False)
        self.parent().ui.Preserve_Topology_Check_Box.setEnabled(False)
        self.parent().ui.Combo_Box_Widget_Label.setEnabled(False)
        self.parent().ui.Display_STL_Check_Box.setEnabled(False)
        self.parent().ui.Mass_Center_Check_Box.setEnabled(False)
        self.parent().ui.Show_Mass_Center_Check_Box.setEnabled(False)
        self.parent().ui.Sphere_Size_Text_Box.setEnabled(False)
        self.parent().ui.STL_Display_Combo_Box_Widget_Label.setEnabled(False)
        self.parent().ui.Object_List_Label.setEnabled(False)
        self.parent().ui.Object_List_Label1.setEnabled(False)
        self.parent().ui.Object_List_LineEdit.setEnabled(False)
        self.parent().ui.Delete_Object_List_Button.setEnabled(False)
        self.parent().ui.Save_STL_Combo_Box.setEnabled(False)
        self.parent().ui.STL_Display_Combo_Box.setEnabled(False)
        self.parent().ui.Save_Frames_Combo_Box.setEnabled(False)
        self.parent().ui.STL_Save_Combo_Box.setEnabled(False)
        self.parent().ui.Folder_Select_Label.setEnabled(False)
        self.parent().ui.Folder_Select_Label_1.setEnabled(False)
        self.parent().ui.Folder_Select_Label_1.setEnabled(False)
        self.parent().ui.Manual_checkBox.setEnabled(False)
        self.parent().ui.Manual_obj_label.setEnabled(False)
        self.parent().ui.Manual_max_label.setEnabled(False)
        self.parent().ui.Manual_min_label.setEnabled(False)
        self.parent().ui.Activate_thresholding_checkBox.setEnabled(False)
        self.parent().ui.min_threshold_horizontalScrollBar.setEnabled(False)
        self.parent().ui.max_threshold_horizontalScrollBar.setEnabled(False)
        self.parent().ui.min_threshold_lineEdit.setEnabled(False)
        self.parent().ui.max_threshold_lineEdit.setEnabled(False)
        self.parent().ui.min_threshold_Label.setEnabled(False)
        self.parent().ui.max_threshold_Label.setEnabled(False)
        self.parent().ui.Save_active_frame_pushButton.setEnabled(False)
        self.parent().ui.frame_number_label.setEnabled(False)
        self.parent().ui.Picture_number_LineEdit.setEnabled(False)
        self.parent().ui.Picture_number_horizontalScrollBar.setEnabled(False)
        if self.parent().ObjectList.Object_List_Create_Counter_i == 1 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 2 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label1.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 3 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label1.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_label2.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 4 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label1.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_label2.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_label3.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 5 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label1.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_label2.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_label3.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_label4.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 6 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label1.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_label2.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_label3.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_label4.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_label5.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 7 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_6.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_6.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label1.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_label2.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_label3.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_label4.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_label5.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_label6.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(False)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 8 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_6.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_6.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_7.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_7.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label1.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_label2.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_label3.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_label4.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_label5.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_label6.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(False)
            self.parent().ui.Manual_obj_label7.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(False)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 9 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_6.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_6.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_7.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_7.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_8.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_8.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label1.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_label2.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_label3.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_label4.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_label5.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_label6.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(False)
            self.parent().ui.Manual_obj_label7.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(False)
            self.parent().ui.Manual_obj_label8.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_8.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_8.setEnabled(False)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 10 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_6.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_6.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_7.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_7.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_8.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_8.setEnabled(False)
            self.parent().ui.Object_List_SpinBox_9.setEnabled(False)
            self.parent().ui.Object_List_LineEdited_9.setEnabled(False)
            self.parent().ui.Manual_obj_label0.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(False)
            self.parent().ui.Manual_obj_label1.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(False)
            self.parent().ui.Manual_obj_label2.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(False)
            self.parent().ui.Manual_obj_label3.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(False)
            self.parent().ui.Manual_obj_label4.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(False)
            self.parent().ui.Manual_obj_label5.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(False)
            self.parent().ui.Manual_obj_label6.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(False)
            self.parent().ui.Manual_obj_label7.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(False)
            self.parent().ui.Manual_obj_label8.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_8.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_8.setEnabled(False)
            self.parent().ui.Manual_obj_label9.setEnabled(False)
            self.parent().ui.Manual_obj_min_LineEdited_9.setEnabled(False)
            self.parent().ui.Manual_obj_max_LineEdited_9.setEnabled(False)

    def unlock(self):
        self.parent().ui.Reset_pushButton.setEnabled(True)
        self.parent().ui.Object_List_Button_Check.setEnabled(True)
        self.parent().ui.Delete_Object_List_Button.setEnabled(True)
        self.parent().ui.Execute_Button.setEnabled(True)
        self.parent().ui.Input_Picture_Folder.setEnabled(True)
        self.parent().ui.Output_Picture_Folder.setEnabled(True)
        self.parent().ui.Segmentation_Methode_1_Label.setEnabled(True)
        self.parent().ui.Segmentation_Methode_2_Label.setEnabled(True)
        self.parent().ui.Segmentation_Methode_1_ComboBox.setEnabled(True)
        self.parent().ui.Segmentation_Methode_2_ComboBox.setEnabled(True)
        self.parent().ui.Voxel_Size_Label.setEnabled(True)
        self.parent().ui.X_Size_Text_Box.setEnabled(True)
        self.parent().ui.Y_Size_Text_Box.setEnabled(True)
        self.parent().ui.Z_Size_Text_Box.setEnabled(True)
        self.parent().ui.Opening_Check_Box.setEnabled(True)
        self.parent().ui.Opening_Methode_Label.setEnabled(True)
        self.parent().ui.Opening_Methode_Combo_Box.setEnabled(True)
        self.parent().ui.Opening_Size_Label.setEnabled(True)
        self.parent().ui.Opening_Size_X_Text_Box.setEnabled(True)
        self.parent().ui.Opening_Size_Y_Text_Box.setEnabled(True)
        self.parent().ui.Borders_Off_Check_Box.setEnabled(True)
        self.parent().ui.Connectivity_Check_Box.setEnabled(True)
        self.parent().ui.Save_Frames_Check_Box.setEnabled(True)
        self.parent().ui.Save_Frames_Combo_Box_Widget_Label.setEnabled(True)
        self.parent().ui.Save_Frame_Methode_Label.setEnabled(True)
        self.parent().ui.Save_Frame_Methode_Combo_Box.setEnabled(True)
        self.parent().ui.Frame_section_Check_Box.setEnabled(True)
        self.parent().ui.Save_STL_Check_Box.setEnabled(True)
        self.parent().ui.Save_STL_Combo_Box_Widget_Label.setEnabled(True)
        self.parent().ui.Decimation_Process_Check_Box.setEnabled(True)
        self.parent().ui.Reduction_Coefficient_Label.setEnabled(True)
        self.parent().ui.Reduction_Coefficient_Text_Box.setEnabled(True)
        self.parent().ui.Preserve_Topology_Check_Box.setEnabled(True)
        self.parent().ui.Combo_Box_Widget_Label.setEnabled(True)
        self.parent().ui.Display_STL_Check_Box.setEnabled(True)
        self.parent().ui.Mass_Center_Check_Box.setEnabled(True)
        self.parent().ui.Show_Mass_Center_Check_Box.setEnabled(True)
        self.parent().ui.Sphere_Size_Text_Box.setEnabled(True)
        self.parent().ui.STL_Display_Combo_Box_Widget_Label.setEnabled(True)
        self.parent().ui.Object_List_Label.setEnabled(True)
        self.parent().ui.Object_List_LineEdit.setEnabled(True)
        self.parent().ui.Object_List_Label1.setEnabled(True)
        self.parent().ui.Save_STL_Combo_Box.setEnabled(True)
        self.parent().ui.STL_Display_Combo_Box.setEnabled(True)
        self.parent().ui.Save_Frames_Combo_Box.setEnabled(True)
        self.parent().ui.STL_Save_Combo_Box.setEnabled(True)
        self.parent().ui.Folder_Select_Label.setEnabled(True)
        self.parent().ui.Folder_Select_Label_1.setEnabled(True)
        self.parent().ui.Manual_checkBox.setEnabled(True)
        self.parent().ui.Manual_obj_label.setEnabled(True)
        self.parent().ui.Manual_max_label.setEnabled(True)
        self.parent().ui.Manual_min_label.setEnabled(True)
        self.parent().ui.Activate_thresholding_checkBox.setEnabled(True)
        self.parent().ui.Save_active_frame_pushButton.setEnabled(True)
        self.parent().ui.frame_number_label.setEnabled(True)
        self.parent().ui.Picture_number_LineEdit.setEnabled(True)
        self.parent().ui.Picture_number_horizontalScrollBar.setEnabled(True)
        if self.parent().ObjectList.Object_List_Create_Counter_i == 1 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 2 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label1.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 3 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label1.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_label2.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 4 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label1.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_label2.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_label3.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 5 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label1.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_label2.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_label3.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_label4.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 6 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label1.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_label2.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_label3.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_label4.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_label5.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 7 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_6.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_6.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label1.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_label2.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_label3.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_label4.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_label5.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_label6.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(True)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 8 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_6.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_6.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_7.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_7.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label1.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_label2.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_label3.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_label4.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_label5.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_label6.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(True)
            self.parent().ui.Manual_obj_label7.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(True)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 9 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_6.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_6.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_7.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_7.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_8.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_8.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label1.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_label2.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_label3.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_label4.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_label5.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_label6.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(True)
            self.parent().ui.Manual_obj_label7.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(True)
            self.parent().ui.Manual_obj_label8.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_8.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_8.setEnabled(True)
        elif self.parent().ObjectList.Object_List_Create_Counter_i == 10 :
            self.parent().ui.Object_List_SpinBox_0.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_0.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_1.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_1.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_2.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_2.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_3.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_3.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_4.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_4.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_5.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_5.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_6.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_6.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_7.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_7.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_8.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_8.setEnabled(True)
            self.parent().ui.Object_List_SpinBox_9.setEnabled(True)
            self.parent().ui.Object_List_LineEdited_9.setEnabled(True)
            self.parent().ui.Manual_obj_label0.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_0.setEnabled(True)
            self.parent().ui.Manual_obj_label1.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_1.setEnabled(True)
            self.parent().ui.Manual_obj_label2.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_2.setEnabled(True)
            self.parent().ui.Manual_obj_label3.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_3.setEnabled(True)
            self.parent().ui.Manual_obj_label4.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_4.setEnabled(True)
            self.parent().ui.Manual_obj_label5.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_5.setEnabled(True)
            self.parent().ui.Manual_obj_label6.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_6.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_6.setEnabled(True)
            self.parent().ui.Manual_obj_label7.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_7.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_7.setEnabled(True)
            self.parent().ui.Manual_obj_label8.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_8.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_8.setEnabled(True)
            self.parent().ui.Manual_obj_label9.setEnabled(True)
            self.parent().ui.Manual_obj_min_LineEdited_9.setEnabled(True)
            self.parent().ui.Manual_obj_max_LineEdited_9.setEnabled(True)
        if self.parent().ui.Activate_thresholding_checkBox.isChecked() is True:
            self.parent().ui.min_threshold_lineEdit.setEnabled(True)
            self.parent().ui.max_threshold_lineEdit.setEnabled(True)
            self.parent().ui.min_threshold_horizontalScrollBar.setEnabled(True)
            self.parent().ui.max_threshold_horizontalScrollBar.setEnabled(True)
            self.parent().ui.min_threshold_Label.setEnabled(True)
            self.parent().ui.max_threshold_Label.setEnabled(True)
        if self.parent().ui.Borders_Off_Check_Box.isChecked() == True:
            self.parent().ui.Gradient_value_label.setEnabled(True)
            self.parent().ui.Gradient_value_lineEdit.setEnabled(True)
        if float(self.parent().ui.Gradient_value_lineEdit.text()) != 0.0 and len(self.parent().Picture.impathes) !=0 and self.parent().ui.Borders_Off_Check_Box.isChecked() == True :
            self.parent().ui.Visualize_border_off_pushButton.setEnabled(True)


if __name__ == '__main__':
    app = QtWidgets.QApplication.instance()
    if app is None:
        app = QtWidgets.QApplication(sys.argv)
    else:
        print('QApplication instance already exists: %s' % str(app))
    MainWindow = MainWindow()
    MainWindow.show()
    sys.exit(app.exec_())
