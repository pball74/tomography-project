.. Tomography documentation master file, created by
   sphinx-quickstart on Mon Feb 22 13:52:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to Computed Tomography Segmentation's documentation
###########################################################


.. rubric:: Context :

The challenge of this project is to develop a program that is able to analyse tomography images in an autonomus way.
In this program we want to segmente the different materials displayed on an image.
For this reason we have developped a class using several functions to execute the procedure.
This "Sphinx" documentation will present these different functions, explain their uses and how they compute. 

.. contents:: Computed Tomography Segmentation's Table of Contents



Tomography Class
################

*******
Library
*******

Library used : ::

    %matplotlib notebook
    from PIL import Image
    import matplotlib.pyplot as plt
    plt.isinteractive()
    import os 
    import numpy as np 
    from scipy.signal import argrelextrema
    from scipy import interpolate, signal, ndimage
    from tqdm import tqdm
    import cv2
    import numba
    from numba import jit
    
    

* *matplotlib* to display graphics
* *PIL* to open images from a directory
* *os* to create folders
* *numpy* for mathematical operations and others
* *scipy* to create a cubic function from our gray level histogram and filter it or  to get its extrema
* *tqdm* for visual progression bar
* *cv2* Open.CV for Erode and Dilate operations
* *numba* to accelerate the speed of the program's execution



************
def__init__
************

Programmation of the initialized function : ::

    def __init__(self,
                data_dir="E://Dossier_KevinCO/Scan_001_Os_implant/SlicesY",
                format_img = ".tif",
                methode1 = 'Simple',
                methode2 = 'Globale',
                object_list = ["vide", "os", "implant", "trocart"],
                vx = 0.07,
                vy = 0.07,
                vz = 0.07,
                Borders_Off = False,
                Opening = False,
                Opening_methode = 'Ellipse',
                Opening_size = (5,5)):

        self.data_dir = data_dir
        self.format_img = format_img
        self.methode1 = methode1
        self.methode2 = methode2
        self.object_list = object_list
        self.impathes = sorted([f for f in os.listdir(self.data_dir) if f.endswith(self.format_img)])
        self.parent = os.path.dirname(self.data_dir)
        self.pixel_volume = vx*vy*vz
        self.Borders_Off = Borders_Off
        self.Borders = 'Borders_Off'
        self.Opening = Opening
        self.Opening_methode = Opening_methode
        self.Opening_size = Opening_size

The input parameters are :

* *self* for the class
* *data_dir* default directory of the set of tomography images, can be changed by doing *self.data_dir = new_data_dir*
* *format_image* default entry *.tif* because tomography images are saved in this format
* *methode1* default entry *'Simple'* for a segmentation at the minimum value, can also be set on *'Commune'* for a segmentation taking into account one pixel being able to belong to several materials
* *methode2* default entry *'Globale'* for global thresholding, can also be set on *'Unique'* for a frame to frame threshold
* *object_list* list of the different objects composing the studied subject, without forgetting the air (air has it's own density, not mandatory if the pictures of the subject studied are focused on the system and the materials are none porous)
* *vx, vy, vz* default value in milimeter, needed to calculate the volume of the different object_list items
* *Borders_Off* default entry set to **False**, can be set to **True** to remove frontiers between the materials
* *Opening* default entry set to **False**, can be set to **True** to apply an  *Opening* methode (Erode and Dilate) on the segmented frames
* *Opening_methode* default entry set to *'Ellipse'*, can be set to *'Rectangle'* and *'Cross'* for an elliptical, rectangle or cross matrix process
* *Opening_size* size of the desired matrix for the *Opening* process      



******************************************
Other functions useful for this program :
******************************************


Create_out_dirs
***************

This function is used to create folders in the parent directory of the frames used for the study.

* *Input* : object_list of the studied system **list**, folder directory of the studied frames
* *Ouput* : creation of folders in the parent directory of the studied frames ::

    def create_out_dirs(self):
        
        if not os.path.exists(os.path.join(self.parent, self.dossier)):
            os.mkdir(os.path.join(self.parent, self.dossier))
            self.simple_dir = os.path.join(self.parent, self.dossier) 

            for i in range(len(self.object_list)):
                os.mkdir(os.path.join(self.simple_dir, self.object_list[i]))
            print("Folder '% s' has been created in '%s'" % (self.dossier, self.parent))
            print("Folder(s) '% s' has been created in '% s'" % (self.object_list, self.dossier))

        else:
            print("Folder(s) already created {0}".format(os.path.join(self.parent, self.dossier)))

    >>>Folder 'Segmentation_Simple_Globale' has been created in 'E://Dossier_KevinCO/Scan_001_Os_implant'
    >>>Folder(s) '['vide', 'os', 'implant', 'trocart']' has been created in 'Segmentation_Simple_Globale'

    

 
GetPixelValue : Between, Outside, BetweenSeveral
**************************************************

These functions are used to find the coordinates of pixels that have a value equal / in a range of / in several ranges of or out of a range of gray level. The coordinates are given back in a shape list of x and y integer values. 

* *Input* : frame studied in array type **16-bit**, exact/lower and upper level gray value **int16**  
* *Ouput* : 2D array of x and y coordinates in **int16** ::

    @staticmethod
    @jit(nopython=True)
    def GetPixelValue(source, value):
        position = np.where(source==value)
        zipped = np.column_stack((position))
        return zipped
    
    @staticmethod
    @jit(nopython=True)
    def GetPixelValueBetween(source, infvalue, suppvalue):
        position = np.where(np.logical_and(infvalue<source, source<suppvalue))
        zipped = np.column_stack((position))
        return zipped
    
    @staticmethod
    @jit(nopython=True)
    def GetPixelValueOutside(source, infvalue, suppvalue):
        position = np.where(np.logical_or(infvalue>source, source>suppvalue))
        zipped = np.column_stack((position))
        return zipped 


    def GetPixelValueBetweenSeveral(self, source, limits): 
        l_0 = None
        for limit in limits :
            l_1 = self.GetPixelValueBetween(source, limit[0], limit[1])
            if l_0 is None:
                l_0 = l_1
                continue
            else:
                l_0 = np.concatenate([l_0, l_1], axis=0)        
        return l_0

   

ChangePixelValue, GrayToRGB
***************************

These functions are used to change a pixel's value knowing its coordinates.

* *Input* : frame studied in array type **16-bit**, 2D array of coordinates, new pixel level gray value **int16**  
* *Ouput* : frame studied in array type with the changed value **16-bit** ::

    @staticmethod
    @jit(nopython=True)
    def ChangePixelValue(source, coordonates, value):
        image = source.copy()
        for i in range(len(coordonates)):
            image[coordonates[i][0],coordonates[i][1]] = value
        return image
    
    @staticmethod
    @jit(nopython=True)
    def Wrapper(coordonates, value_r, value_g, value_b,image_rgb):
        for i in range(len(coordonates)):
            image_rgb[:,:,:3][coordonates[i][0],coordonates[i][1]] = [value_r, value_g, value_b]
        image_rgb = (255.0 / (2**16-1) * (image_rgb - image_rgb.min())).astype(np.uint8)
        return image_rgb


    def ChangePixelValueGrayToRGB(self, source, coordonates, value_r, value_g, value_b ):
        frame = source.copy()
        image_rgb = np.dstack([frame, frame, frame]).reshape(frame.shape[0], frame.shape[1],3)
        image_rgb = self.Wrapper(coordonates, value_r, value_g, value_b,image_rgb)
        return image_rgb


    def ImageGrayToRGB(source):
        frame = source.copy()
        image_rgb = np.dstack([frame, frame, frame]).reshape(frame.shape[0],frame.shape[1],3)  
        image_rgb = (255.0 / image_rgb.max() * (image_rgb - image_rgb.min())).astype(np.uint8)
        return image_rgb


Rescaler
********

This function is used to rescale the gray value of an image, this operation is needed when converting a **16-bit** image into a **8-bit** image.

* *Input* : frame studied in array type **16-bit**
* *Ouput* : frame studied in array type **8-bit** ::

    def rescaler(self, value):
        dist = value.max() - value.min()
        out_value = value / (dist) - (value.min() / dist)
        return out_value


Min_val_above
*************

This function is needed when we are looking for the minimum gray value of a frame (thresholding phase). When Borders_Off = **True** the minimum gray value of the output frame equals 0 so we need to find the minimum gray value above 0.

* *Input* : frame studied in array type **16-bit**
* *Ouput* : **int16** value ::

    @staticmethod
    @jit(nopython=True)
    def min_val_above (array):
        min_val = 2**16
        for i in range(len(array)) : 
            if array[i] > 0 and min_val > array[i] : 
                min_val = array[i] 
        return min_val

SumAllFrame
***********

This function is used before thresholding operation, it concatenates the set of the studied frames gray pixels value in one array for futher use in thresholding.

* *Input* : folder directory of the studied frames
* *Ouput* : concatenated array of **int16** values ::

    def SumAllFrame(self):
        all_frame = []
        all_of_frame = []
        for i in tqdm(range(len(self.impathes)), desc='Count of all frames pixels'):
            frame = np.asarray(Image.open(self.data_dir+'/'+self.impathes[i])).flatten()
            all_frame =np.concatenate([all_frame, frame], axis=0)
            if len(all_frame) > ((frame.shape[0])*((1/20)*len(self.impathes))):
                all_of_frame = np.concatenate([all_of_frame, all_frame], axis=0)
                all_frame = []
        all_of_frame =np.concatenate([all_of_frame, all_frame], axis=0)
        all_of_frame = all_of_frame.astype(int)
        return all_of_frame

Limits
******

This fonction is used when *methode1 = 'Commune'*. It calculates the borders limits for the thresholding operation.

* *Input* : limits of the *methode1 = 'Commune'* **list**, object_list of the studied system **list**
* *Ouput* : limits values in **int16** ::

        def limits(self, limnew, q):
            if q<len(self.object_list)-1 and q>0 and (limnew[q][1]>limnew[q+1][0] and limnew[q][0]<limnew[q-1][1]) :
                limin_inf = limnew[q-1][1]
                limin_supp = limnew[q+1][0]
                between = [[limnew[q][0], limnew[q-1][1]],[limnew[q+1][0], limnew[q][1]]]
                cond = 2
                return limin_inf, limin_supp, limin_inf, limin_supp, cond, between
            elif q<len(self.object_list)-1 and (limnew[q][1]>limnew[q+1][0]) :
                limin_inf = limnew[q][0]
                limin_supp = limnew[q+1][0]
                between_inf = limnew[q+1][0]
                between_supp = limnew[q][1]
                cond = 1
                return limin_inf, limin_supp, between_inf, between_supp, cond, None
            elif q>0 and (limnew[q][0]<limnew[q-1][1]) :
                limin_inf = limnew[q-1][1]
                limin_supp = limnew[q][1]
                between_inf = limnew[q][0]
                between_supp = limnew[q-1][1]
                cond = 1
                return limin_inf, limin_supp, between_inf, between_supp, cond, None  
            else : 
                limin_inf = limnew[q][0]
                limin_supp = limnew[q][1]
                cond = 0
                return limin_inf, limin_supp, limin_inf, limin_supp, cond, None 


************
Thresholding
************


Simple
******

The thresholding *methode1 = 'Simple'* refers to the way of tresholding at the minimum of the frame's gray level histogram function. A pixel can only be part of a single material.

* *Input* : object_list of the studied system **list**, extremum values
* *Ouput* : list of gray level value for the borders **int16** ::
   
    lim = []
    for i in range(len(self.object_list)):

        ainf = np.array(g)[np.array(g) < j[i]].max()
        asupp = np.array(g)[np.array(g) > j[i]].min()
        lim.append([int(Xi[ainf]), int(Xi[asupp])])

    >>> [[22724, 24665], [24665, 26073], [29252, 34602], [34602, 38375]]
  
.. image:: Threshold_Simple.png
    :align: center
    :alt: alternate text

Commune
*******

The thresholding *methode1 = 'Commune'* refers to the way of thresholding at the zero value of the minimum and maximum function of the frame's gray level histogram. A pixel can be part of several materials.

* *Input* : object_list of the studied system **list**, extremum values **list**
* *Ouput* : list of gray level value for the borders **int16** ::

    lim1 = []
    lim = []
    limnew = []
    for i in range(len(self.object_list)):

        ainf = np.array(g)[np.array(g) < j[i]].max()
        asupp = np.array(g)[np.array(g) > j[i]].min()
        lim.append([int(Xi[ainf]), int(Xi[asupp])])
        lim1.append([ainf, asupp])

        pente1 = (Yi[j[i]] - Yi[lim1[i][0]]) / (Xi[j[i]] - Xi[lim1[i][0]]) 
        pente2 = (Yi[lim1[i][1]] - Yi[j[i]]) / (Xi[lim1[i][1]] - Xi[j[i]]) 
        b1 = Yi[lim1[i][0]] 
        b2 = Yi[j[i]] 
        nx1 = (-b1)/(pente1)+Xi[lim1[i][0]] 
        nx2 = (-b2)/(pente2)+Xi[j[i]]
        limnew.append([int(nx1), int(nx2)])

    >>> [[22765, 24639], [24381, 26155], [29933, 33576], [35131, 37190]]

.. image:: Threshold_Commune.png
    :align: center
    :alt: alternate text

Globale
*******

The thresholding *methode2 = 'Globale'* refers to the way of thresholding the function of the stack frames' gray level histogram. All the pictures are summed together to get a single histogram.

* *Input* : object_list of the studied system **list**, concatened array of all frames studied, format of the frames
* *Ouput* : list of gray level value for the borders **int16** *methode1 = 'Simple'* and *methode1 = 'Commune'*, list of maximum values **int16** ::

    def SegGlobaleHist(self):
        
        m = np.bincount(self.all_of_frame, minlength = 2**16)
        minV = self.min_val_above (self.all_of_frame)
        maxV = self.all_of_frame.flatten().max()

        interV = (maxV - minV)
        yp = m[0:2**16] 
        xp = list(range(0,2**16))
        f = interpolate.interp1d(xp, ndimage.gaussian_filter(yp, 100), kind='cubic')

        Xi = np.linspace(minV, maxV, interV)
        Yi = f(Xi)
        h= int(interV*0.01)
        dYi = (f(Xi+h)-f(Xi-h))/(2*h)
        ddYi = np.gradient(ndimage.gaussian_filter(dYi, 100))                      

        # for local maxima
        j = argrelextrema(Yi, np.greater, order = 100) 
        j = np.array(j).flatten()
        j1 =[]
        for i in range(len(j)):
            if Yi[j[i]] > 1000 :
                j1.append(int(j[i]))
        j = j1

        # for local minima
        g = argrelextrema(Yi, np.less, order = 100)
        g = np.array(g).flatten()
        g1 = []
        for i in range(len(g)):
            if Yi[g[i]] > 5 :
                g1.append(int(g[i]))
        g = g1   

        # for local mimima on ddYi
        b = argrelextrema(ddYi, np.less, order = 100)
        b = np.array(b).flatten()
        b1 = []
        for i in range(len(b)):
            if Yi[b[i]] > 1000 :
                b1.append(int(b[i]))
        b = b1   

        j2 = []
        for i in range(len(j)):
            for l in range(len(b)):
                if abs(Xi[j[i]]-Xi[b[l]])<300: 
                    j2.append(j[i])
                    break
        j=j2

        if len(j)>len(self.object_list):
            index = list(Yi[j]).index(Yi[j].min())
            j = np.delete(j, index)

        lim1 = []
        lim = []
        limnew = []
        for i in range(len(self.object_list)):

            ainf = np.array(g)[np.array(g) < j[i]].max()
            asupp = np.array(g)[np.array(g) > j[i]].min()
            lim.append([int(Xi[ainf]), int(Xi[asupp])])
            lim1.append([ainf, asupp])

            pente1 = (Yi[j[i]] - Yi[lim1[i][0]]) / (Xi[j[i]] - Xi[lim1[i][0]]) 
            pente2 = (Yi[lim1[i][1]] - Yi[j[i]]) / (Xi[lim1[i][1]] - Xi[j[i]]) 
            b1 = Yi[lim1[i][0]] 
            b2 = Yi[j[i]]
            nx1 = (-b1)/(pente1)+Xi[lim1[i][0]] 
            nx2 = (-b2)/(pente2)+Xi[j[i]] 
            limnew.append([int(nx1), int(nx2)])

        extremum = Xi[j]
        return lim, limnew, extremum

.. image:: Threshold_Globale.png
    :align: center
    :alt: alternate text

Unique
******


The thresholding *methode2 = 'Globale'* refers to the way of thresholding the function of each frame's gray level histogram. The picture's gray level histogram is calculated for each frame.

* *Input* : object_list of the studied system **list**, folder directory of the frame studied, format of the frames
* *Ouput* : list of gray level value for the borders **int16** *methode1 = 'Simple'* and *methode1 = 'Commune'* ::

    def SegUniqueHist(self, frame, Extremum_Global):
    
        m = np.bincount(frame.flatten(), minlength = 2**16)
        minV = self.min_val_above (frame.flatten())
        maxV = frame.flatten().max()

        interV = (maxV - minV)
        yp = m[0:2**16] 
        xp = list(range(0,2**16))
        f = interpolate.interp1d(xp, ndimage.gaussian_filter(yp, 100), kind='cubic')

        Xi = np.linspace(minV, maxV, interV)
        Yi = f(Xi)
        h= int(interV*0.01)
        dYi = (f(Xi+h)-f(Xi-h))/(2*h)
        ddYi = np.gradient(ndimage.gaussian_filter(dYi, 100))                      

        # for local maxima
        j = argrelextrema(Yi, np.greater, order = 1000) 
        j = np.array(j).flatten()

        # for local minima
        g = argrelextrema(Yi, np.less, order = 800)
        g = np.array(g).flatten()

        Extremum = Xi[j]
        j3 = []
        j1 = []

        c = 0

        for k in range(len(Extremum_Global)):
            c = c+1
            j3 = []
            for i in range(len(j)):              
                if Extremum[i]<Extremum_Global[k]+1000 and Extremum[i]>Extremum_Global[k]-1000 :
                    j3.append(j[i])

            while len(j3) !=1 and len(j3) !=0:
                index = list(Yi[j3]).index(Yi[j3].min())
                j3 = np.delete(j3, index)
            if len(j3) == 1:
                j1.append(j3[0])
            if len(j1)<c :
                j1.append(0)
        j = j1

        lim1 = []
        lim = []
        limnew = []

        for i in range(len(self.object_list)):
            if j[i]>0 and np.count_nonzero(np.array(g) < j[i]) != 0 and np.count_nonzero(np.array(g) > j[i]) != 0 :
                ainf = np.array(g)[np.array(g) < j[i]].max()
                asupp = np.array(g)[np.array(g) > j[i]].min()
                lim.append([int(Xi[ainf]), int(Xi[asupp])])
                lim1.append([ainf, asupp])
            elif j[i]>0 and np.count_nonzero(np.array(g) < j[i]) == 0 and np.count_nonzero(np.array(g) > j[i]) != 0 :
                ainf = 0
                asupp = np.array(g)[np.array(g) > j[i]].min()
                lim.append([int(Xi[ainf]), int(Xi[asupp])])
                lim1.append([ainf, asupp])
            elif j[i]>0 and np.count_nonzero(np.array(g) < j[i]) != 0 and np.count_nonzero(np.array(g) > j[i]) == 0 :
                ainf = np.array(g)[np.array(g) < j[i]].max()
                asupp = interV-1
                lim.append([int(Xi[ainf]), int(Xi[asupp])])
                lim1.append([ainf, asupp])
            else :
                lim.append([0, 0])
                lim1.append([0, 0])

        for i in range(len(self.object_list)):
            if j[i]>0 :
                pente1 = (Yi[j[i]] - Yi[lim1[i][0]]) / (Xi[j[i]] - Xi[lim1[i][0]]) 
                pente2 = (Yi[lim1[i][1]] - Yi[j[i]]) / (Xi[lim1[i][1]] - Xi[j[i]]) 
                b1 = Yi[lim1[i][0]] 
                b2 = Yi[j[i]] 
                nx1 = (-b1)/(pente1)+Xi[lim1[i][0]] 
                nx2 = (-b2)/(pente2)+Xi[j[i]] 
                limnew.append([int(nx1), int(nx2)])
            else : 
                limnew.append([0, 0])

        return lim, limnew



************
Segmentation
************

This function regroups most of the previous functions described above. The purpose of the *Segmentation* function is to binarize the studied stack of frames in defferent materials.

* *Input* : object_list of the studied system **list**, folder directory of the frames studied, list of border from threshold depending the methode **list**, format of the frames, methode1 and 2
* *Ouput* : binarized frames saved in several folders depending of folder directory of the frames studied and object_list ::

    def segmentation(self):
        
        if self.Borders_Off == True :
            self.data_dir = self.parent+'/'+self.Borders
            
        self.dossier = "Segmentation"+'_'+self.methode1+'_'+self.methode2   
        
        if os.path.exists(os.path.join(self.parent, self.dossier)):
            return print("File '% s' already exists, can not proceed to segmentation" % self.dossier)
        
        self.create_out_dirs()
        self.all_of_frame = self.SumAllFrame()
        
        if self.methode2 == 'Globale' :

            if self.methode1 == 'Simple' : 
                lim = self.SegGlobaleHist()[0]

                for q in range(len(self.object_list)) :
                    limmin = lim[q][0]
                    limmax = lim[q][1]

                    for i in tqdm(range(len(self.impathes)), desc='Segmentation of %s' % self.object_list[q]): 
                        frame = np.asarray(Image.open(self.data_dir+'/'+self.impathes[i]))
                        coordonates_in = self.GetPixelValueBetween(frame, limmin, limmax)
                        coordonates_out = self.GetPixelValueOutside(frame, limmin, limmax)
                        frame1 = self.ChangePixelValue(frame, coordonates_in, 2**16-1)
                        frame2 = self.ChangePixelValue(frame1, coordonates_out, 0)
                        ims = Image.fromarray(frame2)

                        if i<10 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'0000'+"{0}.tif".format(i), compression = None)
                        if i<100 and i>=10 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'000'+"{0}.tif".format(i), compression = None)
                        if i<1000 and i>=100 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'00'+"{0}.tif".format(i), compression = None)
                        if i<10000 and i>=1000 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'0'+"{0}.tif".format(i), compression = None)
                        if i<100000 and i>=10000 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+"{0}.tif".format(i), compression = None)

            elif self.methode1 == 'Commune' :
                limnew = self.SegGlobaleHist()[1]

                for q in range(len(self.object_list)) :
                    limin_inf, limin_supp, between_inf, between_supp, cond, between = self.limits(limnew, q)

                    for i in tqdm(range(len(self.impathes)), desc='Segmentation of %s' % self.object_list[q]):
                        frame = np.asarray(Image.open(self.data_dir+'/'+self.impathes[i]))
                        coordonates_out = self.GetPixelValueOutside(frame, limnew[q][0], limnew[q][1])
                        frame1 = self.ChangePixelValue(frame, coordonates_out, 0)
                        coordonates_in = self.GetPixelValueBetween(frame1, limin_inf, limin_supp)
                        frame2 = self.ChangePixelValue(frame1, coordonates_in, 2**16-1)
                        if cond==0 : 
                            frame_rgb = self.ImageGrayToRGB(frame2)
                        if cond==1 : 
                            coordonates_between = self.GetPixelValueBetween(frame2, between_inf, between_supp)
                            frame_rgb = self.ChangePixelValueGrayToRGB(frame2, coordonates_between, 2**16-1,0,0)
                        if cond==2 : 
                            coordonates_between = self.GetPixelValueBetweenSeveral(frame2, between)
                            frame_rgb = self.ChangePixelValueGrayToRGB(frame2, coordonates_between, 2**16-1,0,0)
                        ims = Image.fromarray(frame_rgb)
                        if i<10 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'0000'+"{0}.tif".format(i), compression = None)
                        if i<100 and i>=10 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'000'+"{0}.tif".format(i), compression = None)
                        if i<1000 and i>=100 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'00'+"{0}.tif".format(i), compression = None)
                        if i<10000 and i>=1000 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'0'+"{0}.tif".format(i), compression = None)
                        if i<100000 and i>=10000 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+"{0}.tif".format(i), compression = None)

            else :
                print("Wrong information input")

        elif self.methode2 == 'Unique' :
            Extremum_Global = self.SegGlobaleHist()[2]

            if self.methode1 == 'Simple' : 

                for q in range(len(self.object_list)) :

                    for i in tqdm(range(len(self.impathes)), desc='Segmentation of %s' % self.object_list[q]):
                        frame = np.asarray(Image.open(self.data_dir+'/'+self.impathes[i]))
                        lim = self.SegUniqueHist(frame, Extremum_Global)[0]
                        limmin = lim[q][0]
                        limmax = lim[q][1]
                        coordonates_in = self.GetPixelValueBetween(frame, limmin, limmax)
                        coordonates_out = self.GetPixelValueOutside(frame, limmin, limmax)
                        frame1 = self.ChangePixelValue(frame, coordonates_in, 2**16-1)
                        frame2 = self.ChangePixelValue(frame1, coordonates_out, 0)
                        ims = Image.fromarray(frame2)

                        if i<10 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'0000'+"{0}.tif".format(i), compression = None)
                        if i<100 and i>=10 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'000'+"{0}.tif".format(i), compression = None)
                        if i<1000 and i>=100 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'00'+"{0}.tif".format(i), compression = None)
                        if i<10000 and i>=1000 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'0'+"{0}.tif".format(i), compression = None)
                        if i<100000 and i>=10000 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+"{0}.tif".format(i), compression = None)

            elif self.methode1 == 'Commune' :  

                for q in range(len(self.object_list)) :

                    for i in tqdm(range(len(self.impathes)), desc='Segmentation of %s' % self.object_list[q]):
                        frame = np.asarray(Image.open(self.data_dir+'/'+self.impathes[i]))
                        limnew = self.SegUniqueHist(frame, Extremum_Global)[1]
                        limin_inf, limin_supp, between_inf, between_supp, cond, between = self.limits(self, limnew, q)

                        coordonates_out = self.GetPixelValueOutside(frame, limnew[q][0], limnew[q][1])
                        frame1 = self.ChangePixelValue(frame, coordonates_out, 0)
                        coordonates_in = self.GetPixelValueBetween(frame1, limin_inf, limin_supp)
                        frame2 = self.ChangePixelValue(frame1, coordonates_in, 2**16-1)
                        if cond==0 : 
                            frame_rgb = self.ImageGrayToRGB(frame2)
                        if cond==1 : 
                            coordonates_between = self.GetPixelValueBetween(frame2, between_inf, between_supp)
                            frame_rgb = self.ChangePixelValueGrayToRGB(frame2, coordonates_between, 2**16-1,0,0)
                        if cond==2 : 
                            coordonates_between = self.GetPixelValueBetweenSeveral(frame2, between)
                            frame_rgb = self.ChangePixelValueGrayToRGB(frame2, coordonates_between, 2**16-1,0,0)
                        ims = Image.fromarray(frame_rgb)

                        if i<10 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'0000'+"{0}.tif".format(i), compression = None)
                        if i<100 and i>=10 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'000'+"{0}.tif".format(i), compression = None)
                        if i<1000 and i>=100 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'00'+"{0}.tif".format(i), compression = None)
                        if i<10000 and i>=1000 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+'0'+"{0}.tif".format(i), compression = None)
                        if i<100000 and i>=10000 :
                            ims.save(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+self.object_list[q]+"{0}.tif".format(i), compression = None)

            else :
                print("Wrong information input")
        else :
                print("Wrong information input")

        return print('Done')


.. image:: Segmentation_frame.png
    :align: center
    :alt: alternate text

************
Borders Off
************

In tomography, each material has its own density so its own gray level value on the studied frame. The gray level value of a border is equal to the average of level of gray of its components. But sometimes the border has the level of gray of an existing component and will appear during the segmentation. 

* *Input* : object_list of the studied system **list**, folder directory of the frames studied, format of the frames
* *Ouput* : Border taken off of the frames and saved in a single folder depending of folder directory of the frames studied and used later for the segmentation ::

    def borders_off(self):
    
        if os.path.exists(os.path.join(self.parent, self.Borders)):
                return print("File '% s' already exists, can not proceed to segmentation" % self.Borders)

        os.mkdir(os.path.join(self.parent, self.Borders))

        for i in tqdm(range(len(self.impathes)), desc='Borders Off'):
            frame = np.asarray(Image.open(self.data_dir+'/'+self.impathes[i]))
            grad_x, grad_y = np.gradient(self.rescaler(frame))
            img_grad = np.sqrt(grad_x**2 + grad_y**2)
            coordonates_grad = self.GetPixelValueBetween(img_grad, 0.02, 1)
            frame1 = self.ChangePixelValue(frame, coordonates_grad, 0)
            ims = Image.fromarray(frame1)
            if i<10 :
                ims.save(self.parent+'/'+self.Borders+'/'+self.impathes[i].format(i), compression = None)
            if i<100 and i>=10 :
                ims.save(self.parent+'/'+self.Borders+'/'+self.impathes[i].format(i), compression = None)
            if i<1000 and i>=100 :
                ims.save(self.parent+'/'+self.Borders+'/'+self.impathes[i].format(i), compression = None)
            if i<10000 and i>=1000 :
                ims.save(self.parent+'/'+self.Borders+'/'+self.impathes[i].format(i), compression = None)
            if i<100000 and i>=10000 :
                ims.save(self.parent+'/'+self.Borders+'/'+self.impathes[i].format(i), compression = None)


.. image:: Borders_Off.png
    :align: center
    :alt: alternate text



*******
Opening
*******

In some cases there are still some isolated pixels on the binarized frames. The goal of the opening methode is to remove these isolated pixels.

* *Input* : object_list of the studied system **list**, folder directory of the frames studied, format of the frames, methode1 and 2
* *Ouput* : Opening of the frames and saved in a several folder depending of folder directory of the frames studied and object_list ::

    def opening(self):
        
        self.dossier = "Opening_Segmentation"+'_'+self.methode1+'_'+self.methode2
        
    
        if os.path.exists(os.path.join(self.parent, self.dossier)):
                return print("File '% s' already exists, can not proceed to opening" % self.self.dossier)

        self.create_out_dirs()
        
        self.dossier = "Segmentation"+'_'+self.methode1+'_'+self.methode2
        self.Opening_dir = 'Opening_'+ self.dossier
        
        if self.Opening_methode == 'Ellipse':
            kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, self.Opening_size)
            
        elif self.Opening_methode == 'Cross':
            kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, self.Opening_size)
              
        elif self.Opening_methode == 'Rectangle':
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, self.Opening_size)   
        
        else :
            return print("Wrong opening information")
        
        for q in range(len(self.object_list)):
            segmented = sorted([f for f in os.listdir(self.parent+'/'+self.dossier+'/'+self.object_list[q]) if f.endswith(self.format_img)])
            for i in tqdm(range(len(segmented)), desc='Opening of %s' % self.object_list[q]):
                frame = np.asarray(Image.open(self.parent+'/'+self.dossier+'/'+self.object_list[q]+'/'+segmented[i]))
                opening = cv2.morphologyEx(frame, cv2.MORPH_OPEN, kernel)
                ims = Image.fromarray(opening)
                if i<10 :
                    ims.save(self.parent+'/'+self.Opening_dir+'/'+self.object_list[q]+'/'+segmented[i].format(i), compression = None)
                if i<100 and i>=10 :
                    ims.save(self.parent+'/'+self.Opening_dir+'/'+self.object_list[q]+'/'+segmented[i].format(i), compression = None)
                if i<1000 and i>=100 :
                    ims.save(self.parent+'/'+self.Opening_dir+'/'+self.object_list[q]+'/'+segmented[i].format(i), compression = None)
                if i<10000 and i>=1000 :
                    ims.save(self.parent+'/'+self.Opening_dir+'/'+self.object_list[q]+'/'+segmented[i].format(i), compression = None)
                if i<100000 and i>=10000 :
                    ims.save(self.parent+'/'+self.Opening_dir+'/'+self.object_list[q]+'/'+segmented[i].format(i), compression = None)
            

.. image:: Opening.png
    :align: center
    :alt: alternate text


*******
Volume
*******

This fuction is used to determine the volume of the system's objects.

* *Input* : object_list of the studied system **list**, folder directory of the studied frames, pixel volume 
* *Ouput* : Volume in mm³ of all object_list items ::
 
    def volume(self):
        if self.Opening == True:
            self.dossier = "Opening_Segmentation"+'_'+self.methode1+'_'+self.methode2
        else:
            self.dossier = "Segmentation"+'_'+self.methode1+'_'+self.methode2
        for z in range(len(self.object_list)) :
            k = 0
            addition = sorted([f for f in os.listdir(self.parent+'/'+self.dossier+'/'+self.object_list[z]) if f.endswith(self.format_img)])
            for i in range(len(addition)):
                frame = np.asarray(Image.open(self.parent+'/'+self.dossier+'/'+self.object_list[z]+'/'+addition[i]))
                frame1 = frame.astype(np.uint8)
                frame = frame.flatten().astype(np.uint8)
                b =  np.count_nonzero(frame == 255)
                k = np.add(k, b)
            if len(frame)> frame1.shape[0]*frame1.shape[1] :
                k = k/3
            volume = (k.sum())*self.pixel_volume
            print('Size of ' + self.object_list[z] + ' = ' + str(volume) + 'mm^3')

    >>>Size of vide = 180354.96140200004mm^3
    >>>Size of os = 104798.34790600002mm^3
    >>>Size of implant = 22632.612499000006mm^3
    >>>Size of trocart = 2772.954345000001mm^3



*************
Run program
*************

This function uses the previous built function and run automaticaly the segmentation process to obtain the segmentated frames following the different threshold method or border remover and opening process. 

* *Input* : object_list of the studied system **list**, folder directory of the frames studied, format of the frames, methode1 and 2, volume of a pixel, border and opening method
* *Ouput* : binarized (*optional* : without border, opening process) frames saved in several folders depending of folder directory of the frames studied and object_list ::

    def run(self):
        if self.Borders_Off == True:
            self.borders_off()
        self.segmentation()
        if self.Opening == True :
            self.opening()
        self.volume()
                

.. image:: run.png
    :align: center
    :alt: alternate text                
               
                

   



.. toctree::
   :maxdepth: 2
   :numbered:



   

